<?php $this->load->view($header); ?>
<?php $this->load->view($left); ?>
<script type="text/javascript" src="<?php echo site_url("public/ckeditor/ckeditor.js");?>"></script>
<link rel="stylesheet" href="<?php echo site_url("public/validator/css/validationEngine.jquery.css");?>" type="text/css"/>
<script src="<?php echo site_url("public/validator/js/languages/jquery.validationEngine-en.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo site_url("public/validator/js/jquery.validationEngine.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo site_url("public/js/jquery.mask.min.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url("public/js/jquery.fancybox.js");?>" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url("public/css/jquery.fancybox.css");?>" type="text/css"/>
<script>
$('.fancybox').fancybox({
  width:'1200',
  height: '800'
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('.phone_us').mask('000-000-0000');
	$('.phone_us1').mask('(000) 000-0000');
	$("#cont").validationEngine();
});
function form_submit(){
$('#cont').submit();
}

function addAnothercontent(id){ 
	var count = $('#count_content').val();
        var pid = $('#featured_block_id').val();
	var count_val = parseInt(count)+1;
	jQuery('#count_content').val(count_val);
	//alert(count_val);
	jQuery.ajax({
			   type: "POST",
                           async: false,
				url : '<?php echo site_url("kaizen/home_block/add_subjects/");?>',
              data: { count:count,id:id,pid:pid},
				dataType : "html",
				success: function(data)
				{
					if(data)
					{
							jQuery("#add_subjects").prepend(data);
					}
					else
					{
						//alert("Sorry, Some problem is there. Please try again.");
					}
				},
				error : function() 
				{
					alert("Sorry, The requested property could not be found.");		
				}
			});
                        }
</script>
<script type="text/javascript">
     $(function() {
     $('.iframe-btn').fancybox({	
      	'width'		: 900,
      	'height'	: 600,
      	'type'		: 'iframe',
              'autoScale'    	: false
          });
    });
</script>
<div class="rightDiv">
<div class="right-outer">
  <h3 class="title">Edit
    <?php if(isset($details->title)){echo $details->title;}?>
  </h3>
  <div class="clear"></div>
  <div class="mid-block padbot40">
    <div class="mid-content web-cont-mid" style="min-height:180px !important">
      <div id="webcont-form">
        <div id="member-form" class="midarea">
          <?php 
		  $attributes = array('name' => 'cont', 'id' => 'cont');
		  echo form_open_multipart('kaizen/home_block/addedit/'.$details->id,$attributes);
		  echo form_hidden('contact_id', $details->id);
		  ?>
          <?php
		if($this->session->userdata('ERROR_MSG')==TRUE){
			echo '<div class="notific_error">
					<h2 align="center" style="color:#000;">'.$this->session->userdata('ERROR_MSG').'</h1></div>';
			$this->session->unset_userdata('ERROR_MSG');
		}
		if($this->session->userdata('SUCC_MSG')==TRUE){
			echo '<div class="notific_suc"><h2 align="center" style="color:#000;">'.$this->session->userdata('SUCC_MSG').'</h1></div>';
			$this->session->unset_userdata('SUCC_MSG');
		}
		?>
          <?php echo validation_errors('<div class="notific_error">', '</div>'); ?>
          <div class="single-column" >
            <label class="question-label">Title <span>*</span> </label>
            <input type="text" name="title" id="title" value="<?php if(isset($details->title)){echo $details->title;}?>" class="inputinpt validate[required]" />
          </div>
          
          <div class="single-column" >
            <label class="question-label">Sub Title <span>*</span> </label>
            <input type="text" name="sub_title" id="sub_title" value="<?php if(isset($details->sub_title)){echo $details->sub_title;}?>" class="inputinpt validate[required]" />
          </div>
         
            
               <div class="single-column" >
	          <label class="question-label">Image1 - Size Requirement:  372 X 377 px ( W x H )</label><p class="sizetxt"></p>
	          <input id="htmlfile1" name="image1" value="<?php if(!empty($details->image1)) echo $details->image1; ?>" type="text">
	          <a href="<?php echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile1&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>
            
            <div class="single-column" >
	          <label class="question-label">Image2 - Size Requirement:  372 X 377 px ( W x H )</label><p class="sizetxt"></p>
	          <input id="htmlfile2" name="image2" value="<?php if(!empty($details->image2)) echo $details->image2; ?>" type="text">
	          <a href="<?php echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile2&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>
            
            <div class="single-column">
              <label class="question-label">Content </label>
              <?php
                                    if(!empty($details->content)){
                                        $cont_txt1 = outputEscapeString($details->content);
                                    }
                                    else{
                                        $cont_txt1 = "";
                                    }?>
              <textarea id="content" name="content" class="editor"><?php echo $cont_txt1;?></textarea>
            </div>
            
            <div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Button Text:<span> *</span></label>
                                <input type="text" name="button_text" id="button_text" value="<?php if(isset($details->button_text)){echo $details->button_text;}?>" class="inputinpt validate[required]"/>
                            </li>
                        </ul>
                    </div>
                    
                    
                    
                    <div class="single-column">
     <label class="labelname">Button Type<span> *</span></label>
     <input type="radio" id="button_type" name="button_type" value="1" <?php if(!empty($details->button_type) && $details->button_type == '1'){echo "checked";} ?>       onclick="show_page_div('button_page_div','button_ext_div');" class="inputinpt validate[required]"/> Select Page
    <input type="radio" id="button_type" name="button_type" value="2" <?php if(!empty($details->button_type) && $details->button_type == '2'){echo "checked";} ?>         onclick="show_ext_div('button_page_div','button_ext_div');"  class="inputinpt validate[required]"/> External Link
     </div>
                   

                    <script>
                   function show_page_div(a,b){ 
					   document.getElementById(a).style.display = '';
					   document.getElementById(b).style.display = 'none';
				   }
				   function show_ext_div(a,b){
					   document.getElementById(a).style.display = 'none';
					   document.getElementById(b).style.display = '';
				   }
                   </script>


 <div class="single-column" id="button_page_div" style="display:<?php if(!empty($details->button_type) && $details->button_type == '1'){echo  ''; }else{echo  'none';} ?>;">
        <label class="question-label">Select Page:<span>*</span></label>
          <select name="page_link" id="page_link" class="inputinpt validate[required]">
              <option value="" >-Select Pages-</option>
     <?php 
	 
	  foreach ($page1 as $rs_menu_id){?>
		  
            <option value="<?php echo $rs_menu_id->id?>" <?php if( $rs_menu_id->id==$details->page_link){ echo selected ; }?> ><?php echo $rs_menu_id->title;?></option> <?php }
	  
	  ?>
              </select>
            </div>
            
            
            <div class="single-column" id="button_ext_div" style="display:<?php if(!empty($details->button_type) && $details->button_type == '2'){echo  ''; }else{echo  'none';} ?>;">
              <label class="question-label">External Link URL: <span> *</span></label>
              <input type="text" name="banner_url" id="banner_url" value="<?php if(!empty($details->banner_url) && $details->button_type == '2'){ echo $details->banner_url;} ?>" class="inputinpt validate[required,custom[url]]" />
            </div>
            
                    
                    
                    
        <!--<div class="single-column">
                             <input type="hidden" name="count_content" id="count_content" value="1" />
                     <label class="labelname" ><a href="javascript:void(0);" onclick="addAnothercontent();" >+Add Subjects</a></label>
                             <div id="add_subjects" class=""></div>
             </div>-->    

              
<!--               <div class="single-column">
                             <input type="hidden" name="count_content" id="count_content" value="1" />
    <label class="labelname" ><a href="javascript:void(0);" onclick="addAnothercontent('','','','','','','','','','','','');" >+Add Contact</a></label>
                             <div id="add_content" class=""></div>
             </div>-->
             
             
          <div class="bottonserright" style="padding-bottom:20px;"> 
            <a href="javascript:void(0);" class="web-red-btn" onClick="form_submit();"><span>Save</span></a> <?php echo form_close();?> </div>
        </div>
        </div>
        <div class="bodybottom"> </div>
      </div>
    </div>
    
  </div></div>
  <div class="clear"></div>
  <?php $this->load->view($footer); ?>
</div>
<script type="text/javascript">
<?php if(!empty($content_list)) {
   // pre($content_list);
     foreach ($content_list as $content){
                ?>
 addAnothercontent('<?php echo $content->id; ?>');
     <?php } }?>
	 
	 
	 var editor, html = '';
	if (editor ){
   	editor.destroy();
	editor = null;
	}
    CKEDITOR.replace( 'content' ,{
	width : '95%',
	contentsCss : '<?php echo site_url("public/css/style_ck.css");?>',	
	filebrowserBrowseUrl : '<?php echo site_url("public/ckfinder/ckfinder.html");?>',
	filebrowserUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files");?>',
	filebrowserImageUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images");?>',
	filebrowserFlashUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash");?>'
});

</script>
