<?php foreach($gallery_list as $list){ 
     if(!empty($list->image) && is_file(file_upload_path($list->image))){
    ?>
<li>
        <div class="block">
                <img src="<?php echo $list->image; ?>" alt="<?php echo $list->title; ?>" title="<?php echo $list->title; ?>">
                <div class="about-gal-textwrap">
                        <div class="display-table">
                                <div class="display-tablecell">
                                        <div class="about-gal-text">
                                                <p><?php echo $list->title; ?></p>
                                                <a href="<?php echo $list->image; ?>" data-fancybox-group="gallery" class="btn fancybox">View</a>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</li>
<?php } } ?>