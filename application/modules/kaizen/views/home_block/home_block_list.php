<?php $this->load->view($header); ?>
<?php $this->load->view($left); ?>
<style>
div.dataTables_length {
	padding-left: 2em;
}
div.dataTables_length,  div.dataTables_filter {
	padding-top: 0.55em;
}
</style>
<script type='text/javascript' src='<?php echo base_url(); ?>public/js/jquery.dataTables.min.js'></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/jquery.dataTables.min.css" />
<div class="rightDiv">
  <div class="right-outer">
    <div class="right-outer">
      <h3 class="title">Home Block List</h3>
      <div class="bread-crumb">
       
      </div>
      <div class="clear"></div>
      
      <!--Search area end-->
      <div class="application-table-area">
        <div class="mid-block">
          <div class="members-table member-group">
            <?php
		if($this->session->userdata('ERROR_MSG')==TRUE){
			echo '<div class="notific_error">
					<h2 align="center" style="color:#fff;">'.$this->session->userdata('ERROR_MSG').'</h1></div>';
			$this->session->unset_userdata('ERROR_MSG');
		}
		if($this->session->userdata('SUCC_MSG')==TRUE){
			echo '<div class="notific_suc"><h2 align="center" style="color:#000;">'.$this->session->userdata('SUCC_MSG').'</h1></div>';
			$this->session->unset_userdata('SUCC_MSG');
		}
		?>
            <table cellspacing="0" cellpadding="0" border="0" id="pagi">
              <thead>
                <tr>
                  <th>Title<span class=""></span></th>
                   <th>Sub Title<span class=""></span></th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
        			  if(empty($records)){
        				  ?>
                <tr>
                  <td align="center" valign="top"><b>There are no Records.</b><br /></td>
                </tr>
                <?php
        			  }
        			  else{
        			  $i=0;
        			  foreach($records as $row){
        				  $i++;				  
        			  ?>
                <tr>
                  <td><?php echo $row->title; ?></td>
                  <td><?php echo $row->sub_title; ?></td>
                   
                    </td>
                  <td><a class="block-btn edit-btn" href="<?php echo site_url("kaizen/home_block/doedit/".$row->id);?>"><span>Edit</span></a></td>
                </tr>
                <?php
        			  }
        			  }
        			  ?>
              </tbody>
            </table>
          </div>
          <?php $this->load->view($footer); ?>
        </div>
        <div class="rt-block">
          
        </div>
      </div>
    </div>
  </div>
  
  <!--right column end-->
  <div class="clear"></div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    $('#pagi').dataTable( {
      "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',
    "aoColumns": [
	      null,  
              null,
               null,
      { "bSortable": false }
  
    ]
    } );
} );
</script>