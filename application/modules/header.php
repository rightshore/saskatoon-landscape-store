<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php //pre($this->data['hooks_meta']);
if(!empty($this->data['hooks_meta']->title))
{
	echo '<title>'.$this->data['hooks_meta']->title.'</title>'."\n";
}
else
{
	echo '<title>'.$this->data['site_settings']->site_name.'</title>'."\n";
}

if(!empty($meta_keyword))
{
	$meta_keywords= $meta_keyword;
}
else if(!empty($this->data['hooks_meta']->meta_keyword))
{
	$meta_keywords= $this->data['hooks_meta']->meta_keyword;
}
else
{
	$meta_keywords= $this->data['site_settings']->meta_keyword;
}

if(!empty($meta_description))
{
	$meta_descriptions= $meta_description;
}
else if(!empty($this->data['hooks_meta']->meta_description))
{
	$meta_descriptions= $this->data['hooks_meta']->meta_description;
}
else
{
	$meta_descriptions= $this->data['site_settings']->meta_description;
}	
$mainpage_link = $this->uri->segment(1);
$meta = array(
        array('name' => 'robots', 'content' => 'index, follow, all'),
        array('name' => 'description', 'content' => $meta_descriptions),
        array('name' => 'keywords', 'content' => $meta_keywords),
		array('name' => 'X-UA-Compatible', 'content' => 'IE=edge', 'type' => 'equiv'),
		array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'),
        array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')
		
    );

$page_link = xss_clean($this->uri->segment(1));
if($page_link=='pages')
{
	$page_link = xss_clean($this->uri->segment(2));
}
echo meta($meta);
if(!empty($this->data['site_settings']->site_verification)){
    echo '<meta name="google-site-verification" content="'.$this->data['site_settings']->site_verification.'" />';
}
?>

<!--font section start-->
 <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Oswald:300,400,500,600,700" rel="stylesheet"> 
 <!--font section end-->

<!--css section start-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/style.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/slider.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/media.css'); ?>" />
<link rel="icon" href="http://192.168.1.12/sarcan/public/default/images/icon.png" type="image/gif" sizes="16x16" />

<!--script section start-->
<script type='text/javascript' src='<?php echo base_url('public/default/js/jquery-1.8.3.min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/responsiveslides.min.js'); ?>'></script>
<script>
	$(function() {
    	$(".rslides").responsiveSlides({
			pager: true,
			nav: true,
			auto: false
		});
  	});
</script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/custom.js'); ?>'></script>

<!--script section end-->
</head>

<body>
        <!--banner section start-->
        <?php
        if($this->data['hooks_meta']->page_link=='home' || $this->data['hooks_meta']->id==1){
            $cls='banner';
        }else{
            $cls='banner inner-banner';
        }
        ?>
    <div class="<?php echo $cls; ?>">
    	<?php //pre($commonbanner); ?>
        <!--header section start-->
        <div class="header">
        	<div class="wrapper">
                    <?php 
                    $header_logo_photo = base_url('public/default/images/logo.png');
                    if($this->data['site_settings']->logo_photo!=''){	
                       $path2 = str_replace(base_url().'public/uploads/',file_upload_absolute_path(),$this->data['site_settings']->logo_photo);
                    if(is_file($path2)) { $header_logo_photo = $this->data['site_settings']->logo_photo; }}
                     ?>
            	<a href="<?php echo site_url(); ?>"><img src="<?php echo $header_logo_photo; ?>" alt="<?php echo $this->data['site_settings']->title; ?>" title="<?php echo $this->data['site_settings']->title; ?>" class="logo" /></a>
                <div class="top-right">
                        <?php if(!empty($this->data['contact']->contact_phone)){ ?><p class="call"><?php echo $this->data['contact']->contact_phone; ?></p><?php } ?>
                    <ul class="top-social">
                        <?php echo socialLinks(); ?>
<!--                    	<li><a href="#" class="fb"><img src="images/top_fb_icon.png" alt="" title="" /></a></li>
                        <li><a href="#" class="twit"><img src="images/top_twit_icon.png" alt="" title="" /></a></li>-->
                    </ul>
                    <div class="clear"></div>
                    <a class="responsive"><span></span></a>
                </div>
                <div class="clear"></div>
            </div>
            <?php echo $hooks_cmspages_list; ?>
<!--            <ul class="nav">
                <li><a href="index.html" class="active">Home</a></li>
                <li><a href="about.html">About Us</a></li>
                <li><a href="courses-offered.html">Courses Offered</a></li>
                <li><a href="ground-school.html">Ground School</a></li>
                <li><a href="fleet.html">Fleet</a></li>
                <li><a href="contact.html">Contact Us</a></li>
            </ul>-->
        	<div class="clear"></div>
        </div>
        <!--header section end-->
    	
        
        <!--slider section start-->
        <?php if($this->data['hooks_meta']->page_link=='home' || $this->data['hooks_meta']->id==1){ ?>
        <?php if(!empty($banner_list)) { ?>
        <ul class="rslides">
            <?php foreach($banner_list as $list) { 
                if(!empty(file_upload_path($list->banner_photo)) && !empty($list->banner_photo)){
            ?>
        	<li>
            	<img src="<?php echo $list->banner_photo; ?>" alt="" title="" />
            	<div class="wrapper">
            		<div class="banner-cont">
            			<h1><?php echo $list->title; ?></h1>
            			<?php if(!empty($list->excerpt)) echo outputEscapeString($list->excerpt); ?>
                                <?php 
                                $url='';
                       if(!empty($list->button_type) && $list->button_type==1 && !empty($list->button) && !empty($list->selected_page_link)){
                                    $url=urlByPageLink($list->selected_page_link);
                        }elseif(!empty($list->button_type) && $list->button_type==2 && !empty($list->button) && !empty($list->external_url)){
                                    $url=$list->external_url;
                                } ?>
                 <?php if(!empty($url)){ ?><a href="<?php echo $url; ?>" class="btn"><span><?php echo $list->button; ?></span></a><?php } ?>
            		</div>
            	</div>
            </li>
            <?php } } ?>
        </ul>
        <?php } ?>
        <?php }else{ ?>
        <?php if(!empty($commonbanner)){ 
            if(!empty(file_upload_path($commonbanner->banner_photo)) && !empty($commonbanner->banner_photo)){
        ?>
        <ul class="rslides">
        	<li>
            	<img src="<?php echo $commonbanner->banner_photo; ?>" alt="<?php echo $this->data['hooks_meta']->title; ?>" title="<?php // echo $this->data['hooks_meta']->title; ?>" />
            	<div class="wrapper">
            		<div class="banner-cont">
                                <?php if(!empty($this->data['hooks_meta']->title)){ ?><h1><?php echo $this->data['hooks_meta']->title; ?></h1><?php } ?>
            		</div>
            	</div>
            </li>
        </ul>
        <?php } } } ?>
        <!--slider section end-->
    </div>
	<!--banner section end-->