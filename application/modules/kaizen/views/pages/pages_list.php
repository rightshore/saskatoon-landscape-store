<?php $this->load->view($header); ?>
<?php $this->load->view($left); ?>
<style>
div.dataTables_length {
    padding-left: 2em;
    }
    div.dataTables_length,
    div.dataTables_filter {
        padding-top: 0.55em;
    }
</style>
<script>
function form_submit(){
    $('#cont').submit();
}
</script>
<script type='text/javascript' src='<?php echo base_url(); ?>public/js/jquery.dataTables.min.js'></script> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>public/css/jquery.dataTables.min.css" />
<div class="rightDiv">
	<div class="right-outer">
    	<div class="right-outer">
		<h3 class="title">Pages List</h3>
        <div class="clear"></div>
        <!--Search area end-->
        <div class="application-table-area">
        	<div class="mid-block">
                    <form id="cont" action="<?php echo base_url("kaizen/pages") ?>" method="get" style="display: none;">
                        <div id="webcont-form" style="margin-bottom:30px;">
                        <div class="single-column" >
                            <label class="labelname" style="float:left;">Select location:</label>
              <select name="parent_id" id="parent_id" style="width:268px;height:30px;float:left;margin-left:10px;" tabindex="1">
                <option value="0" <?php echo ((!isset($parent_selectid))?'selected="selected"':'')?> >-All-</option>
               <?php 
//               print_r($page_list);

$OPT_MENU = '';
$parent_id = 0;
     $menus_array = array();
     foreach ($page_list as $rs_menu_id){
     if($rs_menu_id->id!='1' && $rs_menu_id->id!='21')
     {
       $menus_array[$rs_menu_id->id] = array('id' => $rs_menu_id->id,'title' => $rs_menu_id->title,'parent_id' => $rs_menu_id->parent_id,'page_link' => $rs_menu_id->page_link);
       } 
     }
//     print_r($menus_array);
    if(empty($selected)){
        $selected = 0;
    }
        $optmenu = generate_opt_menu2(0,$OPT_MENU,0,$selected,empty($selected)?0:$details->id);
//        $optmenu = generate_opt_menu2(0,$menus_array, $OPT_MENU, $level_depth=0,$selected,$top=0);
     echo $optmenu;
       ?>
              </select>
                            <a href="javascript:void(0);" class="web-red-btn" onclick="form_submit();" style="float:left;margin-left: 10px;"><span>Submit</span></a>
            </div>
                            
            </div>
                    </form>
            	<div class="members-table member-group">
						<?php
		if($this->session->userdata('ERROR_MSG')==TRUE){
			echo '<div class="notific_error">
					<h2 align="center" style="color:#fff;">'.$this->session->userdata('ERROR_MSG').'</h1></div>';
			$this->session->unset_userdata('ERROR_MSG');
		}
		if($this->session->userdata('SUCC_MSG')==TRUE){
			echo '<div class="notific_suc"><h2 align="center" style="color:#000;">'.$this->session->userdata('SUCC_MSG').'</h1></div>';
			$this->session->unset_userdata('SUCC_MSG');
		}
		?>
        <div ><h2 align="center" style="color:#31B12c;"  id="status_mesage"></h2></div>
                    <table cellspacing="0" cellpadding="0" border="0" id="pagi">
						<thead><tr>
                            <th>Title<span class=""></span></th>
                             <th>Show in Top<span class=""></span></th>
<!--                             <th>Show in Footer<span class=""></span></th>
                             <th>Show after Banner<span class=""></span></th>-->
                            <th>Status<span class=""></span></th>
							<th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
        							<?php
        			  if(empty($records)){
        				  ?>
        							<tr>
        								<td align="center" valign="top"><b>There are no records.</b><br /></td>
        							</tr>
        							<?php
        			  }
        			  else{
        			  $i=0;
        			  foreach($records as $row){
        				  $i++;				  
        			  ?>
                      
        										<tr>
        											<td><?php echo $row->title; ?></td>
        											<td><?php if($row->shown_in_top==1){ 
												echo "Yes";
												 } else { 
												echo "No";
												 } ?>
													</td>
                                                    
<!--                                                    <td><?php /*if($row->shown_in_footer==1){ 
												echo "Yes";
												 } else { 
												echo "No";
												 } */ ?>
													</td>-->
<!--                                                   
													</td>-->
                                               
                                                    <td>
                                                    <span id="status_td<?php echo $row->id; ?>"><?php if($row->is_active==1){ ?>
	<a href="javascript:void(0)" onclick="change_status('<?php echo $row->id; ?>','0','status_td','cms_pages','pages')" title="Active"> <img src="<?php echo site_url("public/images/unlock_icon.gif");?>" alt="Active"/> </a>
												<?php } else{ ?>
<a href="javascript:void(0)" onclick="change_status('<?php echo $row->id; ?>','1','status_td','cms_pages','pages')" title="Inactive"> <img src="<?php echo site_url("public/images/locked_icon.gif");?>" alt="Inactive"/></a>
												<?php } ?>
								             </span>
													</td>
                                                    
                                                    		
                							<td>
                								<a class="block-btn edit-btn" href="<?php echo site_url("kaizen/pages/doedit/".$row->id);?>"><span>Edit</span></a> <a href="javascript:void(0);" title="Delete" onclick="rowdelete('<?php echo $row->id; ?>','cms_pages');" class="delete3 delete-red-btn"><span>Delete</span></a>
                                </a>
                              </td>		</tr>
        									
        							<?php
        			  }
        			  }
        			  ?>        
                    </tbody></table>
            	</div>
                 <?php $this->load->view($footer); ?>
          </div>
             <div class="rt-block">
             	  <?php $this->load->view($right); ?>
        </div>
    </div>
    </div>
</div>
<!--right column end-->
<div class="clear"></div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
    $('#pagi').dataTable( {
      "dom": '<"top"iflp<"clear">>rt<"bottom"iflp<"clear">>',	
      "order": [[ 0, "asc" ]],
      "stateSave": true,
    "aoColumns": [
    null,
    null,
	 
	 { "bSortable": false },
      { "bSortable": false }
    ],
    "fnDrawCallback": function () {
        var aTag = $("#pagi_wrapper");           
        $('html,body').animate({scrollTop: aTag.offset().top},'slow');
    }
    
    } );
} );
</script>