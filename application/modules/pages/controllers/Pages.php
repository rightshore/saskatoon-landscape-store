<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pages extends CI_Controller {
	var $data;
	public function __construct() {
	   parent::__construct();
	  $this->load->vars( array(		  
		  'header' => 'common/header',
		  'footer' => 'common/footer',
                  'right' => 'common/right'
		));
		$this->load->model('model_pages');
	    }
	
	public function index()	
	{
            if(!empty($this->data['hooks_meta']->page_link) && !empty($this->data['hooks_meta']->is_active)){
                if($this->data['hooks_meta']->page_link=='contact_us'){  
                    //$where=array('is_active2'=>1);
                   // $order_by=array('title'=>'asc');
                  //  $this->data['contact_subjects']=$this->model_pages->select_row('contact_subjects',$where,$order_by); 
                    $this->load->view('contact_us',$this->data);
                }else if($this->data['hooks_meta']->page_link=='ground_cover_calculator'){
                    $this->load->view('ground_cover_calculator',$this->data);
                }else if($this->data['hooks_meta']->page_link=='retaining_wall_calculator'){
                    $this->load->view('retaining_wall_calculator',$this->data);
                }else if($this->data['hooks_meta']->page_link=='xxxxxxxxx'){
                    $where=array('is_active'=>1);
                    $order_by=array('display_order'=>'asc');
                    $this->data['xxxxxxxx']=$this->model_pages->select_row('xxxxxxxxxxx',$where,$order_by);  
                    $this->load->view('xxxxxxx',$this->data);
                }else if($this->data['hooks_meta']->page_link=='about_us'){  
                    $where=array('is_active2'=>1);
                    $order_by=array('title'=>'asc');
                    $this->data['about_us']=$this->model_pages->select_row('cms_pages',$where,$order_by); 
                    $this->load->view('about_us',$this->data);
                }else{
                    $this->load->view('pages',$this->data);
                }
            }else{
                redirect("notfound404");
            }
    } 
}