<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MY_Controller 
{
	private $limit = 20;
	var $offset = 0;
	function __construct()
	{
		parent::__construct();		 
		
		if( ! $this->session->userdata('web_admin_logged_in')) {
			redirect('kaizen/welcome','refresh');
		}
		$this->load->vars( array(
		  'global' => 'Available to all views',
		  'header' => 'common/header',
		  'left' => 'common/left',
           'right' => 'common/right',
		  'footer' => 'common/footer'
		));
		
		$this->load->model('modelcategory');	
	}

	public function index()
	{	
		
		$this->dolist();	
	}
	
	
	
	public function dolist(){
		$data = array();
		$where = array();
                $order_by = array('display_order' => 'asc');
		$data_row = $this->modelcategory->select_row('category',$where,$order_by);
		$data['records']= $data_row;
		$this->load->view('kaizen/category/category_list',$data);		
	}
	
	public function doadd(){
		$data = array();
                $data['details']= new stdClass;
		$banner_id=$this->uri->segment(4);
		$data['details']->is_active = 1;
		$data['details']->recycle = 1;
		$data['details']->id = $banner_id;
	    $data_row = $this->modelcategory->select_row('category',array('parent_id'=>0));
		$data['category']= $data_row;
		$this->load->view('kaizen/category/edit_category',$data);		
	}
    
	public function addedit()
	{
          // pre($_POST); 
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
		
		
		$this->form_validation->set_error_delimiters('<span class="validation_msg">', '</span>');
		$id=$this->input->post('banner_id','');
	 $banner_detls ='';
	 $banner_photo='';
        
		if($this->form_validation->run() == TRUE) // IF MENDATORY FIELDS VALIDATION TRUE(SERVER SIDE)  
		{	
			$where = array(
                           
                            'id' => $id
                        );
                        
                $banner_detls = $this->modelcategory->select_row('category',$where);
				 
        		if(!empty($banner_detls)) 
                {
                   
			                     
                                $this->title                     =$this->input->post('title',TRUE);
								$this->parent_id                     =$this->input->post('parent_id',TRUE);
								$this->display_order		=$this->input->post('display_order');
                                $this->is_active		=$this->input->post('is_active',TRUE); 
                                if($this->is_active===false){
                                        $this->is_active='1';
                                }
								
												
                                $update_data = array(
                                  
                                   'title'                             => $this->title,
								   'parent_id'                             => $this->parent_id,
									'display_order' 			=> $this->display_order,
                                    'is_active' 			=> $this->is_active
                                );
                                
                
				$update_where = array('id' => $id);
				if($this->modelcategory->update_row('category',$update_data,$update_where)) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{
				
					$session_data = array("SUCC_MSG"  => "Category Updated Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else 
				{	
					$session_data = array("ERROR_MSG"  => "Category People Not Updated.");
					$this->session->set_userdata($session_data);				
				}
			}
			else 
			{                   $this->title                     =$this->input->post('title',TRUE);
			                    $this->parent_id                     =$this->input->post('parent_id',TRUE);
								$this->display_order		=$this->input->post('display_order');
                                $this->is_active		=$this->input->post('is_active',TRUE); 
                                if($this->is_active===false){
                                        $this->is_active='1';
                                }
				
        
				
                $add_data = array(
                                    
                                    'title'                             => $this->title,
									'parent_id'                             => $this->parent_id,
									'display_order' 			=> $this->display_order,
                                    'is_active' 			=> $this->is_active,
                                    'page_link' 			         => name_replaceCat('category',$this->title)
                                );
								//pre($add_data);exit;
                               // echo 'hiiiiiii';
				$id = $this->modelcategory->insert_row('category',$add_data);
				if($id) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{ 
                   
					$session_data = array("SUCC_MSG"  => "Category Inserted Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else // IF UPDATE PROCEDURE NOT EXECUTE SUCCESSFULLY
				{	
					$session_data = array("ERROR_MSG"  => "Category People Not Inserted.");
					$this->session->set_userdata($session_data);				
				}
			}
			redirect("kaizen/category/doedit/".$id,'refresh');			
		}
		else{
			if(!empty($id)){
			$this->doedit();
			}
			else{
				$this->doadd();
			}
		}
	}
	public function doedit()
	{ 
		$data = array();
		$banner_id=$this->uri->segment(4); 
		$where = array(
                            'id' => $banner_id
                        );
                $banner_detls = $this->modelcategory->select_row('category',$where);                       
		if($banner_detls){
			$data['details'] = $banner_detls[0];
		}
		else{
			$data['details']->is_active = 1;
			$data['details']->recycle = 1;
			$data['details']->id = 0;
		}
        $data_row = $this->modelcategory->select_row('category',array('parent_id'=>0));
		$data['category']= $data_row;
		$this->load->view('kaizen/category/edit_category',$data);				
	}
}
