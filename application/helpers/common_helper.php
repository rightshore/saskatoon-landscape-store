<?php 
function pre($data){
    echo '<pre>';print_r($data);'</pre>';
}


function common_email_send($to_email,$form_email,$subject,$message,$attachment=''){
    $CI =& get_instance();
    $CI->load->library('email');
    $config['wordwrap'] = TRUE;
    $config['validate'] = TRUE;
    $config['mailtype'] = 'html';
    $CI->email->initialize($config);
    $CI->email->from($form_email);
    $CI->email->to($to_email);
    #$CI->email->to('reviewwomen2web@gmail.com'); 
    #$CI->email->to('soumya@2webdesign.com');
    $CI->email->reply_to($form_email);
    $CI->email->subject($subject);
    $CI->email->message($message);
    if(!empty($attachment)){
        foreach($attachment as $attach){
            $CI->email->attach($attach);
        }
    }
    if($CI->email->send()) {
        $returnss = 1;
    }else{
          $returnss = 0;
    }
    $CI->email->clear(TRUE);
    return $returnss;
}



function socialLinks(){
        $CI =& get_instance();
        $CI->load->model("common/model_common");
        $where = array(
            'site_id' => 1
        );
        $order_by = array(
            'sequence' => 'asc'
        );
        $social_settings = $CI->model_common->select_row("social_settings",$where,$order_by);
        //pre($social_settings);
        $default = array(
            1   => 'fb',
            2   => 'twit',
            3   => 'in',
            8   => 'insta',
            5   => 'utube',
            6   => 'pint',
            4   => 'gplus'
        );
        $html = '';
        if(!empty($social_settings)){
            foreach($social_settings as $sos){
                $class = '';
                if($default[$sos->social_menus_id]){
                    $class = $default[$sos->social_menus_id];
                }
				$html .= '<li >
                    	<a href="'.$sos->link.'" class="'.$class.'" target="_blank">
                            <img src="'.$sos->logo.'" alt="" title="">
                        </a>
                    </li>';
            }
        }
        echo $html;
        
}

function socialLinksContact(){
        $CI =& get_instance();
        $CI->load->model("common/model_common");
        $where = array(
            'site_id' => 1
        );
        $order_by = array(
            'id' => 'asc'
        );
        $social_settings = $CI->model_common->select_row("social_settings",$where,$order_by);
        //pre($social_settings);
        $default = array(
            1   => 'fb',
            2   => 'twit',
            3   => 'inst',
            4   => 'google',
            5   => 'utube',
            6   => 'pint'            
        );
        $html = '<li>';
        if(!empty($social_settings)){
            foreach($social_settings as $sos){
                //pre($sos);
                $class = '';
                /*if($default[$sos->social_menus_id]){
                    $class = $default[$sos->social_menus_id];
                }*/
				$html .= '<a href="#" target="_blank" class="fb"></a>';
            }
        }
		$html .= '</li>';
        echo $html;
        
}
function name_replaceCat($table,$string){    
                $CI =& get_instance();
		$string = strip_tags(outputEscapeString($string));
		$string = preg_replace('/[^A-Za-z0-9\s\s]/', '', $string);
		$cat_replace = str_replace(" ","_",$string);
		$query = $CI->db->query("SELECT `id` FROM `".$CI->db->dbprefix($table)."` WHERE page_link like '%".$cat_replace."%'");
		$count=$query->num_rows();
		if($query->num_rows()>0){
			$cat_replace=$cat_replace.($count+1);
		}
		return strtolower($cat_replace);
	}
function uploadImage($field='',$upload_dir='',$file_type='gif|jpg|jpeg|png') 
    {		
        $CI =& get_instance();
        $field_name=$field;

        if(!is_dir(file_upload_absolute_path().$upload_dir)){
                $oldumask = umask(0); 
                mkdir(file_upload_absolute_path().$upload_dir, 0777); // or even 01777 so you get the sticky bit set 
                umask($oldumask);
        }
		
        $config['upload_path'] = file_upload_absolute_path().$upload_dir;
        $config['allowed_types'] = $file_type;
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000'; 
		
		                     
        
        $CI->load->library('upload', $config); // LOAD FILE UPLOAD LIBRARY
        $CI->upload->initialize($config);
        if($CI->upload->do_upload($field_name)) // CREATE ORIGINAL IMAGE
		{						
			$fInfo = $CI->upload->data();					
			//$data['uploadInfo'] = $fInfo;            
			
			return $fInfo['file_name']; // RETURN ORIGINAL IMAGE NAME
		}
		else // IF ORIGINAL IMAGE NOT UPLOADED
		{			
			return false; // RETURN ORIGINAL IMAGE NAME              
		}
}

function resizingImage($fileName='',$upload_rootdir='',$img_width='',$img_height='',$img_prefix = ''){
	
        $CI =& get_instance();
        if(!is_dir(file_upload_absolute_path().$upload_rootdir)){
                $oldumask = umask(0); 
                mkdir(file_upload_absolute_path().$upload_rootdir, 0777); // or even 01777 so you get the sticky bit set 
                umask($oldumask);
        }
		
        $config['image_library'] = 'gd2';		
     	$config['source_image'] = file_upload_absolute_path().$upload_rootdir.$fileName; 
        $config['new_image']	= file_upload_absolute_path().$upload_rootdir.$img_prefix.$fileName;
        
        $config['maintain_ratio'] = FALSE;
        $config['width'] = $img_width;
        $config['height'] = $img_height;
	$CI->load->library('image_lib', $config);
       	$CI->image_lib->initialize($config); 
      	if(!$CI->image_lib->resize()){
            echo $CI->image_lib->display_errors();			
        }
        else{
                return false;
        }
	
        

}

function uploadDocument($field='',$upload_dir='',$file_type='*') 
    {		
        $CI =& get_instance();
        $field_name=$field;

        if(!is_dir(file_upload_absolute_path().$upload_dir)){
                $oldumask = umask(0); 
                mkdir(file_upload_absolute_path().$upload_dir, 0777); // or even 01777 so you get the sticky bit set 
                umask($oldumask);
        }
		
        $config['upload_path'] = file_upload_absolute_path().$upload_dir;
        $config['allowed_types'] = $file_type;
        $config['max_size'] = '5000';
        $config['max_width'] = '5000';
        $config['max_height'] = '5000'; 
		
		                     
        
        $CI->load->library('upload', $config); // LOAD FILE UPLOAD LIBRARY
        $CI->upload->initialize($config);
        if($CI->upload->do_upload($field_name)) // CREATE ORIGINAL IMAGE
		{						
			$fInfo = $CI->upload->data();					
			//$data['uploadInfo'] = $fInfo;            
			
			return $fInfo['file_name']; // RETURN ORIGINAL IMAGE NAME
		}
		else // IF ORIGINAL IMAGE NOT UPLOADED
		{			
			return false; // RETURN ORIGINAL IMAGE NAME              
		}
}

        function get_video_url($video_url=''){
                $play=strpos($video_url,"vimeo");
                if($play>0){
                    $video_arr=explode("com/",$video_url);
                    $str=$video_arr[1];
                    $video_str="http://player.vimeo.com/video/".$str."?rel=0";
                }else{
                    $video_str=$video_url;
                }
                return $video_str;
        }
        
        function get_video_image($video_url=''){
                $play=strpos($video_url,"vimeo");
                if($play>0){
                    $video_arr=explode("com/",$video_url);
                    $imgid=$video_arr[1];
                    $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$imgid.php"));
                    $img_str=$hash[0]['thumbnail_medium'];
                }else{
                    $video_arr=explode('embed/',$video_url);
                    $imgarr=explode('?list=',$video_arr[1]);
                    $imgid=$imgarr[0];
                    $img_str='http://img.youtube.com/vi/'.$imgid.'/default.jpg';
                }
                return $img_str;
        }
        
        function urlByPageLink($page_link=''){
        $CI =& get_instance();
        $CI->load->model("common/model_common");
	$hrefVal='';
	if(!empty($page_link))
	{
            $where=array('page_link'=>$page_link);
            $page_dtls=$CI->model_common->selectOne("cms_pages",$where);
            if(!empty($page_dtls)){
                if(!empty($page_dtls->external_link)){
                    $hrefVal=$page_dtls->external_link;
                }else{
                    if($page_link=="home"){
                            $hrefVal=site_url();
                    }else if($page_link=="products" || $page_link=="gallery"){
                            $hrefVal=site_url($page_link);
                    }else{
                            $hrefVal=site_url('pages/'.$page_link);
                    }
                }
            }
        }else{
            $hrefVal='javascript:void(0);';
        }
	
    return $hrefVal;
        
}

function file_upload_path($image_url=''){
            if(!empty($image_url)){
                $path=str_replace(base_url().'public/uploads/',file_upload_absolute_path(),$image_url);
                return $path;
            }else{
                return '';
            }
        }


?>