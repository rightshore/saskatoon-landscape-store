<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends CI_Controller {
	var $data = array();
	public function __construct() {
	   parent::__construct();
	   $this->load->vars( array(		  
		  'header' => 'common/header',
		  'footer' => 'common/footer'
		));		
		$this->load->model('home/model_home');		
	}


        public function index(){
               $this->data['home_block']=  $this->model_home->selectOne('home_block');
               
                $where=array('is_active'=>1,'is_featured'=>1);
				$order=array('display_order'=>'asc');
                $this->data['prod_list'] =  $this->model_home->select_row('products',$where,$order);

                $this->load->view('home',$this->data);
            }
            
} 
