<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Commonbanner extends MY_Controller 
{
	private $limit = 20;
	var $offset = 0;
	function __construct()
	{
		parent::__construct();		 
		
		if( ! $this->session->userdata('web_admin_logged_in')) {
			redirect('kaizen/welcome','refresh');
		}
		$this->load->vars( array(
		  'global' => 'Available to all views',
		  'header' => 'common/header',
		  'left' => 'common/left',
		  'footer' => 'common/footer',
		  'right' => 'common/right'
		));
		
		$this->load->model('modelcommonbanner');	
	}

	public function index()
	{	
		
		$this->dolist();	
	}
	
	
	
	public function dolist(){
		$data = array();
		
                $where = array(
                    
                            
                        );
                $order_by = array('id' => 'asc');
		$data_row = $this->modelcommonbanner->select_row('commonbanner',$where,$order_by);
		$data['records']= $data_row;
		$this->load->view('kaizen/commonbanner/commonbanner_list',$data);		
	}
	
	public function doadd(){
		$data = array();
                $data['details']= new stdClass;
		$commonbanner_id=$this->uri->segment(4);
		$data['details']->is_active = 1;
		$data['details']->id = $commonbanner_id;
		
                $where = array(
                           'page_link!=' => 'notfound_404'//Or 'is_active' => 1,
                           // 'id !=' => 1
                        );
                $order_by = array('title' => 'asc');
                $page_list = $this->modelcommonbanner->select_row('cms_pages',$where,$order_by);
                
                
		
                //$page_list = $this->modelcommonbanner->getDistinctPageDetails();
		$data['page_list'] = $page_list;
		
		//$data_row = $this->modelcommon_banner->getAllDetails("common_banner");
		$prg_is_array=array();
                
                $order_by = array('id' => 'asc');
                $page_id_B = $this->modelcommonbanner->select_row('commonbanner', array(),$order_by);
                
		$selected_page_id = array();
		if(!empty($page_id_B)){
		foreach($page_id_B as $pval){
			$page_id_Arr=explode(',',$pval->page_id);
			foreach($page_id_Arr as $P_id){
			$selected_page_id[]=$P_id;
			}
			}
		}
		
		
		$data['pageList'] = $this->prfileprogramselectbox($page_list,$selected_page_id,'1',$prg_is_array);
                $data['selectedprg'] = '';
		
		$this->load->view('kaizen/commonbanner/edit_commonbanner',$data);		
	}
        
        function prfileprogramselectbox($rs,$selected,$depth=0,$prg_is_array=array())
	{

		$tab='';
		for($k=0;$k<$depth;$k++)
			$tab .=	"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		if($depth > 0)
			$tab .=	"-->";	
		$str = "";

		if(!empty($rs))
		{	
			foreach($rs as $category)
			{
			
			  $count_subcategory 	= array();

			     if(!in_array($category->id,$prg_is_array)){
							
							$str .= "<option value=\"~".$category->id."~\" ";
				
							if(is_array($selected))
							{
								foreach($selected as $val)
								{
									if($val == '~'.$category->id.'~')
										$str .= " disabled ";
				
								}
							}
							else
							{
								if($selected == '~'.$category->id.'~')
									$str .= " selected ";
							
							}
							$title_opt=$tab.$category->title;
							
						 if($category->parent_id!=0){ 
                                                    $where = array(
                                                            'id' => $category->parent_id
                                                        );
                                                    $cms_detls = $this->modelcommonbanner->select_row('cms_pages',$where);
                                                    $title = '';
                                                    if(!empty($cms_detls)){
                                                        $title =  $cms_detls[0]->title;
                                                    }
                                                     $title_opt = $tab.$title.' >> '.$category->title ;
                                                     
                                                 }
						 
							$str .= ">".$title_opt."</option>\n";	
							
				 }
				
			}
			
		}
		return $str;
	   
	}
        
	public function addedit()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('commonbanner_title', 'Title', 'trim|required|xss_clean');
		
		
		$this->form_validation->set_error_delimiters('<span class="validation_msg">', '</span>');
		$id=$this->input->post('commonbanner_id','');
		if($this->form_validation->run() == TRUE) // IF MENDATORY FIELDS VALIDATION TRUE(SERVER SIDE)  
		{	
			$where = array(
                            
                            'id' => $id
                        );
                        $commonbanner_detls = $this->modelcommonbanner->select_row('commonbanner',$where);
        		if(!empty($commonbanner_detls)) 
			{
			
                               /* $uplod_img ="";
                                $orgimgpath=$commonbanner_detls[0]->banner_photo;
								$uplod_img = $this->input->post("htmlfile1",TRUE);
                                if(!empty($uplod_img) && $uplod_img!=$orgimgpath)
								{
									if(!empty($orgimgpath) && is_file(file_upload_absolute_path().'commonbanner/'.$orgimgpath))
									{
											unlink(file_upload_absolute_path().'commonbanner/'.$orgimgpath);
									}
                                } */
                                $this->commonbanner_title		=$this->input->post('commonbanner_title',TRUE);
                                $this->htmlfile1             =$this->input->post('htmlfile1',TRUE);
                                $selected_id=$this->input->post('selected_id','');
                                $this->page_id=implode(',',$selected_id).',';
                                $this->is_active		=$this->input->post('is_active',TRUE); 
                                if($this->is_active===false){
                                        $this->is_active='1';
                                }
                                
                                $update_data = array(
                                    
                                    'title' 			=> $this->commonbanner_title,
                                    'page_id' 			=> $this->page_id,
                                    'banner_photo' 		=> $this->htmlfile1,
                                    'is_active' 			=> $this->is_active
                                );
                                
                
				$update_where = array('id' => $id);
				if($this->modelcommonbanner->update_row('commonbanner',$update_data,$update_where)) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{
					$session_data = array("SUCC_MSG"  => "Common Banner Updated Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else // IF UPDATE PROCEDURE NOT EXECUTE SUCCESSFULLY
				{	
					$session_data = array("ERROR_MSG"  => "Common Banner Not Updated.");
					$this->session->set_userdata($session_data);				
				}
			}
			else 
			{ 
                               /* $uplod_img = '';	
				$uplod_img = $this->input->post("htmlfile1",TRUE); */
                                
				$this->commonbanner_title		=$this->input->post('commonbanner_title',TRUE);
                                $this->htmlfile1             =$this->input->post('htmlfile1',TRUE);
                                 $selected_id=$this->input->post('selected_id','');
                                $this->page_id=implode(',',$selected_id).',';
                                $this->is_active		=$this->input->post('is_active',TRUE); 
                                if($this->is_active===false){
                                        $this->is_active='1';
                                }
                                
                                $add_data = array(
                                    
                                    'title' 			=> $this->commonbanner_title,
                                    'page_id' 			=> $this->page_id,
                                    'banner_photo' 		=> $this->htmlfile1,
                                    'is_active' 			=> $this->is_active
                                );
                                
				$id = $this->modelcommonbanner->insert_row('commonbanner',$add_data);
				if($id) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{
					$session_data = array("SUCC_MSG"  => "Common Banner Inserted Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else // IF UPDATE PROCEDURE NOT EXECUTE SUCCESSFULLY
				{	
					$session_data = array("ERROR_MSG"  => "Common Banner Not Inserted.");
					$this->session->set_userdata($session_data);				
				}
				
			}
			redirect("kaizen/commonbanner/doedit/".$id,'refresh');			
		}
		else{
			if(!empty($id)){
			$this->doedit();
			}
			else{
				$this->doadd();
			}
		}
	}
	public function doedit()
	{
		$data = array();
		$commonbanner_id=$this->uri->segment(4); 
		$where = array(
                            'id' => $commonbanner_id
                        );
                $commonbanner_detls = $this->modelcommonbanner->select_row('commonbanner',$where);
		if($commonbanner_detls){
			$data['details'] = $commonbanner_detls[0];
		}
		else{
			$data['details']->is_active = 1;
			$data['details']->id = 0;
		}
		
		$selected_page_id_arr=explode(',',$data['details']->page_id);
                $selected_page_id = array();
                foreach($selected_page_id_arr as $select_id){
                    $select_id_ar = explode('~',$select_id);
					if(!empty($select_id_ar[1]))
                    $selected_page_id[] = $select_id_ar[1];
                }
		$page_list = $this->modelcommonbanner->getSingleRecordPageName($selected_page_id);
		$data['page_list'] =$page_list;
		$where = array(
							'page_link!=' => 'notfound_404'//Or 'is_active' => 1,
                          //  'id !=' => 1
                        );
                $order_by = array('title' => 'asc');
                $page_lists = $this->modelcommonbanner->select_row('cms_pages',$where,$order_by);
                //$page_list = $this->modelcommonbanner->getDistinctPageDetails();
		$data['page_list'] = $page_list;
		//$data_row = $this->modelcommon_banner->getAllDetails("common_banner");
		
		
		
		$prg_is_array=array();
                $order_by = array('id' => 'asc');
                $page_id_B = $this->modelcommonbanner->select_row('commonbanner', array(),$order_by);
		if(!empty($page_id_B)){
		foreach($page_id_B as $pval){
			$page_id_Arr=explode(',',$pval->page_id);
			foreach($page_id_Arr as $P_id){
			$selected_page_id[]=$P_id;
			}
			}
		}
                $allpage_list = array();
				
				// echo '<pre>';print_r($allpage_list);exit;
				
				
		$data['pageList'] = $this->prfileprogramselectbox($page_lists,$selected_page_id,'1',$prg_is_array);
		$data['selectedprg'] = $this->prfileprogramselectbox($page_list,'','1',$prg_is_array);
		$this->load->view('kaizen/commonbanner/edit_commonbanner',$data);		
	}	
}