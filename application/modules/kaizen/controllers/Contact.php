<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends MY_Controller 
{
	private $limit = 20;
	var $offset = 0;
	function __construct()
	{
		parent::__construct();		 
		if( ! $this->session->userdata('web_admin_logged_in')) {
			redirect('kaizen/welcome','refresh');
		}
		$this->load->vars( array(
		  'global' => 'Available to all views',
		  'header' => 'common/header',
		  'left' => 'common/left',
          'right' => 'common/right',
		  'footer' => 'common/footer'
		));
		$this->load->model('modelcontact');	
	}
	public function index()
	{	
		$this->dolist();	
	}
	public function dolist(){
		$data = array();
		$where = array( );
		$order_by = array('id' => 'asc');
		$data_row = $this->modelcontact->select_row('contact',$where,$order_by);
		$data['records']= $data_row;
		$this->load->view('contact/contact_list',$data);		
	}
	public function addedit()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('contact_title', 'Title', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<span class="validation_msg">', '</span>');
		$id=$this->input->post('contact_id','');
        $marker_logo='';
		if($this->form_validation->run() == TRUE) // IF MENDATORY FIELDS VALIDATION TRUE(SERVER SIDE)  
		{	
			$where = array(
                            'id' => $id
                        );
                $contact_detls = $this->modelcontact->select_row('contact',$where);
        		if(!empty($contact_detls)) 
                { 
                                $this->contact_title		=$this->input->post('contact_title',TRUE);
                                $this->fax		       =$this->input->post('fax',TRUE);
                                $this->longitude		=$this->input->post('longitude',TRUE);
                                $this->latitude                 =$this->input->post('latitude',TRUE);
                                $this->address                 =$this->input->post('address',TRUE);
                                $this->htmlfile1                =$this->input->post('htmlfile1',TRUE);
                                $this->htmlfile2                =$this->input->post('htmlfile2',TRUE);
                                $this->contact_email		=$this->input->post('contact_email',TRUE);
                                $this->contact_phone		=$this->input->post('contact_phone',TRUE);
				$this->work_time		=$this->input->post('work_time',TRUE);
                               //$this->copy_righy		$this=$this->input->post('copy_righy',TRUE);

                              
                                $update_data = array(
                                    'title'                             => $this->contact_title,
                                    'longitude' 			          => $this->longitude,
                                    'latitude'                          => $this->latitude,
                                    'address'                          => $this->address,
                                    'logo_photo'                      => $this->htmlfile1, 
                                    'image'                            => $this->htmlfile2, 
                                    'contact_email' 			=> $this->contact_email,
                                    'contact_phone' 			=> $this->contact_phone,
                                    'fax' 			         => $this->fax,
			         'work_time' 			         => $this->work_time,
									//'copy_right' 			         =>   $this->copy_righy
                                );
                         
			        //pre($update_data);exit;     
								
				$update_where = array('id' => $id);
				if($this->modelcontact->update_row('contact',$update_data,$update_where))
				{
					$session_data = array("SUCC_MSG"  => "Contact Updated Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else 
				{	
					$session_data = array("ERROR_MSG"  => "Contact Not Updated.");
					$this->session->set_userdata($session_data);				
				}
			}
			redirect("kaizen/contact/doedit/".$id,'refresh');			
		}
		else{
			if(!empty($id)){
			$this->doedit();
			}
			else{
				$this->doadd();
			}
		}
	}
	public function doedit()
	{
		$data = array();
		$contact_id=$this->uri->segment(4); 
		
		$where = array('id' => $contact_id );
        $contact_detls = $this->modelcontact->select_row('contact',$where);
		if($contact_detls){
			$data['details'] = $contact_detls[0];
		}
		else{
			//$data['details']->is_active = 1;
			$data['details']->id = 0;
		}
	    $where = array('is_active2' =>1);
        $order_by1 = array('display_order'=>'desc');
		$data['content_list']= $this->modelcontact->select_row('contact_subjects',array(),$order_by1); 
               // pre($data['content_list']);
		$this->load->view('contact/edit_contact',$data);		
	}
	
	   public function deletesubjects(){
		$count = $this->input->post("hidden_id");
                $this->db->where('id', $count);
                $this->db->delete('contact_subjects'); 
	}
	
        public function add_subjects(){		 
		$data = array();
		$count = $this->input->post("count");
                $id = $this->input->post("id");
                $pid=$this->input->post("pid");
                if(!empty($id)){
                    $where=array('id'=>$id);
                    $product_dtls=$this->modelcontact->selectOne('contact_subjects',$where);
                    $data['id'] = $id;
                    $data['count'] = $count;
                    $data['title'] = $product_dtls->title;
                    $data['order'] = $product_dtls->display_order;
                    $data['is_active2'] = $product_dtls->is_active2;
                }else{
                    $data['count'] = $count;
                    $data['id'] = $id;
                    $data['is_active2'] =1;
                }
		$this->load->view('kaizen/contact/add_subjects',$data);				
	}
	
}
