<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header); ?>  
<script type="text/javascript">
  var base_url='<?php echo base_url(); ?>';
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/clndr.css'); ?>" />
<script type='text/javascript' src='<?php echo base_url('public/default/js/underscore-min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/moment.min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/clndr_news.js'); ?>'></script>
<!--content section start-->
<script type="text/javascript">
    function showhidediv(year,month){
        if(year==''){
            $("#month_ul").html('');
        }else{
            $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url("news/getMonth");?>',
                data: {year:year,month:month},
                success: function(data){ 
                     $("#month_ul").html(data);
                },
            });
        }
    }
    $(document).ready( function() {
     var lotsOfMixedEvents = [
	  <?php foreach($news_list as $events) { 
          $where=array('id'=>$events->category_id);
          $cat_dtls=$this->model_news->selectOne('blog_category',$where);
          ?>
    {
        date: '<?php echo date('Y-m-d',strtotime($events->post_dt)); ?>',
        link_color:'<?php echo $cat_dtls->category_color; ?>',
        title: 'News'
    }, 
	<?php } ?>
	
];

$('.cal1').clndr({
    events: lotsOfMixedEvents,	
    multiDayEvents: {
        endDate: 'end',
        singleDay: 'date',
        startDate: 'start'
    },
	showAdjacentMonths: true,
    adjacentDaysChangeMonth: false
});



});
</script>
    <div class="content">
    	<div class="wrapper">
            <!--left section start-->
            <div class="inner-left">
                <ul class="news-list event-detail">
                	<li class="community">
                            <?php if(!empty($news_details->post_dt)){ ?>
                    	<div class="event-date blue-dark">
                            <span class="cal-date"><?php echo date('d',strtotime($news_details->post_dt)); ?></span>
                            <span class="month"><?php echo date('M',strtotime($news_details->post_dt)); ?></span>
                            <span class="year"><?php echo date('Y',strtotime($news_details->post_dt)); ?></span>
                       	</div>
                            <?php } ?>
                        <div class="event-right">
                            <h5><a href="events-detail.html"><?php echo $news_details->title; ?></a></h5>
                            <?php if(!empty($news_details->description)) echo outputEscapeString($news_details->description); ?>
                            <a href="<?php echo site_url('news'); ?>" class="back-btn">Back</a>
                        </div>
                    </li>
                </ul>
            </div>
            <!--left section end-->
            
            
            <!--right section start-->
            <div class="inner-right">
            	<div class="right-block noborder">
                	<div class="calendar">
                        <div class="cal1"></div>
                    </div>
                </div>
                <?php if(!empty($category_list)){ ?>
            	<div class="right-block noborder">
                	<h3>Categories</h3>
                	<ul class="right-menu">
                            <?php foreach($category_list as $cat){ ?>
                    	<li><a href="<?php echo site_url('news?cat='.$cat->page_link); ?>" <?php if(!empty($cat_link) && $cat_link==$cat->page_link) echo 'class="active"'; ?>><?php echo $cat->title; ?> </a></li>
                            <?php } ?>
                    </ul>
                    <div class="clear"></div>
                </div>
                <?php } ?>
                <?php //pre($year_list); ?>
                <?php if(!empty($year_list)) { ?>
                <div class="right-block noborder">
                	<h3>Archive</h3>
                    <div class="archive-year">
                    	<form method="post">
                        	<label>Year:</label>
                            <select onChange="showhidediv(this.value)" id="year">
                                <option value="">Select Year</option>
                                <?php foreach($year_list as $year){ ?>
                            	<option value="<?php echo $year->year; ?>"><?php echo $year->year; ?></option>
                                <?php } ?>
                            </select>
                        </form>
                    </div>
                	<ul class="right-menu" id="month_ul">
                    </ul>
                    <div class="clear"></div>
                </div>
                <?php } ?>
            </div>
            <!--right section end-->
            
        </div>
    </div>
    <!--content section end-->
<?php $this->load->view($footer);