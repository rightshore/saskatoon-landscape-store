<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header);
?>

<!--content section start-->
	<div class="home-products">
		<div class="wrapper">
			<?php if(!empty($prod_list)){ ?>
			<h2>Featured Products</h2>
			<ul>
				<?php foreach($prod_list as $list){ ?>
				<li>
                <?php if(!empty($list->image) && is_file(file_upload_path($list->image))){ ?>
					<div class="image">
						<a href="<?php echo site_url('products/details/'.$list->page_link); ?>"><img src="<?php echo $list->image; ?>" alt="" title=""></a>
					</div>
                    <?php } ?>
					<div class="detail">
						<h4><a href="<?php echo site_url('products/details/'.$list->page_link); ?>"><?php echo $list->title; ?></a></h4>
						<?php if(!empty($list->excerpt)) echo '<p>'.$list->excerpt.'</p>'; ?>
						<a href="<?php echo site_url('products/details/'.$list->page_link); ?>" class="btn">More</a>
						<div class="clear"></div>
					</div>
				</li>  
				<?php } ?>
			</ul>
			<div class="clear"></div>
			<a href="<?php echo site_url('products'); ?>" class="view-btn">View All</a>
			<?php } ?>
		</div>
	</div>
	<!--content section end-->
    
     <!-- about section start -->
     
     <div class="about-section">
		<div class="wrapper">	
        	<h2><?php echo $home_block->title; ?></h2>
			<div class="about-text">
            	<h3><?php echo $home_block->sub_title; ?></h3>
				<?php echo $home_block->content; ?>
                <!--<a href="about.html" class="view-btn res">Read More</a>-->
			</div>
                <?php if(!empty($home_block->image2) && is_file(file_upload_path($home_block->image2))){ ?>
			<div class="about-pic">
				<img src="<?php echo $home_block->image2; ?>" alt="" title="">
			</div>
                <?php } ?>
            <div class="clear"></div>
            
            
            <?php 
                                
                       if(!empty($home_block->button_type) && $home_block->button_type==1 && !empty($home_block->page_link)){
                             $hplinks = $this->model_home->selectOne('cms_pages',array('id'=>$home_block->page_link));
                                    $homeblockurl=urlByPageLink($hplinks->page_link);
                        }elseif(!empty($home_block->button_type) && $home_block->button_type==2 && !empty($home_block->banner_url)){
                                    $homeblockurl=$home_block->banner_url;
                                }?>
            
            
            <a href="<?php if(!empty($homeblockurl)){ echo $homeblockurl;} else { echo 'javascript:void(0)';} ?>" class="view-btn"><?php echo $home_block->button_text; ?></a>
		</div>
		<span class="about-back" style="background: url(<?php echo $home_block->image1;?>) no-repeat center 0;"></span>
	</div>
     
    <!--<div class="about-section">
		<div class="wrapper">	
        	<h2>About Us</h2>
			<div class="about-text">
            	<h3>Who We Are</h3>
				<h6><span>The Saskatoon Landscape Store</span> is a local, family owned landscape supply store. <strong>Our goal is to provide exceptional service and product to both contractors and home owners seeking to create a brand new landscape or improve upon existing yards.</strong> Our products are always high quality and reliable for your landscaping needs.</h6>
				<p>We supply bulk products such as aggregate, mulch, and soil, as well as retaining wall bricks and patio stones. We also carry a wide variety of sprinkler parts to help you build your own underground sprinkler system or to repair your current one. </p>
                <a href="about.html" class="view-btn res">Read More</a>
			</div>
			<div class="about-pic">
				<img src="<?php echo base_url('public/default/images/'); ?>/about_pic2.jpg" alt="" title="">
			</div>
            <div class="clear"></div>
            <a href="about.html" class="view-btn">Read More</a>
		</div>
		<span class="about-back"></span>
	</div>-->
    <!-- about section end --> 
	

<?php
$this->load->view($footer);/*end*/
