<?php  echo $this->load->view($header); ?>
<!--content section start-->
    <div class="content">
    	
        	<!--right section start-->
            <div class="wrapper">
            	<?php echo $this->load->view($right); ?>         
            </div>
            <!--right section end-->
            <?php $banner_photo= '';
                  if($this->data['hooks_meta']->about_us_photo!=''){	
                  $path2 = str_replace(base_url().'public/uploads/',file_upload_absolute_path(),$this->data['hooks_meta']->about_us_photo);
                  if(is_file($path2)) { $banner_photo=$this->data['hooks_meta']->about_us_photo; }} ?>
            <?php if($banner_photo!='') { ?>
            <img src="<?php echo $banner_photo; ?>" alt="" title="" class="home-about-plain slide-left">
            <div class="clear"></div>
            <?php } ?>	   
           
            <!--left section start-->
           <div class="wrapper">
            	<?php if(!empty($this->data['hooks_meta']->content)) echo $this->data['hooks_meta']->content; ?>
           </div>
            <!--left section end-->
       
      </div>
    <!--content section end-->
<?php echo $this->load->view($footer); ?>