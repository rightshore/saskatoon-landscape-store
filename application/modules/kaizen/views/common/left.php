<?php 
$fetch_class =  $this->router->fetch_class();
//pre($fetch_class);exit;
$comp_arr = array(
                    'gallery' => 'gallery',
					'products' => 'products',
					'contact' => 'contact',
					'category' => 'category',
                                        'commonbanner' => 'commonbanner',
					'home_block' => 'home_block'
                    
            );
			
$active_cls = '';
if(in_array($fetch_class,$comp_arr)){
    $active_cls = "active";
}
$comp_arr2 = array(
                    'blog' => 'blog',
                    'blog_category' => 'blog_category',
                    'blog_comments' => 'blog_comments'
            );
			
$active_cls2 = '';
if(in_array($fetch_class,$comp_arr2)){
    $active_cls2 = "active";
}

?>

<div class="leftDiv">
  <ul class="cat-list">
  
    <li class="listing-dashboard"> 
    <a href="<?php echo site_url("kaizen/main/"); ?>" <?php if(!empty($fetch_class) && $fetch_class == "main"){ ?> class="active" <?php } ?>  >Dashboard</a> 
    </li>
    
    <li class="listing-web"> 
    <a href="<?php echo site_url("kaizen/pages"); ?>" <?php if(!empty($fetch_class) && $fetch_class == "pages"){ ?> class="active" <?php } ?>>Pages</a> 
    </li>
    
<!--    <li class="listing-web"> 
    <a href="<?php echo site_url("kaizen/settings/");?>" class="setting <?php if(!empty($fetch_class) && $fetch_class == "settings"){ echo "active" ; } ?>">Website Settings</a> 
    </li>-->
    
    <li class="listing-communication"> <a href="javascript:void(0)">Components<span class="listing-arrow"></span></a>
      <div style="<?php if(!empty($active_cls)){ echo 'display:block;'; }else{echo 'display:block;';}?>" class="cat-cont">
        <ul class="category_sub_list">
             <li><a href="<?php echo site_url("kaizen/commonbanner"); ?>" class="<?php if(!empty($active_cls) && ($fetch_class == "commonbanner")){ echo "active" ; } ?>">Common Banner</a></li>
          <li><a href="<?php echo site_url("kaizen/gallery"); ?>" class="<?php if(!empty($active_cls) && ($fetch_class == "gallery")){ echo "active" ; } ?>">Gallery</a></li>
          <li><a href="<?php echo site_url("kaizen/products"); ?>" class="<?php if(!empty($active_cls) && ($fetch_class == "products"||$fetch_class == "category")){ echo "active" ; } ?>">Products</a></li>
          
          <li><a href="<?php echo site_url("kaizen/contact"); ?>" class="<?php if(!empty($active_cls) && ($fetch_class == "contact")){ echo "active" ; } ?>">Contact</a></li>
          
           <li><a href="<?php echo site_url("kaizen/home_block"); ?>" class="<?php if(!empty($active_cls) && ($fetch_class == "home_block")){ echo "active" ; } ?>">Home Block</a></li>
        
        </ul>
      </div>
    </li>
<!--     <li class="listing-members"> <a href="javascript:void(0)">Blog<span class="listing-arrow"></span></a>
      <div style="<?php if(!empty($active_cls2)){ echo 'display:block;'; }?>" class="cat-cont">
        <ul class="category_sub_list">
          <li><a href="<?php echo site_url("kaizen/blog"); ?>" class="<?php if(!empty($active_cls2) && ($fetch_class == "blog")){ echo "active" ; } ?>">Blog</a></li>
          
        </ul>
      </div>
    </li>-->
  </ul>
</div>

<!--left column end-->