<?php
class Modelsite_map extends MY_Model{
        public function __construct()
        {
            parent::__construct();	
            $this->site_id=$this->session->userdata('SITE_ID');		
        }
    
        public function getallcmspages(){
            $this->db->select('id,title,parent_id,page_link,display_order');
            $this->db->from($this->db->dbprefix('cms_pages'));
            $this->db->where('site_id',1);
            $this->db->where('is_active',1);
            $st="id != 135";
            $this->db->where($st, NULL, FALSE);  
            $this->db->order_by('display_order','asc');
            $edit_query = $this->db->get();
            if($edit_query)
            {
                    $edit_res=$edit_query->result();		
                    return $edit_res;
            }
            else
            {
                    //log_message('error',": ".$this->db->_error_message() );
                    return false;
            }
        }
}
?>