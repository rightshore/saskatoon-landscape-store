<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gallery extends CI_Controller {
	var $data = array();
	public function __construct() {
	   parent::__construct();
	   $this->load->vars( array(		  
		  'header' => 'common/header',
		  'footer' => 'common/footer'
		));		
		$this->load->model('gallery/model_gallery');		
	}

        public function index(){
            $this->data['gallery_list'] = $this->model_gallery->select_row('gallery',array('is_active'=>1),array('display_order'=>'asc'),8);
            $this->data['next_gallery_list'] = $this->model_gallery->load_more(8);
            $this->load->view('gallery',$this->data);
        }
        
        public function load_more(){
            $limit=$this->input->post('val');
            $this->data['gallery_list'] = $this->model_gallery->load_more($limit);
            $this->data['next_gallery_list'] = $this->model_gallery->load_more($limit+8);
            $next='1';
            if(!empty($this->data['next_gallery_list'])){
                $next='2';
            }
            if(!empty($this->data['gallery_list'])){
                $html=$this->load->view('load_more',$this->data,true);
                echo $html.'_=_'.$next;
            }else{
                echo 0;
            }
        }
		
}
