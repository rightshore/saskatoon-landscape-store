<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common extends CI_Controller {
	public function __construct() {
	   parent::__construct();
	    $this->load->vars( array(		  
		    'call_to_action' => 'call_to_action'
		));
	$this->load->model('common/model_common');
	}
    public function google_map(){ 
                $id = $this->uri->segment(3);
                $this->data['logo_photo'] = $this->data['contact']->logo_photo;
                $this->data['default_lat'] = $this->data['contact']->latitude;
                $this->data['default_lng'] = $this->data['contact']->longitude;
                $this->data['id'] = $id;
                //pre($this->data);
                $this->load->view('common/google_map_dealers_v3',$this->data);
    }
    public function google_map_xml(){  
            $this->data['default_lat'] = $this->data['contact']->latitude;
            $this->data['default_lng'] = $this->data['contact']->longitude;
            $this->data['title'] = $this->data['contact']->title;
            $this->data['address'] = $this->data['contact']->address;
            $this->load->view('common/google_map_xml',$this->data);
             }
			
	

		
		public function contact_form(){
                  //  pre($_POST);exit;
		$name	=	$this->input->post('name',TRUE);
		$email		=	$this->input->post('email',TRUE);
		//$phone		=	$this->input->post('phone',TRUE);
		//$subject		=	$this->input->post('subject',TRUE);
		$comments	=	$this->input->post('comments',TRUE);
		$err = array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|xss_clean');
		$this->form_validation->set_rules('comments', 'Comments', 'trim|required|xss_clean');
		if($this->form_validation->run() == TRUE)
		{
//				       $this->load->library('email');
//					$config['wordwrap'] = TRUE;
//					$config['validate'] = TRUE;
//					$config['mailtype'] = 'html';
//					$this->email->initialize($config);
					$message ='<!DOCTYPE HTML>
					<html>
					<head>
					<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<title>'.$this->config->item("COMPANY_NAME").'</title>
					</head>					
					<body>
					<table align="center" width="600" border="1" cellpadding="4" cellspacing="0">
					<tr><td><b>Name:</b></td><td>'.stripslashes($name).'</td></tr>
					<tr><td><b>Email:</b></td><td>'.$email.'</td></tr>';
					
                                        
                    if(!empty($comments)){
                        $message.='<tr><td valign="top"><b>Comments: </b></td><td>'.$comments.'</td></tr>';
					}
                                        
					$message.='</table>
					</body>
					</html>';
					//$this->email->from($email, $name);
                                       // $this->email->to($this->data['site_settings']->contact_email);
                                       // $this->email->reply_to($email);
					//$this->email->subject('Contact from '.outputEscapeString($this->config->item("COMPANY_NAME")));
					//$this->email->message($message);
                                        $to = $this->data['site_settings']->contact_email;
                                        $from = $email;
                                        $subject = 'Contact from '.outputEscapeString($this->config->item("COMPANY_NAME"));    
                                        $message = $message;
                                        $files = ''; 
                                        common_email_send($to,$from,$subject,$message,$files);
					//$this->email->send();
					$err =  '';
					echo json_encode($err);
		}
		else{
			 $err['name'] = form_error('name');
			 $err['phone'] = form_error('phone');
			 $err['email'] = form_error('email');
			 $err['comments'] = form_error('comments');
			 echo json_encode($err);
			}
	}
	
	
}