<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pages extends MY_Controller 
{
	private $limit = 20;
	var $offset = 0;
	function __construct()
	{
		parent::__construct();		 
		if( ! $this->session->userdata('web_admin_logged_in')) {
			redirect('kaizen/welcome','refresh');
		}
		$this->load->vars( array(
		  'global' => 'Available to all views',
		  'header' => 'common/header',
		  'left' => 'common/left',
		  'footer' => 'common/footer',
		  'right' => 'common/right'
		));
		
		$this->load->model('modelpages');	
	}
	public function index()
	{	
		$this->dolist();	
	}
	public function dolist(){
		$data = array();
		$parent_id = $this->input->get("parent_id");
                
                if(!empty($parent_id)){
                     $where = array('parent_id' => $parent_id);
                }else{
                     $where = array();
                }
       
                $order_by = array('title' => 'asc');
		$data_row = $this->modelpages->select_row('cms_pages',$where,$order_by);
		
                $rest_arr = $data_row;
		#echo $this->db->last_query();
                
                if(!empty($parent_id)){
                    $where_ar = array('id' => $parent_id);
                    $parebt_rec = $this->modelpages->select_row('cms_pages',$where_ar);
                    if(!empty($data_row)){
                        foreach($data_row as $data_ind){
                                $where = array('parent_id' => $data_ind->id);
                                $order_by = array('title' => 'asc');
                                $data_row_sub = $this->modelpages->select_row('cms_pages',$where,$order_by);
                                
                                
                                if(!empty($data_row_sub)){
                                    $rest_arr = array_merge($rest_arr,$data_row_sub);
                                    foreach($data_row_sub as $data_sub_sub){
                                        $where = array('parent_id' => $data_sub_sub->id);
                                        $order_by = array('title' => 'asc');
                                        $data_row_sub_sub = $this->modelpages->select_row('cms_pages',$where,$order_by);
                                        
                                        if(!empty($data_row_sub_sub)){
                                            $rest_arr = array_merge($rest_arr,$data_row_sub_sub);
                                            foreach($data_row_sub_sub as $data_sub_sub_sub){
                                                $where = array('parent_id' => $data_sub_sub_sub->id);
                                                $order_by = array('title' => 'asc');
                                                $data_row_sub_sub_sub = $this->modelpages->select_row('cms_pages',$where,$order_by);
                                                if(!empty($data_row_sub_sub_sub)){
                                                    $rest_arr = array_merge($rest_arr,$data_row_sub_sub_sub);
                                                }
                                            }
                                        }
                                    }
                                }
                        }
                    }
                   $data_row = array_merge($rest_arr,$parebt_rec);
                    
                }
//                pre($data_row);
//                if(!empty($parebt_rec)){
//                    $data_row = array_merge($data_row,$parebt_rec);
//                }
//                pre($data_row);
                
                $data['records']= $data_row;
                
                $wherepg = array();
                $order_bypg = array('title' =>'asc');
		$data['page_list']= $this->modelpages->select_row('cms_pages',$wherepg,$order_bypg);
                //pre($data['page_list']);
                $data['selected'] = $parent_id;
//                print_r($data);
                $this->load->view('pages/pages_list',$data);		
	}
	
	public function doadd(){
		$data = array();
        $data['details']= new stdClass;
		$pages_id=$this->uri->segment(4);
		$data['details']->is_active = 1;
		$data['details']->id = $pages_id;
		$wherepg = array( 'is_active' =>1);
        $order_bypg = array('title' => 'asc');
		$data['page_list']= $this->modelpages->select_row('cms_pages',$wherepg,$order_bypg);
		$this->load->view('pages/edit_pages',$data);		
	}
	
	public function popup()
	{
		
		$count =  $this->uri->segment(4);
		$pos =  $this->uri->segment(5);
		$this->data['folder_name'] = $count;
		$this->data['pos'] = $pos;
		$this->data['imgwidth'] = '1140';
		$this->data['imgheight'] = '698';
		$this->data['session'] = $this->uri->segment(5); 
		$this->load->view('upload_product_crop',$this->data);
	}
        
	public function addedit()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('pages_title', 'Title', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<span class="validation_msg">', '</span>');
		$id=$this->input->post('pages_id','');
        $pages_title ='';
		if($this->form_validation->run() == TRUE) // IF MENDATORY FIELDS VALIDATION TRUE(SERVER SIDE)  
		{	
			$where = array('id' => $id);
            $pages_detls = $this->modelpages->select_row('cms_pages',$where);
        	if(!empty($pages_detls)) 
			{
                                $this->pages_title		=$this->input->post('pages_title',TRUE);
                                $this->htmlfile1             =$this->input->post('htmlfile1',TRUE);
                                $this->htmlfile8             =$this->input->post('htmlfile8',TRUE);
								$this->htmlfile9             =$this->input->post('htmlfile9',TRUE);
                                $this->parent_id		=$this->input->post('parent_id',TRUE);
                                $this->meta_title		=$this->input->post('meta_title',TRUE);
                                $this->shown_in_banner		=$this->input->post('shown_in_banner',TRUE);
                                $this->external_link		=$this->input->post('external_link',TRUE);
                                 $this->shown_in_top		=$this->input->post('shown_in_top',TRUE);      
                                 $this->shown_in_footer		=$this->input->post('shown_in_footer',TRUE);
                                 $this->shown_in_side		=$this->input->post('shown_in_side',TRUE);
                                 $this->htmlfile5		=$this->input->post('htmlfile5',TRUE);
							    $this->display_order	=$this->input->post('display_order',TRUE);
                                $this->meta_keyword		=$this->input->post('meta_keyword','');
                                $this->meta_description	=$this->input->post('meta_desc','');  
                                $this->content          =$this->input->post('content');
								$this->content2          =$this->input->post('content2');
								$this->content3          =$this->input->post('content3');
								$this->content4          =$this->input->post('content4');
                                $this->is_active		=$this->input->post('is_active',TRUE); 
								
								
                                if($this->is_active===false){
                                        $this->is_active='1';
                                }
                                $update_data = array(
                                    'title' 			=> $this->pages_title,
                                    'banner_photo'                  => $this->htmlfile1, 
                                    'about_us_photo'                  => $this->htmlfile8, 
									'about_us_photo2'                  => $this->htmlfile9, 
				   					'parent_id' 			=> $this->parent_id,
                                    'content' 			=> $this->content,
									'content2' 			=> $this->content2,
									'content3' 			=> $this->content3,
									'content4' 			=> $this->content4,
				   					'external_link'   => $this->external_link,
                                    'shown_in_top' 	=> $this->shown_in_top, 
                                    'shown_in_footer' 	=> $this->shown_in_footer,
                                    /*'shown_in_banner'   => $this->shown_in_banner,
                                    'shown_in_top' 	=> $this->shown_in_top, 
                                    'shown_in_footer' 	=> $this->shown_in_footer, 
                                    'shown_in_side' 	=> $this->shown_in_side, */
                                    'display_order' 	=> $this->display_order, 
                                    'com_cat_img' 	=> $this->htmlfile5, 
                                    'meta_title' 		=> $this->meta_title ,				
                                    'meta_keyword'		=> $this->meta_keyword,   					
                                    'meta_description' 	=> $this->meta_description,  
                                    'is_active' 			=> $this->is_active
									
                                );
                              
				$update_where = array('id' => $id);
				if($this->modelpages->update_row('cms_pages',$update_data,$update_where)) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{
					$session_data = array("SUCC_MSG"  => "Pages Updated Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else // IF UPDATE PROCEDURE NOT EXECUTE SUCCESSFULLY
				{	
					$session_data = array("ERROR_MSG"  => "Pages Not Updated.");
					$this->session->set_userdata($session_data);				
				}
			}
			else 
			{ 
                                $this->pages_title		=$this->input->post('pages_title',TRUE);
                                $this->htmlfile1             =$this->input->post('htmlfile1',TRUE);
                                $this->htmlfile8             =$this->input->post('htmlfile8',TRUE);
                                $this->parent_id		=$this->input->post('parent_id',TRUE);
                                $this->meta_title		=$this->input->post('meta_title',TRUE);
                                $this->shown_in_banner		=$this->input->post('shown_in_banner',TRUE);
                                $this->external_link		=$this->input->post('external_link',TRUE);
                                 $this->shown_in_top		=$this->input->post('shown_in_top',TRUE);
                                 $this->htmlfile5		=$this->input->post('htmlfile5',TRUE);
                                 $this->shown_in_footer		=$this->input->post('shown_in_footer',TRUE);
                                 $this->shown_in_side		=$this->input->post('shown_in_side',TRUE);
                                $this->display_order	=$this->input->post('display_order',TRUE);
                                $this->meta_keyword		=$this->input->post('meta_keyword','');
                                $this->meta_description	=$this->input->post('meta_desc','');  
                                $this->content          =$this->input->post('content');
				$this->content2          =$this->input->post('content2');
				$this->content3          =$this->input->post('content3');
				$this->content4          =$this->input->post('content4');
                                
                                $this->is_active		=$this->input->post('is_active',TRUE); 
								
								
							
                                if($this->is_active===false){
                                        $this->is_active='1';
                                }
                                
                                $add_data = array(
                                    'title' 			=> $this->pages_title,
                                    'banner_photo'                  => $this->htmlfile1, 
                                    'about_us_photo'                  => $this->htmlfile8, 
				   					'parent_id' 			=> $this->parent_id,
                                    'content' 			=> $this->content,
									'content2' 			=> $this->content2,
									'content3' 			=> $this->content3,
									'content4' 			=> $this->content4,
				   					'external_link'   => $this->external_link,
                                    'shown_in_top' 	=> $this->shown_in_top, 
                                    'shown_in_footer' 	=> $this->shown_in_footer,
                                    /*'shown_in_top' 	=> $this->shown_in_top, 
                                    'shown_in_footer' 	=> $this->shown_in_footer, 
                                    'shown_in_banner' 	=> $this->shown_in_banner, 
                                    'shown_in_side' 	=> $this->shown_in_side, */
                                    'display_order' 	=> $this->display_order, 
                                    'meta_title' 		=> $this->meta_title ,	
                                    'com_cat_img' 	=> $this->htmlfile5, 
                                    'meta_keyword'		=> $this->meta_keyword,   					
                                    'meta_description' 	=> $this->meta_description,
                                    'page_link' 			=> name_replaceCat('cms_pages',$this->pages_title),   
                                    'is_active' 			=> $this->is_active
									
                                );
				$id = $this->modelpages->insert_row('cms_pages',$add_data);
                     
				if($id)
				{	
					$session_data = array("SUCC_MSG"  => "Pages Inserted Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else 
				{	
					$session_data = array("ERROR_MSG"  => "Pages Not Inserted.");
					$this->session->set_userdata($session_data);				
				}
			}
			redirect("kaizen/pages/doedit/".$id,'refresh');			
		}
		else{
			if(!empty($id)){
			$this->doedit();
			}
			else{
				$this->doadd();
			}
		}
	}
	public function doedit($id)
	{
		$data = array();
		$pages_id=$this->uri->segment(4); 
		$where = array( 'id' => $pages_id);
        $pages_detls = $this->modelpages->select_row('cms_pages',$where);            
		if($pages_detls){
			$data['details'] = $pages_detls[0];
		}
		else{
			$data['details']->is_active = 1;
			$data['details']->id = 0;
		}
		$wherepg = array( 'is_active' =>1);
        $order_bypg = array('title' => 'asc');
		$data['page_list']= $this->modelpages->select_row('cms_pages',$wherepg,$order_bypg);
		
		$this->load->view('pages/edit_pages',$data);		
	}
}