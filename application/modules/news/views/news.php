<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header); ?>  
<script type="text/javascript">
  var base_url='<?php echo base_url(); ?>';
</script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/clndr.css'); ?>" />
<script type='text/javascript' src='<?php echo base_url('public/default/js/underscore-min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/moment.min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/clndr_news.js'); ?>'></script>
<!--content section start-->
<script type="text/javascript">
    function showhidediv(year,month){
        if(year==''){
            $("#month_ul").html('');
        }else{
            $.ajax({
                type: "POST",
                async: false,
                url: '<?php echo base_url("news/getMonth");?>',
                data: {year:year,month:month},
                success: function(data){ 
                     $("#month_ul").html(data);
                },
            });
        }
    }
    $(document).ready( function() {
     var lotsOfMixedEvents = [
	  <?php foreach($news_list as $events) { 
          $where=array('id'=>$events->category_id);
          $cat_dtls=$this->model_news->selectOne('blog_category',$where);
          ?>
    {
        date: '<?php echo date('Y-m-d',strtotime($events->post_dt)); ?>',
        link_color:'<?php echo $cat_dtls->category_color; ?>',
        title: 'News'
    }, 
	<?php } ?>
	
];

$('.cal1').clndr({
    events: lotsOfMixedEvents,	
    multiDayEvents: {
        endDate: 'end',
        singleDay: 'date',
        startDate: 'start'
    },
	showAdjacentMonths: true,
    adjacentDaysChangeMonth: false
});



});
</script>
    <div class="content">
    	<div class="wrapper">
            <!--left section start-->
            <div class="inner-left">
                <?php if(!empty($category)){ ?>
                <ul class="tabmenu">
                    <li><a href="#" class="<?php echo $category->page_link; ?>" style="background-color: <?php echo $category->category_color; ?>;"><?php echo $category->title; ?></a></li>
                </ul>
                <?php }elseif(!empty($category_list)){ ?>
            	<ul class="tabmenu">
                    <li class="active"><a href="#" class="all">All</a></li>
                    <?php foreach($category_list as $cat){ ?>
                    <li><a href="#" class="<?php echo $cat->page_link; ?>" style="background-color: <?php echo $cat->category_color; ?>;"><?php echo $cat->title; ?></a></li>
                <?php } }?>
                </ul>
                <?php if(!empty($category_list) || !empty($category)){ ?>
                <div class="tabcont">
                	<ul class="news-list">
                            <?php foreach($blog_list as $blog) {
                            $where=array('id'=>$blog->category_id);
                            $cat_dtls=$this->model_news->selectOne('blog_category',$where);
                            ?>
                    	<li class="<?php echo $cat_dtls->page_link; ?>">
                            <?php if(!empty($blog->post_dt) && $blog->post_dt!='0000-00-00 00:00:00') {  ?>
                            <div class="event-date" style="background-color: <?php echo $cat_dtls->category_color; ?>;">
                            	<span class="cal-date"><?php echo date('d',strtotime($blog->post_dt)); ?></span>
                                <span class="month"><?php echo date('M',strtotime($blog->post_dt)); ?></span>
                                <span class="year"><?php echo date('Y',strtotime($blog->post_dt)); ?></span>
                            </div>
                            <?php } ?>
                            <div class="event-right">
                                <h5><a href="<?php echo site_url('news/details/'.$blog->page_link); ?>"><?php echo $blog->title; ?></a></h5>
                                <?php if(!empty($blog->excerpt)){ ?><p> <?php echo $blog->excerpt; ?> <a href="<?php echo site_url('news/details/'.$blog->page_link); ?>" class="more">[MORe]</a></p><?php } ?>
                            </div>
                        </li>
                            <?php } ?>
                    </ul>
		</div>
                <?php } ?>
                <?php if(!empty($category) || !empty($selected_date)){ ?>
                <a href="<?php echo site_url('news'); ?>" class="back-btn">View All News</a>
                <?php } ?>
			</div>
                
            <!--left section end-->
            
            
            <!--right section start-->
            <div class="inner-right">
            	<div class="right-block noborder">
                	<div class="calendar">
                        <div class="cal1"></div>
                    </div>
                </div>
                <?php if(!empty($category_list)){ ?>
            	<div class="right-block noborder">
                	<h3>Categories</h3>
                	<ul class="right-menu">
                            <?php foreach($category_list as $cat){ ?>
                    	<li><a href="<?php echo site_url('news?cat='.$cat->page_link); ?>" <?php if(!empty($cat_link) && $cat_link==$cat->page_link) echo 'class="active"'; ?>><?php echo $cat->title; ?> </a></li>
                            <?php } ?>
                    </ul>
                    <div class="clear"></div>
                </div>
                <?php } ?>
                <?php //pre($year_list); ?>
                <?php if(!empty($year_list)) { ?>
                <div class="right-block noborder">
                	<h3>Archive</h3>
                    <div class="archive-year">
                    	<form method="post">
                        	<label>Year:</label>
                            <select onChange="showhidediv(this.value)" id="year">
                                <option value="">Select Year</option>
                                <?php foreach($year_list as $year){ ?>
                            	<option value="<?php echo $year->year; ?>" <?php if(!empty($sel_year)) echo 'selected'; ?>><?php echo $year->year; ?></option>
                                <?php } ?>
                            </select>
                        </form>
                    </div>
                    <ul class="right-menu" id="month_ul">
                    </ul>
                        <?php if(!empty($sel_year) && !empty($sel_month)){ ?>
                                <script type="text/javascript">
                                showhidediv('<?php echo $sel_year; ?>','<?php echo $sel_month; ?>');
                                </script>
                                <?php } ?>
                    <div class="clear"></div>
                </div>
                <?php } ?>
            </div>
            <!--right section end-->
            
        </div>
    </div>
    <!--content section end-->
<?php $this->load->view($footer);