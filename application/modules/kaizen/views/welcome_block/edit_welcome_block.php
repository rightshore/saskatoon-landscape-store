<?php //pre($page_list); ?>
<?php $this->load->view($header); ?>
<?php $this->load->view($left); ?>
<script type="text/javascript" src="<?php echo site_url("public/ckeditor/ckeditor.js");?>"></script>
<link rel="stylesheet" href="<?php echo site_url("public/validator/css/validationEngine.jquery.css");?>" type="text/css"/>
<script src="<?php echo site_url("public/validator/js/languages/jquery.validationEngine-en.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo site_url("public/validator/js/jquery.validationEngine.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url("public/js/jquery.fancybox.js");?>" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url("public/css/jquery.fancybox.css");?>" type="text/css"/>
<script>
$('.fancybox').fancybox();
</script>
<script type="text/javascript">
    $('.fancybox').fancybox({
  width:'1200',
  height: '800'
});
$(document).ready(function(){
	$("#cont").validationEngine();
	});
function form_submit(){	
    $('#cont').submit();
}

</script>
<script type="text/javascript">
     $(function() {
     $('.iframe-btn').fancybox({	
      	'width'		: 900,
      	'height'	: 600,
      	'type'		: 'iframe',
              'autoScale'    	: false
          });
    });
</script>
<div class="rightDiv">
  <div class="right-outer">
    <?php if(isset($details->id) && $details->id >0){?>
    <h3 class="title">Edit
      <?php if(isset($details->title)){echo $details->title;}?>
    </h3>
    <?php }
    else {?>
    <h3 class="title">Add Home Page Block</h3>
    <?php } ?>
    <div class="clear"></div>
    <div class="mid-block padbot40">
      <div class="mid-content web-cont-mid">
        <div id="webcont-form">
          <div id="member-form" class="midarea">
            <?php 
		  $attributes = array('name' => 'cont', 'id' => 'cont');
		  echo form_open_multipart('kaizen/welcome_block/addedit/'.$details->id,$attributes);
		  echo form_hidden('initiatives_id', $details->id);
		  
		  ?>
            <?php
		if($this->session->userdata('ERROR_MSG')==TRUE){
			echo '<div class="notific_error">
					<h2 align="center" style="color:#fff;">'.$this->session->userdata('ERROR_MSG').'</h1></div>';
			$this->session->unset_userdata('ERROR_MSG');
		}
		if($this->session->userdata('SUCC_MSG')==TRUE){
			echo '<div class="notific_suc"><h2 align="center" style="color:#000;">'.$this->session->userdata('SUCC_MSG').'</h1></div>';
			$this->session->unset_userdata('SUCC_MSG');
		}
		?>
            <?php echo validation_errors('<div class="notific_error">', '</div>'); ?>
            <div class="single-column" >
              <label class="question-label">Title<span> *</span></label>
              <input type="text" name="title" id="title" value="<?php if(isset($details->title)){echo $details->title;}?>" class="inputinpt validate[required]" />
            </div>

            <div class="single-column" >
                <label class="question-label">Excerpt<span> *</span></label>
                
                   <textarea id="description" name="description" class="inputinpt validate[required]"><?php if(!empty($details->description)){echo $details->description;}?></textarea>
            </div>

<!--              <div class="single-column" >
              <label class="question-label">Vision Text</label>
                 <?php
                                    if(!empty($details->vision_text )){
                                        $cont_txt2 = outputEscapeString($details->vision_text );
                                    }
                                    else{
                                        $cont_txt2 = "";
                                    }?>
                                <textarea id="vision_text" name="vision_text" class="editor"><?php echo $cont_txt2;?></textarea>
            </div>-->
            
            
<!--                <div class="single-column" >
              <label class="question-label">Mission Text:</label>
                                         <?php
                                    if(!empty($details->mission_text )){
                                        $cont_txt5 = outputEscapeString($details->mission_text );
                                    }
                                    else{
                                        $cont_txt5 = "";
                                    }?>
                                <textarea id="mission_text" name="mission_text" class="excerpt"><?php echo $cont_txt5;?></textarea>
                   </div>-->
                   
                   
                   
                   
     <div class="single-column">
     <label class="labelname">Button Type<span> *</span></label>
     <input type="radio" id="button_type" name="button_type" value="1" <?php if(!empty($details->button_type) && $details->button_type == '1'){echo "checked";} ?>       onclick="show_page_div('button_page_div','button_ext_div');" class="inputinpt validate[required]"/> Select Page
    <input type="radio" id="button_type" name="button_type" value="2" <?php if(!empty($details->button_type) && $details->button_type == '2'){echo "checked";} ?>         onclick="show_ext_div('button_page_div','button_ext_div');"  class="inputinpt validate[required]"/> External Link
     </div>
                   

                    <script>
                   function show_page_div(a,b){ 
					   document.getElementById(a).style.display = '';
					   document.getElementById(b).style.display = 'none';
				   }
				   function show_ext_div(a,b){
					   document.getElementById(a).style.display = 'none';
					   document.getElementById(b).style.display = '';
				   }
                   </script>


 <div class="single-column" id="button_page_div" style="display:<?php if(!empty($details->button_type) && $details->button_type == '1'){echo  ''; }else{echo  'none';} ?>;">
        <label class="question-label">Select Page:<span>*</span></label>
          <select name="page_link" id="page_link" class="inputinpt validate[required]">
              <option value="" >-Select Pages-</option>
     <?php 
	 
	  foreach ($page_list as $rs_menu_id){?>
		  
            <option value="<?php echo $rs_menu_id->id?>" <?php if( $rs_menu_id->id==$details->page_link1){ echo selected ; }?> ><?php echo $rs_menu_id->title;?></option> <?php }
	  
	  ?>
              </select>
            </div>
            
            
            <div class="single-column" id="button_ext_div" style="display:<?php if(!empty($details->button_type) && $details->button_type == '2'){echo  ''; }else{echo  'none';} ?>;">
              <label class="question-label">External Link URL: <span> *</span></label>
              <input type="text" name="banner_url" id="banner_url" value="<?php if(!empty($details->banner_url) && $details->button_type == '2'){ echo $details->banner_url;} ?>" class="inputinpt validate[required,custom[url]]" />
            </div>
                   
                   
                   
                   
            
              <div class="single-column" >
	          <label class="question-label">Welcome Photo - Size Requirement:  365 x 420 px ( W x H )</label><p class="sizetxt"></p>
	          <input id="htmlfile1" name="htmlfile1" value="<?php if(!empty($details->image)) echo $details->image; ?>" type="text">
	          <a href="<?php echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile1&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>
            
            
            
<!--                   <div class="single-column">
               
                                    <label class="labelname">Button Type:</label>
                                    <input type="radio" id="button_type" name="button_type" value="1" <?php if(!empty($details->button_type) && $details->button_type == '1'){echo "checked";} ?>  onclick="show_page_div();" /> Select Page
                                    <input type="radio" id="button_type" name="button_type" value="2" <?php if(!empty($details->button_type) && $details->button_type == '2'){echo "checked";} ?>  onclick="show_ext_div();"  /> External Link
                       
                   </div>
                   <script>
                   function show_page_div(){ 
					   document.getElementById('button_page_div').style.display = '';
					   document.getElementById('button_ext_div').style.display = 'none';
				   }
				   function show_ext_div(){
					   document.getElementById('button_page_div').style.display = 'none';
					   document.getElementById('button_ext_div').style.display = '';
				   }
                   </script>
                   <div class="single-column" id="button_page_div" style="display:<?php if(!empty($details->button_type) && $details->button_type == '1'){echo  ''; }else{echo  'none';} ?>;">
              <label class="question-label">Select Page: *</label>
              <select name="page_id" id="page_id" class="inputinpt validate[required]">
                  <option value="" >-Select Pages-</option>
                  <?php if(!empty($page_list)) { 
                  foreach($page_list as $list){
                  ?>
                  <option value="<?php echo $list->page_link; ?>" <?php if(!empty($details->page_id) && $details->page_id==$list->page_link){ echo 'selected'; } ?>><?php echo $list->title; ?></option>
                  <?php } } ?>
              </select>
            </div>
                   <div class="single-column" id="button_ext_div" style="display:<?php if(!empty($details->button_type) && $details->button_type == '2'){echo  ''; }else{echo  'none';} ?>;">
              <label class="question-label">External Link URL: *</label>
              <input type="text" name="link" id="link" value="<?php if(isset($details->link)){echo $details->link;}?>" class="inputinpt validate[required,custom[url]]" />
            </div>-->
                   
                   <div class="single-column" >
              <label class="question-label">Display Order<span>*</span></label>
              <input type="text" name="display_order" id="display_order" value="<?php if(isset($details->display_order)){echo $details->display_order;}?>" class="inputinpt validate[required,custom[integer]]" />
            </div>
                   
                   <div class="single-column" >
              <label class="question-label">Status:<span>*</span></label>
              <input type="radio" name="is_active" id="is_active" value="1" 
                                <?php echo ((isset($details->is_active) && $details->is_active ==1)?'checked="checked"':'')?>/>
              &nbsp;Active &nbsp;&nbsp;
              <input type="radio" name="is_active" id="is_active_1" value="0" <?php echo ((isset($details->is_active) && $details->is_active ==0)?'checked="checked"':'')?> />
              &nbsp;Inactive &nbsp;&nbsp; </div>
            <div class="bottonserright" style="padding-bottom:20px;"> <a href="javascript:void(0);" title="Delete" onClick="rowdelete('<?php echo $details->id; ?>','welcome_block');" class="web-red-btn" <?php if(isset($details->id) && $details->id >0){}else{echo 'style="display:none;"';}?>> <span>Delete</span> </a> <a href="javascript:void(0);" class="web-red-btn" onClick="form_submit();"><span>Save</span></a> <?php echo form_close();?> </div>
          </div>
        </div>
        <div class="bodybottom"> </div>
      </div>
    </div>
    <div class="rt-block">
  <?php $this->load->view($right); ?>
</div></div>
  </div>
  <div class="clear"></div>
  <?php $this->load->view($footer); ?>
</div>

<script type="text/javascript">
	var editor, html = '';
	if (editor ){
   	editor.destroy();
	editor = null;
	}
    CKEDITOR.replace( 'description1' ,{
	width : '95%',
	contentsCss : '<?php echo site_url("public/kaizen/css/style_ck.css");?>',	
	filebrowserBrowseUrl : '<?php echo site_url("/filemanager/dialog.php?type=2&editor=ckeditor&fldr=");?>',
	filebrowserUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files");?>',
	filebrowserImageUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images");?>',
	filebrowserFlashUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash");?>'
});


</script>

<script type="text/javascript">   
	var editor, html = '';
	if (editor ){
   	editor.destroy();
	editor = null;
	}
    CKEDITOR.replace( 'vision_text' ,{
	width : '95%',
	contentsCss : '<?php echo site_url("public/kaizen/css/style_ck.css");?>',	
	filebrowserBrowseUrl : '<?php echo site_url("/filemanager/dialog.php?type=2&editor=ckeditor&fldr=");?>',
	filebrowserUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files");?>',
	filebrowserImageUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images");?>',
	filebrowserFlashUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash");?>'
});   
</script>

<script type="text/javascript">   
	var editor, html = '';
	if (editor ){
   	editor.destroy();
	editor = null;
	}
    CKEDITOR.replace( 'mission_text' ,{
	width : '95%',
	contentsCss : '<?php echo site_url("public/kaizen/css/style_ck.css");?>',	
	filebrowserBrowseUrl : '<?php echo site_url("/filemanager/dialog.php?type=2&editor=ckeditor&fldr=");?>',
	filebrowserUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files");?>',
	filebrowserImageUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images");?>',
	filebrowserFlashUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash");?>'
});   
</script>