<?php
class Model_news extends MY_Model{
	//var $site_id='';
	public function __construct(){
		parent::__construct();	
		//$this->site_id=$this->config->item("SITE_ID");	
	}
        public function select_year(){
		$year = '%Y-%m-%d';
            if($query = $this->db->query('SELECT YEAR(STR_TO_DATE(post_dt, "'.$year.'" ) ) AS year FROM `'.$this->db->dbprefix('blog_post')."` WHERE `is_active`='1' GROUP BY year order by year desc" ))
            {
            //echo $this->db->last_query();exit;
                $res = $query->result();
                if(!empty($res))
                { 
                    return $res;
                }
                else{
                    return false;
                }
            }
        }
        public function select_month($y=''){
		$year = '%Y-%m-%d';
            if($query = $this->db->query('SELECT YEAR(STR_TO_DATE(post_dt, "'.$year.'" ) ) AS year, MONTH(STR_TO_DATE(post_dt, "'.$year.'" ) ) AS month FROM `'.$this->db->dbprefix('blog_post')."` WHERE `is_active`='1' AND YEAR(`post_dt`) ='".$y."' GROUP BY month order by month asc" ))
            {
            //echo $this->db->last_query();exit;
                $res = $query->result();
                if(!empty($res))
                { 
                    return $res;
                }
                else{
                    return false;
                }
            }
        }
	public function select_blog($cat_id='',$year='',$month='',$searchstring=''){
        $search = '';
		 if(!empty($searchstring)){
            $search = " AND (title like '%".trim($searchstring)."%' or excerpt like '%".trim($searchstring)."%' or description like '%".trim($searchstring)."%')";
			
        }
        if(!empty($cat_id)){
            //$search = " and `category_id` like '%".$cat_id."%'";
			$search = " and FIND_IN_SET('".$cat_id."',category_id)";
        }
        if($year!='' && $month!=''){
            $search = " and YEAR(`post_dt`) = '".$year."' and MONTH(`post_dt`) = '".$month."'";
        }
		$currentdate = date('Y-m-d H:i:s');
		if($query = $this->db->query("SELECT * FROM `".$this->db->dbprefix('blog_post')."` WHERE `is_active`='1'   ".$search." 
		and `post_dt`<= '".$currentdate."' order by `post_dt` desc" ))
		{
		//echo $this->db->last_query();exit;
			$res = $query->result();
			if(!empty($res))
			{ return $res;
		}
		else{
			return false;
		}
	}}
        
        public function select_latest_blog($cat_id='',$year='',$month='',$searchstring=''){
        $search = '';
		 if(!empty($searchstring)){
            $search = " AND (title like '%".trim($searchstring)."%' or excerpt like '%".trim($searchstring)."%' or description like '%".trim($searchstring)."%' or author like '%".trim($searchstring)."%')";
			
        }
        if(!empty($cat_id)){
            //$search = " and `category_id` like '%".$cat_id."%'";
			$search = " and FIND_IN_SET('".$cat_id."',category_id)";
        }
        if($year!='' && $month!=''){
            $search = " and YEAR(`post_dt`) = '".$year."' and MONTH(`post_dt`) = '".$month."'";
        }
		$currentdate = date('Y-m-d H:i:s');
		if($query = $this->db->query("SELECT * FROM `".$this->db->dbprefix('blog_post')."` WHERE `is_active`='1'   ".$search." and `post_dt`< '".$currentdate."' order by `post_dt` desc limit 0,3" ))
		{
		//echo $this->db->last_query();exit;
			$res = $query->result();
			if(!empty($res))
			{ return $res;
		}
		else{
			return false;
		}
	}}
        
        public function select_feature_blog($cat_id='',$year='',$month='',$searchstring=''){
        $search = '';
		 if(!empty($searchstring)){
            $search = " AND (title like '%".trim($searchstring)."%' or excerpt like '%".trim($searchstring)."%' or description like '%".trim($searchstring)."%' or author like '%".trim($searchstring)."%')";
			
        }
        if(!empty($cat_id)){
            //$search = " and `category_id` like '%".$cat_id."%'";
			$search = " and FIND_IN_SET('".$cat_id."',category_id)";
        }
        if($year!='' && $month!=''){
            $search = " and YEAR(`post_dt`) = '".$year."' and MONTH(`post_dt`) = '".$month."'";
        }
		$currentdate = date('Y-m-d H:i:s');
		if($query = $this->db->query("SELECT * FROM `".$this->db->dbprefix('blog_post')."` WHERE `is_active`='1'  and `is_feature`='1' ".$search." and `post_dt`< '".$currentdate."' order by `post_dt` desc" ))
		{
		//echo $this->db->last_query();exit;
			$res = $query->result();
			if(!empty($res))
			{ return $res;
		}
		else{
			return false;
		}
	}}
        
	public function getsingleblog($curtable,$url){
		$query = $this->db->query("SELECT * FROM `".$this->db->dbprefix($curtable)."` WHERE  `is_active`='1' AND `page_link`='".$url."'");
		#echo $this->db->last_query();
		if($query)
		{
			return $query->row();	
		}
		else{
			return false;
		}
	}
	
	public function prev_post($id="")
	{
		$query = false;
		if(!empty($id)){
			$query = $this->db->query("SELECT * FROM `".$this->db->dbprefix('blog_post')."` WHERE `is_active`=1 AND `id`<".$this->db->escape($id)." ORDER BY `id` DESC LIMIT 1");			
		}
		if($query){
			return $query->row();	
		}
		else{
			return false;
		}
	}
	public function next_post($id="")
	{
		$query = false;
		if(!empty($id)){
			$query = $this->db->query("SELECT * FROM `".$this->db->dbprefix('blog_post')."` WHERE `is_active`=1 AND `id`>".$this->db->escape($id)." ORDER BY `id` ASC  LIMIT 1");
		}
		if($query){
			return $query->row();	
		}
		else{
			return false;
		}
	}
	
		
	
	public function commentCount($table="",$post_id="")
	{
		if($this->db->query("UPDATE `".$this->db->dbprefix($table)."` SET `comment_count`=(`comment_count` + 1)  WHERE `id`=".$this->db->escape($post_id)." ")){
			return $post_id;
		
		}else{
			return false;
		}		
	}
}
?>