<?php $this->load->view($header); ?>
<?php $this->load->view($left); ?>
<link rel="stylesheet" href="<?php echo site_url("public/validator/css/validationEngine.jquery.css");?>" type="text/css"/>
<script src="<?php echo site_url("public/validator/js/languages/jquery.validationEngine-en.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo site_url("public/validator/js/jquery.validationEngine.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url("public/js/jquery.fancybox.js");?>" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url("public/css/jquery.fancybox.css");?>" type="text/css"/>
<script src="<?php echo site_url("public/js/jquery.mask.min.js");?>" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo site_url("public/ckeditor/ckeditor.js");?>"></script>

<script>
$('.fancybox').fancybox({
  width:'1200',
  height: '800'
});
</script>

<script type="text/javascript">
$(document).ready(function(){
	$("#cont").validationEngine();
	$('.phone_us').mask('0000000000');
	});
function form_submit(){
    $('#cont').submit();
}
</script>

<script type="text/javascript">
     $(function() {
     $('.iframe-btn').fancybox({	
      	'width'		: 900,
      	'height'	: 600,
      	'type'		: 'iframe',
              'autoScale'    	: false
          });
    });
	
	
	function form_submit(){
	 $('#selected_pages_id option').prop('selected', true);
    $('#cont').submit();
}
	
	
	function addAnothercontent(id){ 
	var count = $('#count_content').val();
     var pid = $('#featured_block_id').val();
	var count_val = parseInt(count)+1;
	jQuery('#count_content').val(count_val);
	//alert(count_val);
	jQuery.ajax({
			   type: "POST",
               async: false,
				url : '<?php echo site_url("kaizen/products/add_requirements/");?>',
                 data: { count:count,id:id,pid:pid},
				dataType : "html",
				success: function(data)
				{
					if(data)
					{
							jQuery("#add_requirements").prepend(data);
					}
					else
					{
						//alert("Sorry, Some problem is there. Please try again.");
					}
				},
				error : function() 
				{
					alert("Sorry, The requested property could not be found.");		
				}
			});
                        }
	
	
</script>

<div class="rightDiv">
  <div class="right-outer">
    <?php if(isset($details->id) && $details->id >0){?>
    <h3 class="title">Edit
      <?php if(isset($details->title)){echo $details->title;}?>
    </h3>
    <?php }
    else {?>
    <h3 class="title">Add Products</h3>
    <?php } ?>
    <div class="clear"></div>
    <div class="mid-block padbot40">
      <div class="mid-content web-cont-mid">
        <div id="webcont-form">
          <div id="member-form" class="midarea">
            <?php 
		  $attributes = array('name' => 'cont', 'id' => 'cont');
		  echo form_open_multipart('kaizen/products/addedit/'.$details->id,$attributes);
		  echo form_hidden('banner_id', $details->id);
		  
		  ?>
            <?php
		if($this->session->userdata('ERROR_MSG')==TRUE){
			echo '<div class="notific_error">
					<h2 align="center" style="color:#fff;">'.$this->session->userdata('ERROR_MSG').'</h1></div>';
			$this->session->unset_userdata('ERROR_MSG');
		}
		if($this->session->userdata('SUCC_MSG')==TRUE){
			echo '<div class="notific_suc"><h2 align="center" style="color:#000;">'.$this->session->userdata('SUCC_MSG').'</h1></div>';
			$this->session->unset_userdata('SUCC_MSG');
		}
		?>
            <?php echo validation_errors('<div class="notific_error">', '</div>'); ?>
            
             <div class="single-column" >
              <label class="question-label">Title <span> *</span></label>
              <input type="text" name="title" id="title" value="<?php if(isset($details->title)){echo $details->title;}?>" class="inputinpt validate[required]" />
            </div>
            
            
         <div class="single-column" >
         <label class="question-label">Category <span> *</span></label>
              
         <select name="category" class="inputinpt validate[required]" >
           <option value="">Select</option>
              <?php
			  foreach($category as $row)
		{
			?>
          <option value="<?php echo $row->id;?>" disabled="disabled">
		  <?php echo $row->title;?> </option>
          <?php $data_rec= $this->modelproducts->select_row('category',array('parent_id'=>$row->id));
		         foreach($data_rec as $row1)
		{ ?><option value="<?php echo $row1->id;?>" <?php if($row1->id==$details->category) {?> selected="selected" <?php }?>>
                   <?php echo '&nbsp;&nbsp;&nbsp;---> '.$row1->title;?>   </option>

            <?php }}?>
          </select>
         </div>
         
            
            
            <div class="single-column" >
              <label class="question-label">Excerpt<span></span></label>
              <textarea name="excerpt" id="excerpt"><?php if(isset($details->excerpt)){echo $details->excerpt;}?></textarea>
            </div>
            
            
            <div class="single-column" >
              <label class="question-label">Description </label>
              <?php
                                    if(!empty($details->description)){
                                        $cont_txt3 = outputEscapeString($details->description);
                                    }
                                    else{
                                        $cont_txt3 = "";
                                    }?>
              <textarea name="description" id="description" class="inputinpt" ><?php if(isset($details->description)){echo $details->description;}?></textarea>
            </div>
            
            <div class="single-column" >
            <label class="question-label">Product Photo - Size Requirement:  319 x 324 px ( W x H )</label><p class="sizetxt"></p>
            <input id="htmlfile1" name="htmlfile1" value="<?php if(!empty($details->image)) echo $details->image; ?>" type="text">
            <a href="<?php echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile1&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
          </div>
              
            <div class="single-column">
                  <input type="hidden" name="count_content" id="count_content" value="1" />
                     <label class="labelname" ><a href="javascript:void(0);" onclick="addAnothercontent();" >+Add Image</a></label>
                        <div id="add_requirements" class=""></div>
             
             </div>
            
            
            
            <div class="single-column" >
              <label class="question-label">Display Order <span> *</span></label>
              <input type="text" name="display_order" id="display_order" value="<?php if(isset($details->display_order)){echo $details->display_order;}?>" class="inputinpt validate[required,custom[integer]]" />
            </div>
            

            
            <div class="single-column" >
              <label class="question-label">Status:<span>*</span></label>
              <input type="radio" name="is_active" id="is_active" value="1" 
                                <?php echo ((isset($details->is_active) && $details->is_active ==1)?'checked="checked"':'')?>/>
              &nbsp;Active &nbsp;&nbsp;
              <input type="radio" name="is_active" id="is_active_1" value="0" <?php echo ((isset($details->is_active) && $details->is_active ==0)?'checked="checked"':'')?> />
              &nbsp;Inactive &nbsp;&nbsp; </div>
            <div class="bottonserright" style="padding-bottom:20px;"> <a href="javascript:void(0);" title="Delete"
             onClick="rowdelete('<?php echo $details->id; ?>','products');" class="web-red-btn" <?php if(isset($details->id) && $details->id >0){}else{echo 'style="display:none;"';}?>> <span>Delete</span> </a> <a href="javascript:void(0);" class="web-red-btn" onClick="form_submit();">
                          <span>Save</span></a> <?php echo form_close();?> </div>
          </div>
        </div>
        <div class="bodybottom"> </div>
      </div>
    </div>
    <div class="rt-block">
  <?php $this->load->view($right); ?>
</div>
  </div>
</div>
  <div class="clear"></div>
  <?php $this->load->view($footer); ?>
</div>
<script type="text/javascript">

$(function() {
					$("#MoveRightP,#MoveLeftP").click(function(production) {
						var id = $(production.target).attr("id");
						var selectFrom = id == "MoveRightP" ? "#pages_id" : "#selected_pages_id";
						var moveTo = id == "MoveRightP" ? "#selected_pages_id" : "#pages_id";
						var selectedItems = $(selectFrom + " :selected").toArray();
						$(moveTo).append(selectedItems);
					});
					});


<?php if(!empty($content_list)) {
     foreach ($content_list as $content){
                ?>
 addAnothercontent('<?php echo $content->id; ?>');
     <?php } }?>
</script>
<script type="text/javascript">
	var editor, html = '';
	if (editor ){
   	editor.destroy();
	editor = null;
	}
    CKEDITOR.replace( 'description' ,{
	width : '95%',
	contentsCss : '<?php echo site_url("public/css/style_ck.css");?>',	
	filebrowserBrowseUrl : '<?php echo site_url("public/ckfinder/ckfinder.html");?>',
	filebrowserUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files");?>',
	filebrowserImageUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images");?>',
	filebrowserFlashUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash");?>'
});
</script>