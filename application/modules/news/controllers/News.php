<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class News extends CI_Controller {
	var $data = array();
	public function __construct() {
	   parent::__construct();
	   $this->load->vars( array(		  
		  'header' => 'common/header',
		  'footer' => 'common/footer',
		    'right' => 'common/right',
		));		
		$this->load->model('news/model_news');		
	}

        public function index(){
            $cat=$sel_year=$catid=$sel_month=$selected_date='';
            $this->data['category']='';
            $day1 = $this->input->get('days');
            $month1 = $this->input->get('months');
            $year1 = $this->input->get('years');
            $date='';
            if(!empty($day1) && !empty($month1) && !empty($year1)){
                $date1=$day1.' '.$month1.' '.$year1;
                $selected_date=date('Y-m-d',strtotime($date1));
            }
            $this->data['selected_date'] = $selected_date;
            $cat=$this->input->get('cat');
            $yr=$this->input->get('year');
            $mnth=$this->input->get('month');
            if(!empty($yr)){
                $sel_year=$yr;
                $this->data['sel_year']=$sel_year;
            }
            if(!empty($mnth)){
                $sel_month=$mnth;
                $this->data['sel_month']=$sel_month;
            }
            if(!empty($cat)){
			$cat_id = $this->model_news->selectOne('blog_category',array('page_link'=>$cat));
                        $catid=$cat_id->id;
                        $this->data['category']=$cat_id;
                        $this->data['cat_link']=$cat;
	   }
            $this->data['year_list'] = $this->model_news->select_year();
            $where=array('is_active'=>1);
            $order=array('is_order'=>'asc');
            $this->data['category_list'] = $this->model_news->select_row('blog_category',$where,$order);
            $this->data['news_list'] = $this->model_news->select_blog('','','','');
            if(!empty($selected_date)){
                $where=array('post_dt'=>$selected_date);
                $order=array('id'=>'asc');
                $this->data['blog_list'] = $this->model_news->select_row('blog_post',$where,$order);
            }else{
                $this->data['blog_list'] = $this->model_news->select_blog($catid,$sel_year,$sel_month,'');
            }
            $this->load->view('news',$this->data);
	}
        
        public function details(){
            $link=$this->uri->segment(3);
            $this->data['year_list'] = $this->model_news->select_year();
            $where=array('is_active'=>1);
            $order=array('is_order'=>'asc');
            $this->data['category_list'] = $this->model_news->select_row('blog_category',$where,$order);
            $this->data['news_list'] = $this->model_news->select_blog('','','','');
            if(!empty($link)){
            $where=array('page_link'=>$link);
            $this->data['news_details'] = $this->model_news->selectOne('blog_post',$where);
            if(!empty($this->data['news_details'])){
                $this->load->view('news_details',$this->data);
            }else{
                redirect('notfound404');
            }
            }else{
                redirect('notfound404');
            }
            
        }
        
        public function getMonth(){
       $year = $this->input->post('year');
       $sel_mnth = $this->input->post('month');
       //echo $sel_mnth;
       $month = '';
       $month_list = $this->model_news->select_month($year);
       if(!empty($month_list)){
           foreach ($month_list as $mnth){
               $cls='';
               if($mnth->month==$sel_mnth){
                    $month .= '<li class="active">';
                    $cls='class="active"';
               }else{
                    $month .= '<li>';
               }
               $month .= '<a '.$cls.' href="'.site_url('news?month='.$mnth->month.'&year='.$year).'">'.date("F", mktime(0, 0, 0, $mnth->month, 10)).'</a></li>';
           }
       }
       echo $month;
   }
}