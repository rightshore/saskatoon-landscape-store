<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 class Metalist
 {
 	
 	public function get_meta()
 	{	
		$CI =& get_instance();
		$CI->load->model('home/model_meta');
		$CI->load->model('home/model_home');
		$tot_meta = $CI->model_meta->count_meta();
		
		if($tot_meta)
		{			
			$meta_list = $CI->model_meta->meta_list();
			$data=array();
			$CI->data['hooks_meta'] = $meta_list;	
			$site_setting = $CI->model_meta->site_settings();
			$CI->data['site_settings']=$site_setting;
			
			$commonbanner = $CI->model_meta->commonbanner($meta_list->id);
			$CI->data['commonbanner']=$commonbanner;
			$contact = $CI->model_meta->getcontact();
			$CI->data['contact'] = $contact;
			
			$cmspages_top_header = $CI->model_meta->cmspages_top_header();
			$CI->data['cmspages_top_header'] = $cmspages_top_header;
			
			$cmspages_down_footer = $CI->model_meta->cmspages_down_footer();
			$CI->data['cmspages_down_footer'] = $cmspages_down_footer;
			
			
                        $where=array('parent_id'=>0,'is_active'=>1,'shown_in_banner'=>1);
                        $order_by=array('display_order'=>'asc');
                        $CI->data['banner_pages_list']=$CI->model_meta->select_row('cms_pages',$where,$order_by);
                        
                        /*if(!empty($meta_list->parent_id)) {
                            $where=array('parent_id'=>$meta_list->parent_id,'is_active'=>1);
                            $order_by=array('display_order'=>'asc');
                            $CI->data['subpage_list']=$CI->model_meta->select_row('cms_pages',$where,$order_by);
                            $where=array('id'=>$meta_list->parent_id);
                            $page_details=$CI->model_meta->selectOne('cms_pages',$where);
                            $CI->data['parent_title']=$page_details->title;
                        }else{
                            $where=array('parent_id'=>$meta_list->id,'is_active'=>1);
                            $order_by=array('display_order'=>'asc');
                            $CI->data['subpage_list']=$CI->model_meta->select_row('cms_pages',$where,$order_by);
                            $CI->data['parent_title']=$meta_list->title;
                        }*/
			
		}
		else
		{
                        
			$site_setting = $CI->model_meta->site_settings();
			$data=array();
			$CI->data['hooks_meta'] = $site_setting;
			$meta_list = $site_setting;
                        
                        
			$CI->data['site_settings']=$site_setting;
                        $contact = $CI->model_meta->getcontact();
			$CI->data['contact'] = $contact;
            
                        
		}
	}
 }