<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home_block extends MY_Controller 
{
	private $limit = 20;
	var $offset = 0;
	function __construct()
	{
		parent::__construct();		 
		if( ! $this->session->userdata('web_admin_logged_in')) {
			redirect('kaizen/welcome','refresh');
		}
		$this->load->vars( array(
		  'global' => 'Available to all views',
		  'header' => 'common/header',
		  'left' => 'common/left',
          'right' => 'common/right',
		  'footer' => 'common/footer'
		));
		$this->load->model('modelhome_block');	
	}
	public function index()
	{	
		$this->dolist();	
	}
	public function dolist(){
		$data = array();
		$where = array( );
		$order_by = array('id' => 'asc');
		$data_row = $this->modelhome_block->select_row('home_block',$where,$order_by);
		$data['records']= $data_row;
		$this->load->view('home_block/home_block_list',$data);		
	}
	public function addedit()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<span class="validation_msg">', '</span>');
		$id=$this->input->post('contact_id','');
        $marker_logo='';
		if($this->form_validation->run() == TRUE) // IF MENDATORY FIELDS VALIDATION TRUE(SERVER SIDE)  
		{	
			$where = array(
                            'id' => $id
                        );
                $home_block_detls = $this->modelhome_block->select_row('home_block',$where);
        		if(!empty($home_block_detls)) 
                { 
                                $this->title		=$this->input->post('title',TRUE);
                                $this->sub_title		       =$this->input->post('sub_title',TRUE);
                                $this->image1		=$this->input->post('image1',TRUE);
                                $this->image2                 =$this->input->post('image2',TRUE);
                                $this->content                 =$this->input->post('content',TRUE);
                                $this->button_text		=$this->input->post('button_text',TRUE);
                                $this->button_type		=$this->input->post('button_type',TRUE);
								$this->page_link		=$this->input->post('page_link',TRUE);
								$this->banner_url		=$this->input->post('banner_url',TRUE);
                                
                              
                                $update_data = array(
                                    'title'                             => $this->title,
                                    'sub_title' 			          => $this->sub_title,
                                    'image1'                          => $this->image1,
                                    'image2'                          => $this->image2,
                                    'content' 			=> $this->content,
                                    'button_text' 			=> $this->button_text,
                                    'button_type' 			         => $this->button_type,
									'page_link' 			         => $this->page_link,
									'banner_url' 			         => $this->banner_url
                                );
                         
			              //pre($update_data );exit;
								
				$update_where = array('id' => $id);
				if($this->modelhome_block->update_row('home_block',$update_data,$update_where))
				{
					$session_data = array("SUCC_MSG"  => "Home Block Updated Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else 
				{	
					$session_data = array("ERROR_MSG"  => "Home Block Not Updated.");
					$this->session->set_userdata($session_data);				
				}
			}
			redirect("kaizen/home_block/doedit/".$id,'refresh');			
		}
		else{
			if(!empty($id)){
			$this->doedit();
			}
			else{
				$this->doadd();
			}
		}
	}
	public function doedit()
	{
		$data = array();
		$home_block_id=$this->uri->segment(4); 
		
		$where = array('id' => $home_block_id );
        $home_block_detls = $this->modelhome_block->select_row('home_block',$where);
		if($home_block_detls){
			$data['details'] = $home_block_detls[0];
		}
		else{
			//$data['details']->is_active = 1;
			$data['details']->id = 0;
		}
	    $where = array('is_active2' =>1);
        $order_by1 = array('display_order'=>'desc');
		$data['content_list']= $this->modelhome_block->select_row('home_block_subjects',array(),$order_by1); 
		$data['page1'] = $this->modelhome_block->select_row('cms_pages');
               // pre($data['content_list']);
		$this->load->view('home_block/edit_home_block',$data);		
	}
	
	   public function deletesubjects(){
		$count = $this->input->post("hidden_id");
                $this->db->where('id', $count);
                $this->db->delete('home_block_subjects'); 
	}
	
        public function add_subjects(){		 
		$data = array();
		$count = $this->input->post("count");
                $id = $this->input->post("id");
                $pid=$this->input->post("pid");
                if(!empty($id)){
                    $where=array('id'=>$id);
                    $product_dtls=$this->modelhome_block->selectOne('home_block_subjects',$where);
                    $data['id'] = $id;
                    $data['count'] = $count;
                    $data['title'] = $product_dtls->title;
                    $data['order'] = $product_dtls->display_order;
                    $data['is_active2'] = $product_dtls->is_active2;
                }else{
                    $data['count'] = $count;
                    $data['id'] = $id;
                    $data['is_active2'] =1;
                }
		$this->load->view('kaizen/home_block/add_subjects',$data);				
	}
	
}
