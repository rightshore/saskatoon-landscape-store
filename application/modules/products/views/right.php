<?php
$main_cat = $this->model_products->select_row('category',array('is_active'=>1,'parent_id'=>0),array('display_order'=>'asc'));
?>
<?php if(!empty($main_cat)){ ?>
<h4>categories explorer</h4>
<ul class="pro-category">
	<?php foreach($main_cat as $main){ 
			$sub_cat = $this->model_products->select_row('category',array('is_active'=>1,'parent_id'=>$main->id),array('display_order'=>'asc'));
			if(!empty($sub_cat)){
	?>
        <li>
			<a <?php if($cat_dtls->id==$main->id || $main_cat_dtls->id==$main->id) echo 'class="active"'; ?> href="<?php echo site_url('products/category/'.$main->page_link); ?>"><?php echo $main->title; ?></a>
			<?php if(!empty($sub_cat)) echo '<ul>'; ?>
			<?php foreach($sub_cat as $sub){
					$prod = $this->model_products->select_row('products',array('is_active'=>1,'category'=>$sub->id),array('display_order'=>'asc'));
					if(!empty($prod)){
				 ?>
				<li>
					<a href="<?php echo site_url('products/listing/'.$sub->page_link); ?>"><?php echo $sub->title; ?></a>
					<?php if(!empty($prod)) echo '<ul>'; ?>
					<?php foreach($prod as $pr){ ?>
						<li><a href="<?php echo site_url('products/details/'.$pr->page_link); ?>"><?php echo $pr->title; ?></a></li>
					<?php } ?>
					<?php if(!empty($prod)) echo '</ul>'; ?>
				</li>
			<?php } } ?>
			<?php if(!empty($sub_cat)) echo '</ul>'; ?>
		</li>
        
    <?php } } ?>
</ul>
<?php } ?>
