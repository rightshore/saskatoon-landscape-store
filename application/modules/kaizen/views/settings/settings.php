<?php $this->load->view($header); ?>
<?php echo $this->load->view($left); ?>
<?php  echo link_tag("public/validator/css/validationEngine.jquery.css")."\n"; ?>
<script type="text/javascript" src="<?php echo site_url("public/ckeditor/ckeditor.js");?>"></script>
<script src="<?php echo base_url("public/validator/js/languages/jquery.validationEngine-en.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url("public/validator/js/jquery.validationEngine.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url("public/js/jquery.fancybox.js");?>" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url("public/css/jquery.fancybox.css");?>" type="text/css"/>
<script>
$('.fancybox').fancybox({
  width:'1200',
  height: '800'
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#cont").validationEngine();
});	

function form_submit()
{
	$('#cont').submit();
	return true;
}

</script>

<script>
    
    function UnCheck()
{
	if(document.getElementById("show_comment").checked == false) 
	{ 
	 document.getElementById("comments_moderated").checked = false;
	 document.getElementById("comments_moderated").disabled = true;
	}
	if(document.getElementById("show_comment").checked == true) 
	{ 
	 document.getElementById("comments_moderated").disabled = false;
	}
}

function addAnotherFile(url,predifine_link,sequence,social_settings_id,logo){
	var count = $('#count').val();
	var count_val = parseInt(count)+1;
	$('#count').val(count_val);
	$.ajax({
			   type: "POST",
               async : false,
				url : '<?php echo site_url("kaizen/settings/add_file/");?>',
				data: { count:count,url:url,predifine_link:predifine_link,social_settings_id:social_settings_id,sequence:sequence,logo:logo},
				dataType : "html",
				success: function(data)
				{
					if(data)
					{
						
							$("#socialdiv").prepend(data);
						
						
					}
					else
					{
						//alert("Sorry, Some problem is there. Please try again.");
					}
				},
				error : function() 
				{
		
					alert("Sorry, The requested property could not be found.");		
				}
			});
}
</script>

<script type="text/javascript">
     $(function() {
     $('.iframe-btn').fancybox({	
      	'width'		: 900,
      	'height'	: 600,
      	'type'		: 'iframe',
              'autoScale'    	: false
          });
    });
</script>
<div class="rightDiv">
	<div class="right-outer">
            
	            
    	<div class="clear"></div>
      <h3 class="title">Website Setting</h3><div class="bread-crumb"><ul><li>Edit Setting Details</li></ul></div>
      
      <?php 
		  $attributes = array('name' => 'cont', 'id' => 'cont');
		  echo form_open_multipart('kaizen/settings/save',$attributes);		  
		  ?>
      <?php
		if($this->session->userdata('ERROR_MSG')==TRUE){
			echo '<div class="notific_error">
					<h2 align="center" style="color: #009900;">'.$this->session->userdata('ERROR_MSG').'</h1></div>';
			$this->session->unset_userdata('ERROR_MSG');
		}
		if($this->session->userdata('SUCC_MSG')==TRUE){
			echo '<div class="notific_suc"><h2 align="center" style="color: #009900;">'.$this->session->userdata('SUCC_MSG').'</h1></div>';
			$this->session->unset_userdata('SUCC_MSG');
		}
		?>
        <div class="clear"></div>
        
      <?php echo validation_errors('<div class="notific_error">', '</div>'); ?>
        <div class="padbot40">
            <div id="webcont-form">
        		<div id="member-form" class="midarea"> 
        	<div class="mid-block">
                    <div class="mid-content noBotPadd">
                    <div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Site Name: *</label>
                                <input type="text" name="site_name" id="site_name" value="<?php if(isset($details->site_name)){echo $details->site_name;}?>" class="validate[required]" />
                            </li>
                        </ul>
                    </div>
                    
                    
                    <div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Site URL: *</label>
                                <input type="text" name="url" id="url" value="<?php if(isset($details->url)){echo $details->url;}?>" class="validate[required, custom[url]]" />
                            </li>
                        </ul>
                    </div>
                        
                        <div class="single-column" >
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Copy Right: *</label>
                                <input type="text" name="copy_right" id="copy_right" value="<?php if(isset($details->copy_right)){echo $details->copy_right;}?>" class="validate[required]" />
                            </li>
                        </ul>
                    </div>
                        
                        <div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Contact Email: </label>
                                <input type="text" name="contact_email" id="contact_email" value="<?php if(isset($details->contact_email)){echo $details->contact_email;}?>" class="validate[optional,custom[email]]"/>
                            </li>
                        </ul>
                    </div>
                        
<!--                        <div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Forgot Password Email: </label>
                                <input type="text" name="forgot_password_email" id="forgot_password_email" value="<?php if(isset($details->forgot_password_email)){echo $details->forgot_password_email;}?>" class="validate[optional,custom[email]]"/>
                            </li>
                        </ul>
                    </div>-->
                        
                    
                        <div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Username*: </label>
         <input type="text" name="user_name" id="user_name" value="<?php if(isset($user_details[0]->user_name)){echo $user_details[0]->user_name;}?>" class="validate[required]"/>
                            </li>
                        </ul>
                    </div>
                    
                         <div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Password: </label>
                                <input type="text" name="password" id="password" value="<?php if(isset($user_details[0]->pwd_hint)){echo $user_details[0]->pwd_hint;}?>" class="validate[required]"/>
                            </li>
                        </ul>
                    </div>
                        
                        <div class="single-column" >
	          <label class="question-label">Header Logo - Size Requirement:  429 x 88 px ( W x H )</label><p class="sizetxt"></p>
	          <input id="htmlfile1" name="htmlfile1" value="<?php if(!empty($details->logo_photo)) echo $details->logo_photo; ?>" type="text">
	          <a href="<?php echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile1&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>
            
            

                        
                        <div class="single-column" >
	          <label class="question-label">Footer Logo - Size Requirement:  52 x 61 px ( W x H )</label><p class="sizetxt"></p>
	          <input id="htmlfile2" name="htmlfile2" value="<?php if(!empty($details->footer_logo)) echo $details->footer_logo; ?>" type="text">
	          <a href="<?php echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile2&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>
            
            
            
            <!--<div class="single-column" >
	          <label class="question-label">Home Page Footer Logo2 - Size Requirement:  2000 x 639 px ( W x H )</label><p class="sizetxt"></p>
	          <input id="htmlfile3" name="htmlfile3" value="<?php if(!empty($details->footer_logo2)) echo $details->footer_logo2; ?>" type="text">
	          <a href="<?php echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile3&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>-->
            
            
            <!--<div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Footer Logo2 Title: *</label>
                                <input type="text" name="footer_logo2_title" id="footer_logo2_title" value="<?php if(isset($details->footer_logo2_title)){echo $details->footer_logo2_title;}?>" class="validate[required]" />
                            </li>
                        </ul>
                    </div>-->
            


<!--<div class="single-column" >
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Header SARC URL: *</label>
                                <input type="text" name="sarc_url" id="sarc_url" value="<?php if(isset($details->sarc_url)){echo $details->sarc_url;}?>" class="validate[required,custom[url]]" />
                            </li>
                        </ul>
                    </div>-->
            
     
                          
                    <!--Google Analytics panel -->
                    <div class="seopan">
                        <h2><a href="javascript:showseopanel('droplistseo');" class="expandable">Google Analytics</a></h2>
                        <div class="droplists" id="droplistseo" style="display:none;">
                            <div class="single-column">
                                <ul>
                                    <li class="fullWidth">
                                        <label class="labelname">Google Site Verification:</label>
                                        <input name="site_verification" id="site_verification" type="text" value="<?php if(isset($details->site_verification)){echo $details->site_verification;}?>" class="titlefiled"  />
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="single-column">
                                <ul>
                                    <li class="fullWidth">
                                        <label class="labelname">Google Analytics Code:</label>
                                        <textarea name="analytics_code" id="analytics_code" class="description"><?php if(isset($details->analytics_code)){echo html_entity_decode(stripslashes($details->analytics_code), ENT_QUOTES,'UTF-8');}?></textarea>
                                    </li>
                                </ul>
                            </div>
                            
                            <div class="single-column">
                                <ul>
                                    <li class="fullWidth">
                                        <label class="labelname">Profile ID:</label>
                                        <input name="profile_id" id="profile_id" type="text" value="<?php if(isset($details->profile_id)){echo $details->profile_id;}?>" class="titlefiled"  />
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="seopan">
	<h2><a href="javascript:void(0);" onclick="javascript:showseopanel('droplistseopanel');" class="expandable">SEO</a></h2>
        <div class="droplists" id="droplistseopanel" style="display:none;">
                            <div class="single-column" >
                                <label class="question-label">Meta Title</label>
                                <input name="meta_title" id="meta_title" type="text" value="<?php if(isset($details->meta_title)){echo $details->meta_title;}?>" class="titlefiled" />
                            </div>
                            <div class="single-column" >
                                <label class="question-label">Meta Keyword</label>
                                <textarea name="meta_keyword" id="meta_keyword" class="description"><?php if(isset($details->meta_keyword)){echo html_entity_decode(stripslashes($details->meta_keyword), ENT_QUOTES,'UTF-8');}?>
</textarea>
                            </div>
                            <div class="single-column" >
                                <label class="question-label">Meta Description</label>
                                <textarea name="meta_desc" id="meta_desc" class="description" ><?php if(isset($details->meta_description)){echo html_entity_decode(stripslashes($details->meta_description), ENT_QUOTES,'UTF-8');}?>
</textarea>
                            </div>
		
		<?php echo form_hidden("sbmt","1");?> </div>
</div>
                            
                    
                    
                    
                    <div class="seopan" style="display:none">
                        <h2><a href="javascript:showseopanel('droplistblog');" class="expandable">Blog Settings</a></h2>
                        <div class="droplists" id="droplistblog" style="display:none;">
                            
                            <div class="single-column" >
                                <label class="question-label">Show Comment</label>
                                <input onclick="UnCheck();" type="checkbox" name="show_comment" id="show_comment" value="1" <?php if(isset($details->show_comment) && ($details->show_comment == 1)){ ?> checked <?php }?> class="inputinpt"  />
                            </div>
                            <div class="single-column" >
                                <label class="question-label">Comments Moderated?</label>
                                <input type="checkbox" name="comments_moderated" id="comments_moderated" value="1" <?php if(isset($details->comments_moderated) && ($details->comments_moderated == 1)){ ?> checked <?php }?> 
                                <?php if(empty($details->show_comment) && ($details->show_comment == 0)){ ?> disabled <?php }?> class="inputinpt"  />
                            </div>
                            
                        </div>
                    </div>
                    
                    
                    <div class="seopan">
                        <h2><a href="javascript:showseopanel('droplistsocialmedia');" class="expandable">Social Media</a></h2>
                        <div class="droplists" id="droplistsocialmedia" style="display:none;">
                            <a onclick="addAnotherFile('','')" href="javascript:void(0);" class="temp-btn" style="width:15%"><img style="float: left;margin-top: 3px;margin-left: 10px;width: 22px;" src="<?php echo base_url(); ?>public/images/plus-icon.png">Add Social Media</a>
                            
                            <input type="hidden" name="count" id="count" value="1" />	
                            
                            
                           
                            <div id="socialdiv">
                                
                            </div>
                            
                            
                            
                        </div>
                    </div>
                    
                            
                        
                        <?php echo form_hidden("sbmt","1");?> </div>
                    
                    <!--Google Analytics panel -->
                    
                    
                    
                    
                    
        </div>
        
        <div class="rt-block">
        <div class="rt-bg-block">
            <div class="rt-column search-side">
                <br>
                <a class="temp-btn " href="javascript:void(0);" onclick="form_submit();" >Update</a>
            </div>
        </div>
        </div>
        </div>
        </div>
        </div>
        <?php echo $this->load->view($footer); ?>
        
          </div>
                    
             </div>
            
        
        
      
     <!--<a href="javascript:void(0);" class="darkgreybtn" onclick="form_submit();"><span>UPDATE</span></a> -->
      <input type="hidden" name="save_draft" id="save_draft" value="0" />
      <?php echo form_close();?> 
</div>

<script type="text/javascript">

function showdiv(id){
	$('.showhide'+id).fadeIn('slow');
}

function hidediv(id){
	$('.showhide'+id).fadeOut('slow');
}


function confirmdel(id,field){
	if(confirm("Are you sure want to delete image?")){
		window.location.href="<?php echo site_url("kaizen/settings/dodeleteimg/");?>?deleteid="+id+'&field='+field;
	}
	else{
		return false;
	}
}
<?php if(!empty($social_settings_arr)){ ?>
    <?php foreach($social_settings_arr as $ssa){ ?>
            addAnotherFile('<?php echo $ssa->link ; ?>','<?php echo $ssa->social_menus_id ; ?>','<?php echo $ssa->sequence ; ?>','<?php echo $ssa->id ; ?>','<?php echo $ssa->logo ; ?>');
    <?php } ?>
<?php } ?>
</script>
<?php //$this->load->view($footer); ?>
