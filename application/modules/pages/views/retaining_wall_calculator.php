<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header); //pre($circlr_except_list); exit; 
?>
<script language="JavaScript" type="text/javascript">
function CalculateSum(LXtext, HXtext, form)
{
var LX = parseFloat(LXtext);
var HX = parseFloat(HXtext);
form.BGAnswer.value = Math.round (100*((LX*1.5*0.5)/27))/100;
form.SAnswer.value = Math.round (100*((LX*1.5*0.1)/27))/100;
form.SUAnswer.value = Math.round (100*(((HX/4)-1)*(LX*1.7)))/100;
form.CCSAnswer.value = Math.round (100*(LX*1.7))/100;
form.TRAnswer.value = Math.round (100*(HX/4))/100;
form.RCSAnswer.value = Math.round( 100*(1*1))/100;
form.RSUAnswer.value = Math.round (100*((HX/4)-1))/100;
form.PSAnswer.value = Math.round ((((HX/4)-1)*(LX*1.7))/150);
form.PCAnswer.value = Math.round ((LX*1.7)/150);

}



/* ClearForm: this function has 1 argument: form.
   It clears the input and answer fields on the form.
   It needs to know the names of the INPUT elements in order
   to do this. */

function ClearForm(form)
{
form.input_LX.value = "";
form.input_HX.value = "";
form.BGAnswer.value = "";
form.SAnswer.value = "";
form.SUAnswer.value = "";
form.CCSAnswer.value ="";
form.TRAnswer.value = "";
form.RCSAnswer.value = "";
form.RSUAnswer.value = "";
form.PSAnswer.value = "";
form.PCAnswer.value = "";
}

</script>
<div class="about-section calc-wrap">
    <div class="wrapper">	
    <h2><?php echo $this->data['hooks_meta']->title; ?></h2>
							
<form>
		<div class="calc-block">
			<div class="calc-blockin">
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Length (ft):</label>
						<div class="calc-field">
							<input type="text" name="input_LX" size="4" value="" class="input"/>
						</div>
					</div>
					<div class="calc-formlt right">
						<label>Height (inc):</label>
						<div class="calc-field">
							<input type="text" name="input_HX" size="4" value=""  class="input"/>
							<input name="AddButton" type="button" value="Calculate" onclick="CalculateSum(this.form.input_LX.value, this.form.input_HX.value, this.form)" class="submit"/>
						</div>
					</div>
				</div>
				<input type="button" value="Clear" name="RClearButton" onclick="ClearForm(this.form)" class="clrfield"/>
			</div>
			<div class="clear"></div>
		</div>
		
		<div class="calc-block">
			<h5>Step 1. Base Materials Required</h5>
			<div class="calc-blockin">
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Base Gravel:</label>
						<div class="calc-field">
							<input type="text" name="BGAnswer" size="12" class="input"/>
						</div>
					</div>
					<div class="calc-formlt right">
						<label>Sand:</label>
						<div class="calc-field">
							<input type="text" name="SAnswer" size="12" class="input"/>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		
		
		<div class="calc-block">
			<h5>Step 2. Stackstone / RomanStack Required</h5>
			<div class="calc-blockin">
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Standard Units:</label>
						<div class="calc-field">
							<input type="text" name="SUAnswer" size="12"  class="input1"/>
						</div>
					</div>
					<div class="calc-formlt right">
						<label>Coping ( cap stone ):</label>
						<div class="calc-field">
							<input type="text" name="CCSAnswer" size="12"  class="input1"/>
						</div>
					</div>
				</div>
				<div class="calc-com">
					<div class="calc-formlt">
						<label>total # rows :</label>
						<div class="calc-field">
							<input type="text" name="TRAnswer" size="12"  class="input1"/>
						</div>
					</div>
					<div class="calc-formlt right">
						<label>Rows cap stone :</label>
						<div class="calc-field">
							<input type="text" name="RCSAnswer" size="12" class="input1"/>
						</div>
					</div>
				</div>
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Rows standard units :</label>
						<div class="calc-field">
							<input type="text" name="RSUAnswer" size="12"  class="input1"/>
						</div>
					</div>
					<div class="calc-formlt right">
						<label>Pallets standards :</label>
						<div class="calc-field">
							<input type="text" name="PSAnswer" size="12" class="input1"/>
						</div>
					</div>
				</div>
				<div class="calc-com">
<!--					<div class="calc-formlt">
						<label>Rows standard units :</label>
						<div class="calc-field">
							<input type="text" name="RSUAnswer" size="12"  class="input1"/>
						</div>
					</div>-->
					<div class="calc-formlt">
						<label>Pallets Coping :</label>
						<div class="calc-field">
							<input type="text" name="PCAnswer" size="12" class="input1"/>
						</div>
					</div>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		
		
		<div class="calc-block">
			<h5>Step 3. Tips For Building Your Retaining Wall</h5>
			<div class="calc-blockin">
				<ul class="step3">
					<?php if(!empty($this->data['hooks_meta']->content)) echo outputEscapeString($this->data['hooks_meta']->content); ?>
				</ul>
				<a href="javascript:window.print()" class="print">Print</a>
			</div>
			<div class="clear"></div>
		</div>

</form>
        <div class="clear"></div>
    </div>
</div>
<!-- about section end --> 
<?php $this->load->view($footer);/*end*/ ?>