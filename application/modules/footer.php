<!--footer section start-->
	<div class="footer">
		<div class="wrapper">
                    <?php echo $hooks_footerpages_list; ?>
<!--            <ul class="ftnav">
                <li><a href="index.html" class="active">Home</a></li>
                <li><a href="about.html">About Us</a></li>
                <li><a href="courses-offered.html">Courses Offered</a></li>
                <li><a href="ground-school.html">Ground School</a></li>
                <li><a href="fleet.html">Fleet</a></li>
                <li><a href="contact.html">Contact Us</a></li>
            </ul>-->
			<?php if(!empty($this->data['contact'])) { ?>
                        <ul class="ftinfo">
                                <?php if(!empty($this->data['contact']->address)) { ?><li class="addrs"><span><?php echo $this->data['contact']->address; ?></span></li><?php } ?>
				<?php if(!empty($this->data['contact']->contact_email)) { ?><li class="mail"><span><a href="mailto:<?php echo $this->data['contact']->contact_email ?>"><?php echo $this->data['contact']->contact_email; ?></a></span></li><?php } ?>
				<?php if(!empty($this->data['contact']->contact_phone)) { ?><li class="call"><span><a href="tel:<?php echo str_replace(array('(',') ',' ','-'),array('','','',''),$this->data['contact']->contact_phone); ?>"><?php echo str_replace(array('(',') '),array('','-'),$this->data['contact']->contact_phone); ?></a></span></li><?php } ?>
			</ul>
                        <?php } ?>
		</div>
	</div>
	<div class="copyright">
		<div class="wrapper">
                        <?php if(!empty($this->data['site_settings']->copy_right)) { ?><p><?php echo date('Y')." ".$this->data['site_settings']->copy_right; ?> | <a href="<?php echo site_url('pages/privacy_policy'); ?>">Privacy Policy</a></p><?php } ?>
			<ul class="top-social" style="float:left; padding-left:100px;">
                            <?php echo socialLinks(); ?>
<!--				<li><a href="#" class="fb"><img src="images/top_fb_icon.png" alt="" title="" /></a></li>
                                <li><a href="#" class="twit"><img src="images/top_twit_icon.png" alt="" title="" /></a></li>-->
			</ul>
                          <p class="designby" style="float:right">Website Design & Developed by <a href="https://www.2webdesign.com/" rel="nofollow" target="_blank">2Webdesign</a></p>
		</div>
	</div>
	<!--footer section end-->


<!--script section start-->	
<script type='text/javascript' src='<?php echo base_url('public/default/js/jquery.nicescroll.min.js'); ?>'></script>
<script>
	$(document).ready(function() {
		
		var nice = $(".recycling-loc-names").niceScroll();  // The document page (body)
		$("#boxscroll").niceScroll("#boxscroll .wrap",{boxzoom:true});  // hw acceleration enabled when using wrapper

	  });
</script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/waypoints.min.js'); ?>'></script>
<script>
$('.post').waypoint(function() {
   $(this).addClass('visible animated grayscale');
   },
{ offset: '50%' });
</script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/map.js'); ?>'></script>
<!--script section end-->

</body>
</html>