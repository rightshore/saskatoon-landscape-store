<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header); //pre($circlr_except_list); exit;
?>

    <div class="about-section">
		<div class="wrapper">	
        	<h2><?php echo $this->data['hooks_meta']->title; ?></h2>
                <?php if(!empty($this->data['hooks_meta']->content)){ ?>
			<div class="about-text">
                            <?php echo outputEscapeString($this->data['hooks_meta']->content); ?>
			</div>
                <?php } ?>
                <?php if(!empty($this->data['hooks_meta']->about_us_photo2) && is_file(file_upload_path($this->data['hooks_meta']->about_us_photo2))){ ?>
			<div class="about-pic">
				<img src="<?php echo $this->data['hooks_meta']->about_us_photo2; ?>" alt="" title="">
			</div>
                <?php } ?>
            <div class="clear"></div>
		</div>
        <?php if(!empty($this->data['hooks_meta']->about_us_photo) && is_file(file_upload_path($this->data['hooks_meta']->about_us_photo))){ ?>
		<span class="about-back" style="background: url(<?php echo $this->data['hooks_meta']->about_us_photo; ?>) no-repeat center 0;"></span>
        <?php } ?>
	</div>
    <!-- about section end --> 
	
	
	<!-- our story section start -->
    <div class="our-story">
    	<div class="wrapper">
        	<?php echo outputEscapeString($this->data['hooks_meta']->content4); ?>
            <div class="quote">
                <?php if(!empty($this->data['hooks_meta']->content2)){ ?>
            	<div class="quotebx">
                	<div class="qtobxinr">
                            <?php echo outputEscapeString($this->data['hooks_meta']->content2); ?>
                        </div>
                </div>
                <?php } ?>
            	<?php echo outputEscapeString($this->data['hooks_meta']->content3); ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
	
	<?php
$this->load->view($footer);/*end*/
?>