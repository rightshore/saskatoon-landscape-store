<?php echo $this->load->view($header); ?>
<script src="<?php echo base_url('public/js/jquery.nestable.js'); ?>" type="text/javascript" charset="utf-8"></script>
<?php echo $this->load->view($left); ?>

<div class="rightDiv">
    <?php //$this->load->view($cms_header); ?>
	<div class="right-outer">
    	<div class="right-outer">
		<h3 class="title ">Arrange Pages</h3><div class="bread-crumb">
        <ul>

            <li></li>


            <li></li>

        </ul>


        </div>
        <div class="clear"></div>

        <div>
        	<div class="mid-block">


               <div class="mid-content" style="min-height:0px;">
                   <div class="appeal-form" id="form_application" style="border-top:none;margin:0px 0px 0px 0px;padding: 0px 0px 0px 0px;">

    <style>

		pre,code {
			font-size: 12px;
		}

		pre {
			width: 100%;
			overflow: auto;
		}

		small {
			font-size: 90%;
		}

		small code {
			font-size: 11px;
		}

		.placeholder {
			outline: 1px dashed #4183C4;
		}

		.mjs-nestedSortable-error {
			background: #fbe3e4;
			border-color: transparent;
		}

		#tree {
			width: 550px;
			margin: 0;
		}

		ol {
			max-width: 450px;
			padding-left: 25px;
		}

		ol.sortable,ol.sortable ol {
			list-style-type: none;
		}

		.sortable li div {
			border: 1px solid #d4d4d4;
			-webkit-border-radius: 3px;
			-moz-border-radius: 3px;
			border-radius: 3px;
			cursor: move;
			border-color: #D4D4D4 #D4D4D4 #BCBCBC;
			margin: 0;
			padding: 3px;
		}

		li.mjs-nestedSortable-collapsed.mjs-nestedSortable-hovering div {
			border-color: #999;
		}

		.disclose, .expandEditor {
			cursor: pointer;
			width: 20px;
			display: none;
		}

		.sortable li.mjs-nestedSortable-collapsed > ol {
			display: none;
		}

		.sortable li.mjs-nestedSortable-branch > div > .disclose {
			display: inline-block;
		}

		.sortable span.ui-icon {
			display: inline-block;
			margin: 0;
			padding: 0;
		}

		.menuDiv {
			background: #EBEBEB;
		}

		.menuEdit {
			background: #FFF;
		}

		.itemTitle {
			vertical-align: middle;
			cursor: pointer;
		}

		.deleteMenu {
			float: right;
			cursor: pointer;
		}


  </style>

<style type="text/css">

.cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
* html .cf { zoom: 1; }
*:first-child+html .cf { zoom: 1; }

/*html { margin: 0; padding: 0; }*/
/*body { font-size: 100%; margin: 0; padding: 1.75em; font-family: 'Helvetica Neue', Arial, sans-serif; }*/

h1 { font-size: 1.75em; margin: 0 0 0.6em 0; }

a { color: #2996cc; }
a:hover { text-decoration: none; }

p { line-height: 1.5em; }
.small { color: #666; font-size: 0.875em; }
.large { font-size: 1.25em; }


.dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }

.dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
.dd-list .dd-list { padding-left: 30px; }
.dd-collapsed .dd-list { display: none; }

.dd-item,
.dd-empty,
.dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }

.dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd-handle:hover { color: #2ea8e5; background: #fff; }

.dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
.dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
.dd-item > button[data-action="collapse"]:before { content: '-'; }

.dd-placeholder,
.dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
.dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
    background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                      -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                         -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                              linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
    background-size: 60px 60px;
    background-position: 0 0, 30px 30px;
}

.dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
.dd-dragel > .dd-item .dd-handle { margin-top: 0; }
.dd-dragel .dd-handle {
    -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
}



.nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }

#nestable-menu { padding: 0; margin: 20px 0; }

#nestable-output,
#nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }

#nestable2 .dd-handle {
    color: #fff;
    border: 1px solid #999;
    background: #bbb;
    background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
    background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
    background:         linear-gradient(top, #bbb 0%, #999 100%);
}
#nestable2 .dd-handle:hover { background: #bbb; }
#nestable2 .dd-item > button:before { color: #fff; }

@media only screen and (min-width: 700px) {

    .dd { float: left; width: 60%; }
    .dd + .dd { margin-left: 2%; }

}

.dd-hover > .dd-handle { background: #2ea8e5 !important; }


.dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
    background: #fafafa;
    background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
    background:         linear-gradient(top, #fafafa 0%, #eee 100%);
    -webkit-border-radius: 3px;
            border-radius: 3px;
    box-sizing: border-box; -moz-box-sizing: border-box;
}
.dd3-content:hover { color: #2ea8e5; background: #fff; }

.dd-dragel > .dd3-item > .dd3-content { margin: 0; }

.dd3-item > button { margin-left: 30px; }

.dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
    border: 1px solid #aaa;
    background: #ddd;
    background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
    background:         linear-gradient(top, #ddd 0%, #bbb 100%);
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}
.dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
.dd3-handle:hover { background: #ddd; }

    </style>



<b id="toArrayOutput" style="color: green;"></b>
    <menu id="nestable-menu">
        <button type="button" data-action="expand-all">Expand All</button>
        <button type="button" data-action="collapse-all">Collapse All</button>
    </menu>

<div class="cf nestable-lists">

        <div class="dd" id="nestable">
     <?
     print_r($page_list_menu);

    ?>
        </div>
</div>


        <textarea style="display:none;" id="nestable-output"></textarea>
        <input type="hidden" id="fetchcount" value="0" />
  <script>

$(document).ready(function()
{

    var updateOutput = function(e)
    {
        var fetchcount = $("#fetchcount").val();
        var list   = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
            $.ajax({
            data:{"fulldata" : window.JSON.stringify(list.nestable('serialize'))},
            type: 'POST',
            url: '<?php echo site_url("kaizen/site_map/changedisplayordernew/");?>',
  				dataType : "html",
  				success: function(data)
  				{
  					if(data)
  					{

                                            if(fetchcount != 0){
  							$("#toArrayOutput").html(data);
                                                        jQuery(window).scrollTop($("#toArrayOutput").offset().top);
                                            }
                                            fetchcount++;
                                            $("#fetchcount").val(fetchcount)
  					}
  					else
  					{
  						//alert("Sorry, Some problem is there. Please try again.");
  					}
  				},
  				error : function()
  				{
  					alert("Sorry, The requested property could not be found.");
  				}
        });
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    $('#nestable').nestable({
        group: 1
    })
    .on('change', updateOutput);




    updateOutput($('#nestable').data('output', $('#nestable-output')));


    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('.dd').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('.dd').nestable('collapseAll');
        }
    });


});
</script>


                   </div>

                </div>

                <?php $this->load->view($footer); ?>
                    <?php //echo $this->load->view($copyright); ?>
             </div>
            <div class="rt-block">
      <?php $this->load->view($right); ?>
    </div>


        </div>

    </div>
    </div>


</div>

<!--right column end-->
<div class="clear"></div>
<script>$(document).ready(function() {
		/* for top navigation */
		$(" .menulink ul ").css({display: "none"}); // Opera Fix
		$(" .menulink li ").hover(function(){
			$(this).find('ul:first').css({visibility: "visible",display: "none"}).slideDown(400);
			$(this).children('a').addClass('active');
		},function(){
			$(this).find('ul:first').css({visibility: "hidden"});
			$(this).children('a').removeClass('active');
		});

	});
</script>
</div>
</body>
</html>
