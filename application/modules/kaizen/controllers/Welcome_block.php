<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome_block extends MY_Controller 
{
	private $limit = 20;
	var $offset = 0;
	function __construct()
	{
		parent::__construct();		 
		
		if( ! $this->session->userdata('web_admin_logged_in')) {
			redirect('kaizen/welcome','refresh');
		}
		$this->load->vars( array(
		  'global' => 'Available to all views',
		  'header' => 'common/header',
		  'left' => 'common/left',
                    'right' => 'common/right',
		  'footer' => 'common/footer'
		));
		 $this->load->model('common/model_common');
		$this->load->model('modelwelcome_block');	
	}
	public function index()
	{	
		$this->dolist();	
	}
	
	public function dolist(){ 
		$data = array();
		$where = array();
        $order_by = array('id' => 'asc');   
		$data_row = $this->modelwelcome_block->select_row('welcome_block',$where,$order_by); 
		$data['records']= $data_row; 
		$this->load->view('kaizen/welcome_block/welcome_block_list',$data);		
	}
	
	public function doadd(){ 
		$data = array();
                $data['details']= new stdClass;
		$initiatives_id=$this->uri->segment(4);
		$data['details']->is_active = 1;
		$data['details']->id = $initiatives_id;
		$where=array('is_active'=>1);
		$order=array('title'=>'asc');
		$data['page_list'] = $this->modelwelcome_block->select_row('cms_pages',$where,$order);
		$this->load->view('kaizen/welcome_block/edit_welcome_block',$data);		
	}

	public function addedit()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('title', 'Title', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<span class="validation_msg">', '</span>');
		$id=$this->input->post('initiatives_id','');
	 $initiatives_detls ='';
		if($this->form_validation->run() == TRUE) // IF MENDATORY FIELDS VALIDATION TRUE(SERVER SIDE)  
		{	
			$where = array( 'id' => $id );
                $initiatives_detls = $this->modelwelcome_block->select_row('welcome_block',$where);
        		if(!empty($initiatives_detls)) 
                {
                                            $this->title		= $this->input->post('title',TRUE);
                                            $this->display_order	= $this->input->post('display_order');
                                            $this->image		= $this->input->post('htmlfile1');
											$this->button_type		=$this->input->post('button_type');
											$this->page_link		=$this->input->post('page_link');
											$this->banner_url		=$this->input->post('banner_url');
                                            $this->description		= $this->input->post('description');
                                            //$this->vision_text          = $this->input->post('vision_text');
                                            $this->is_active		=$this->input->post('is_active',TRUE);

                                
                if($this->is_active===false){
                        $this->is_active='1';
                }	
                                                                
				           			
				$update_data = array(
											'title'                 => $this->title,
											'display_order'         => $this->display_order,
											'button_type' 			=> $this->button_type,                       
											'page_link1' 			  =>$this->page_link,
											'banner_url' 			  =>$this->banner_url,
											'description'          => $this->description,
											'image'                 => $this->image,
											//'vision_text'           => $this->vision_text, 
											'is_active'             => $this->is_active,
											
				);
								
				//print_r($update_data); exit;				
				$update_where = array('id' => $id);
				if($this->modelwelcome_block->update_row('welcome_block',$update_data,$update_where)) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{
                                    if(!empty($this->is_active)){
                                 $this->activeinactive($id,1,'welcome_block');
                           }
					$session_data = array("SUCC_MSG"  => "Home Page Block Updated Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else 
				{	
					$session_data = array("ERROR_MSG"  => "Home Page Block Not Updated.");
					$this->session->set_userdata($session_data);				
				}
			}
			else 
			{  
									$this->title                = $this->input->post('title',TRUE);
									$this->display_order        = $this->input->post('display_order');
									$this->image                = $this->input->post('htmlfile1');
									$this->description         = $this->input->post('description');
									//$this->vision_text          = $this->input->post('vision_text');
									$this->page_id              = $this->input->post('page_id');
									$this->link                 = $this->input->post('link');
									$this->button_type		=$this->input->post('button_type');
									$this->page_link		=$this->input->post('page_link');
									$this->banner_url		=$this->input->post('banner_url');
									$this->is_active            =$this->input->post('is_active',TRUE);

                                
                if($this->is_active===false){
                        $this->is_active='1';
                }	
                                                                
                                											
				
                $add_data = array(
									'title' 			=> $this->title,
									'display_order' 		=> $this->display_order,
									'description' 			=> $this->description,
									'image'                         => $this->image,
									'button_type' 			=> $this->button_type,                       
									'page_link1' 			  =>$this->page_link,
									'banner_url' 			  =>$this->banner_url,
									//'vision_text'                   => $this->vision_text,
									'is_active' 			=> $this->is_active,
									'page_link' 			=> name_replaceCat('welcome_block',$this->title),
									
                                );
							
				$id = $this->modelwelcome_block->insert_row('welcome_block',$add_data);
				if($id) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{ 
                                    if(!empty($this->is_active)){
                        $this->activeinactive($id,1,'welcome_block');
                    }
					$session_data = array("SUCC_MSG"  => "Home Page Block Inserted Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else // IF UPDATE PROCEDURE NOT EXECUTE SUCCESSFULLY
				{	
					$session_data = array("ERROR_MSG"  => "Home Page Block Not Inserted.");
					$this->session->set_userdata($session_data);				
				}
				
			}
			redirect("kaizen/welcome_block/doedit/".$id,'refresh');			
		}
		else{
			if(!empty($id)){
			$this->doedit();
			}
			else{
				$this->doadd();
			}
		}
	}
	public function doedit()
	{ 
		$data = array();
		$banner_id=$this->uri->segment(4); 		
		$where = array(
                            'id' => $banner_id
                        );
                $banner_detls = $this->modelwelcome_block->select_row('welcome_block',$where);                       
		if($banner_detls){
			$data['details'] = $banner_detls[0];
		}
		else{
			$data['details']->is_active = 1;
			$data['details']->id = 0;
		}
                $where=array('is_active'=>1);
                $order=array('title'=>'asc');
                $data['page_list'] = $this->modelwelcome_block->select_row('cms_pages',$where,$order);
		$this->load->view('kaizen/welcome_block/edit_welcome_block',$data);				
	}

	public function updatefeature(){
		$finalids = $this->input->post('final');
		$main = rtrim($finalids, ",");
		$m = str_replace(",","','",$main);
		$res = $this->modelwelcome_block->updateisfeture($m);
		if($res){
			$session_data = array("SUCC_MSG"  => "Fetured Updated Successfully.");
			$this->session->set_userdata($session_data);
			echo 1;	
		}
	}
}