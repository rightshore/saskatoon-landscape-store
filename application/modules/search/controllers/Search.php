<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends CI_Controller {
	var $data = array();
	public function __construct() { 
	   parent::__construct();
	   // Set master content and sub-views
	   $this->load->vars( array(		  
		  'header' => 'common/header',
		  'footer' => 'common/footer'
		));
		
		$this->load->model('search/model_search');
	}

	public function index()	{ //echo 'hi';
            $match ='';
            $match = $this->input->get('search','TRUE');
            if(!empty($match)){
                $this->data['search'] = $match;
                $this->data['prod_list'] = $this->model_search->get_search('products','title,excerpt,description',$match);
                #echo $this->db->last_query();
                $this->load->view('search',$this->data);
            }else{
                redirect();
            }
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */