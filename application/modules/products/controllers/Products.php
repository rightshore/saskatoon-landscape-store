<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Products extends CI_Controller {
	var $data = array();
	public function __construct() {
	   parent::__construct();
	   $this->load->vars( array(		  
		  'header' => 'common/header',
		  'footer' => 'common/footer',
		    'right' => 'products/right',
		));		
		$this->load->model('products/model_products');		
	}

        public function index(){
            $this->data['cat_list'] = $this->model_products->select_row('category',array('is_active'=>1,'parent_id'=>0),array('display_order'=>'asc'));
            $this->load->view('products',$this->data);
		}
		
		public function category(){
			$cat_link=$this->uri->segment(3);
			if(!empty($cat_link)){
				$cat_dtls=$this->model_products->selectOne('category',array('page_link'=>$cat_link));
				if(!empty($cat_dtls)){
					$this->data['cat_dtls'] =$cat_dtls;
					$this->data['cat_list'] = $this->model_products->select_row('category',array('is_active'=>1,'parent_id'=>$cat_dtls->id),array('display_order'=>'asc'));
					$this->load->view('products_category',$this->data);
				}else{
					redirect('products');
				}
			}else{
				redirect('products');
			}
		}
		
		public function listing(){
			$cat_link=$this->uri->segment(3);
			if(!empty($cat_link)){
				$cat_dtls=$this->model_products->selectOne('category',array('page_link'=>$cat_link));
				if(!empty($cat_dtls)){
					$this->data['cat_dtls'] =$cat_dtls;
					$this->data['main_cat_dtls']=$this->model_products->selectOne('category',array('id'=>$cat_dtls->parent_id));
					$this->data['prod_list'] = $this->model_products->select_row('products',array('is_active'=>1,'category'=>$cat_dtls->id),array('display_order'=>'asc'));
					$this->load->view('products_list',$this->data);
				}else{
					redirect('products');
				}
			}else{
				redirect('products');
			}
		}
        
        public function details(){
            $link=$this->uri->segment(3);
            if(!empty($link)){
				$prod_dtls=$this->model_products->selectOne('products',array('page_link'=>$link));
				if(!empty($prod_dtls)){
					$this->data['prod_dtls'] =$prod_dtls;
					$this->data['cat_dtls'] =$this->model_products->selectOne('category',array('id'=>$prod_dtls->category));
					$this->data['main_cat_dtls']=$this->model_products->selectOne('category',array('id'=>$this->data['cat_dtls']->parent_id));
					$this->data['gallery_list']=$this->model_products->select_row('requirements',array('is_active2'=>1,'pid'=>$prod_dtls->id),array('display_order'=>'asc'));
					$this->load->view('products_details',$this->data);
				}else{
					redirect('products');
				}
			}else{
				redirect('products');
			}
            
            
        }
        
        
}
