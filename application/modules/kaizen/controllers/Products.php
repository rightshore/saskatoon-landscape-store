<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller 
{
	private $limit = 20;
	var $offset = 0;
	function __construct()
	{
		parent::__construct();		 
		
		if( ! $this->session->userdata('web_admin_logged_in')) {
			redirect('kaizen/welcome','refresh');
		}
		$this->load->vars( array(
		  'global' => 'Available to all views',
		  'header' => 'common/header',
		  'left' => 'common/left',
           'right' => 'common/right',
		  'footer' => 'common/footer'
		));
		
		$this->load->model('modelproducts');	
	}

	public function index()
	{	
		
		$this->dolist();	
	}
	
	
	
	public function dolist(){
		$data = array();
		$where = array();
                $order_by = array('display_order' => 'asc');
		$data_row = $this->modelproducts->select_row('products',$where,$order_by);
		$data['records']= $data_row;
		$this->load->view('kaizen/products/products_list',$data);		
	}
	
	public function doadd(){
		$data = array();
        $data['details']= new stdClass;
		$banner_id=$this->uri->segment(4);
		$data['details']->is_active = 1;
		$data['details']->recycle = 1;
		$data['details']->id = $banner_id;
		$category1 = $this->modelproducts->select_row('category',array('parent_id'=>0));
		$data['category']= $category1;
		$this->load->view('kaizen/products/edit_products',$data);		
	}
    
	public function addedit()
	{
          //pre($_POST); 
		$this->load->library('form_validation');
		$this->form_validation->set_rules('excerpt', 'Excerpt', 'trim|required|xss_clean');
		
		
		$this->form_validation->set_error_delimiters('<span class="validation_msg">', '</span>');
		$id=$this->input->post('banner_id','');
	 $banner_detls ='';
	 $banner_photo='';
        
		if($this->form_validation->run() == TRUE) // IF MENDATORY FIELDS VALIDATION TRUE(SERVER SIDE)  
		{	
			$where = array(
                           
                            'id' => $id
                        );
                        
                $banner_detls = $this->modelproducts->select_row('products',$where);
				 
        		if(!empty($banner_detls)) 
                {
                   
			                   $this->title                     =$this->input->post('title',TRUE);
								$this->excerpt                     =$this->input->post('excerpt',TRUE);
								$this->description                     =$this->input->post('description',TRUE);
								 $this->htmlfile1                =$this->input->post('htmlfile1',TRUE);
								$this->category                     =$this->input->post('category',TRUE);
								$this->display_order		=$this->input->post('display_order');
                                $this->is_active		=$this->input->post('is_active',TRUE); 
                                if($this->is_active===false){
                                        $this->is_active='1';
                                }
												
                                $update_data = array(
                                  
                                     'title'                             => $this->title,
									'excerpt'                             => $this->excerpt,
									'description'                             => $this->description,
									 'image'                      => $this->htmlfile1,
									'category'                             => $this->category,
									'display_order' 			=> $this->display_order,
                                    'is_active' 			=> $this->is_active,
                                );
								
								
								    $count_content=$this->input->post('count_content');
                                    for($inc=1;$inc<$count_content;$inc++){
                                    $cont_id=$this->input->post("cont_id".$inc,TRUE);
									$insert['image']            =$this->input->post('image'.$inc,TRUE);
                                    $insert['display_order'] = $this->input->post("order".$inc,TRUE);
                                    $insert['is_active2'] = $this->input->post("is_active2".$inc,TRUE);
                                    $insert['pid'] = $id;
                                    $insert['id'] = $this->input->post("cont_id".$inc,TRUE);
									 //$insert['pid'] = $id;
									 
//									 
//									  if($is_main == $inc)
//									 {
//                                        $insert['is_main'] =  1;
//                                     }else{
//                                         $insert['is_main'] =  0;
//                                     }
									 
									  //$insert['id'] = $this->input->post("cont_id".$inc,TRUE);
                                    if(!empty($insert)){
                                    if($cont_id==0){
                                        $this->modelproducts->insert_row('requirements',$insert);
                                    }else{
                                       $up_where=array('id'=>$cont_id);
                                       $this->modelproducts->update_row('requirements',$insert,$up_where); 
                                    }
                                    }
									}

                                
                
				$update_where = array('id' => $id);
				if($this->modelproducts->update_row('products',$update_data,$update_where)) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{
				
					$session_data = array("SUCC_MSG"  => "Products Updated Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else 
				{	
					$session_data = array("ERROR_MSG"  => "Products Not Updated.");
					$this->session->set_userdata($session_data);				
				}
			}
			else 
			{                   $this->title                     =$this->input->post('title',TRUE);
								$this->excerpt                     =$this->input->post('excerpt',TRUE);
								$this->description                     =$this->input->post('description',TRUE);
								 $this->htmlfile1                =$this->input->post('htmlfile1',TRUE);
								$this->category                     =$this->input->post('category',TRUE);
								$this->display_order		=$this->input->post('display_order');
                                $this->is_active		=$this->input->post('is_active',TRUE); 
                                if($this->is_active===false){
                                        $this->is_active='1';
                                }
				
        
				
                $add_data = array(
                                    
                                    'title'                             => $this->title,
									'excerpt'                             => $this->excerpt,
									'description'                             => $this->description,
									  'image'                      => $this->htmlfile1,   
									'category'                             => $this->category,
									'display_order' 			=> $this->display_order,
                                    'is_active' 			=> $this->is_active,
                                    'page_link' 			         => name_replaceCat('products',$this->title)
                                );
								//pre($add_data);exit;
                               // echo 'hiiiiiii';
				$id = $this->modelproducts->insert_row('products',$add_data);
				if($id) // IF UPDATE PROCEDURE EXECUTE SUCCESSFULLY
				{ 
                   
				    
                                      
                                    $count_content=$this->input->post('count_content');
                                    for($inc=1;$inc<$count_content;$inc++){
                                    $cont_id=$this->input->post("cont_id".$inc,TRUE);
                                    $insert['image']            =$this->input->post('image'.$inc,TRUE);
                                    $insert['display_order'] = $this->input->post("order".$inc,TRUE);
                                    $insert['is_active2'] = $this->input->post("is_active2".$inc,TRUE);
                                    $insert['pid'] = $id;
                                    $insert['id'] = $this->input->post("cont_id".$inc,TRUE);
                                    if(!empty($insert['display_order'])){
                                    if($cont_id==0)
									{
                                        $this->modelproducts->insert_row('requirements',$insert);
                                    }else{
                                       $up_where=array('id'=>$cont_id);
                                       $this->modelproducts->update_row('requirements',$insert,$up_where); 
                                    }
                                    }
                                }
                                
				   
				   
					$session_data = array("SUCC_MSG"  => "Products Inserted Successfully.");
					$this->session->set_userdata($session_data);					
				}			
				else // IF UPDATE PROCEDURE NOT EXECUTE SUCCESSFULLY
				{	
					$session_data = array("ERROR_MSG"  => "Products Not Inserted.");
					$this->session->set_userdata($session_data);				
				}
			}
			redirect("kaizen/products/doedit/".$id,'refresh');			
		}
		else{
			if(!empty($id)){
			$this->doedit();
			}
			else{
				$this->doadd();
			}
		}
	}
	public function doedit()
	{ 
		$data = array();
		$banner_id=$this->uri->segment(4); 
		$where = array(
                            'id' => $banner_id
                        );
                $banner_detls = $this->modelproducts->select_row('products',$where);                       
		if($banner_detls){
			$data['details'] = $banner_detls[0];
		}
		else{
			$data['details']->is_active = 1;
			$data['details']->recycle = 1;
			$data['details']->id = 0;
		}
		
		
		$idds = explode(',',$banner_detls[0]->pages_id);
                  
		if($banner_detls){
			$data['details'] = $banner_detls[0];
	    
		}
		else{
			$data['details']->is_active = 1;
			$data['details']->id = 0;
		}
		
		$where1 = array('pid' =>$banner_id);
        $order_by1 = array('display_order'=>'desc');
		$data['content_list']= $this->modelproducts->select_row('requirements',$where1,$order_by1);
        $category1 = $this->modelproducts->select_row('category',array('parent_id'=>0));
		$data['category']= $category1;
		$this->load->view('kaizen/products/edit_products',$data);				
	}
	
	
	public function add_requirements(){		 
		$data = array();
		$count = $this->input->post("count");
                $id = $this->input->post("id");
                $pid=$this->input->post("pid");
                if(!empty($id)){
                    $where=array('id'=>$id);
                    $product_dtls=$this->modelproducts->selectOne('requirements',$where);
                    $data['id'] = $id;
                    $data['count'] = $count;
				    $data['image'] = $product_dtls->image;
                    $data['order'] = $product_dtls->display_order;
                    $data['is_active2'] = $product_dtls->is_active2;
                }else{
                    $data['count'] = $count;
                    $data['id'] = $id;
                    $data['is_active2'] =1;
                }
                 
		$this->load->view('kaizen/products/add_requirements',$data);				
	}
	 

       public function updatefeature(){
		$finalids = $this->input->post('final');
		$main = rtrim($finalids, ",");
		$m = str_replace(",","','",$main);
		$res = $this->modelproducts->updateisfeture($m);
		if($res){
			$session_data = array("SUCC_MSG"  => "Fetured Updated Successfully.");
			$this->session->set_userdata($session_data);
			echo 1;	
		}
	}


      public function deleterequirements(){
		$count = $this->input->post("hidden_id");
                $this->db->where('id', $count);
                $this->db->delete('requirements'); 
	}
}