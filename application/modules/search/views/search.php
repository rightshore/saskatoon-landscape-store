<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header);
?>
<?php if(count($prod_list)>3){ ?>
<script src="<?php echo base_url('public/js/jPages.js'); ?>"></script>
<script>
  $(function() {    
    $("ul.all").jPages({
        containerID: "itemContainerAll",	
        perPage : 3,
        previous : 'Previous',
        next : 'Next',
        //first: 'First',
        //last: 'Last',
        minHeight: false,
        callback : function(pages, items ){
            var aid='scrollto';
            var aTag = $("a[name='"+ aid +"']");
            var scrollpagi =$("#scrollpagi").val();
            if(scrollpagi!=1) {
            $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }
            $("#scrollpagi").val(2);
        }
    });
  });
</script>
<input type="hidden" value="1" name="scrollpagi" id="scrollpagi"/>
<?php } ?>
<!--content section start-->
<div class="home-products">
        <div class="wrapper">
            <p><strong>Search Products for : <?php if(!empty($search)) echo $search; ?></strong></p>
            <br><br>
                <?php if(!empty($prod_list)){ ?>
                <a id="scrollto"></a>
                <ul id="itemContainerAll">
                        <?php foreach($prod_list as $list){ ?>
                        <li>
        <?php if(!empty($list->image) && is_file(file_upload_path($list->image))){ ?>
                                <div class="image">
                                        <a href="<?php echo site_url('products/details/'.$list->page_link); ?>"><img src="<?php echo $list->image; ?>" alt="" title=""></a>
                                </div>
            <?php } ?>
                                <div class="detail">
                                        <h4><a href="<?php echo site_url('products/details/'.$list->page_link); ?>"><?php echo $list->title; ?></a></h4>
                                        <?php if(!empty($list->excerpt)) echo '<p>'.$list->excerpt.'</p>'; ?>
                                        <a href="<?php echo site_url('products/details/'.$list->page_link); ?>" class="btn">More</a>
                                        <div class="clear"></div>
                                </div>
                        </li>
                        <?php } ?>
                </ul>
                <?php if(count($prod_list)>3){ ?>
                <ul class="all pagination"></ul>
                <?php } ?>
                <div class="clear"></div>
                <?php }else{ echo $this->data['hooks_meta']->content; } ?>
        </div>
</div>
<!--content section end-->
<?php
$this->load->view($footer);/*end*/
