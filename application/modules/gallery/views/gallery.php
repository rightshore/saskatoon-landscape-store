<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/jquery.fancybox.css?v=2.1.5') ?>" media="screen" />
<script type="text/javascript" src="<?php echo base_url('public/default/js/jquery.fancybox.js?v=2.1.5') ?>"></script>
<script type="text/javascript" language="javascript">
    var base_url='<?php echo base_url(); ?>';
	$(function() {
		$('.fancybox').fancybox();
	});
        function load_more(){
            var val=$('#start_limit').val();
            var newval=Number(val) + 8;
            $.ajax({
                url: base_url+'gallery/load_more',
                type: 'post',        
                data: {'val' : val},
                success: function (data, status) {
                  if(data!='0'){
                      var content=data.split('_=_');
                      $('#prod_list').append(content[0]);
                      $('#start_limit').val(newval);
                      if(content[1]=='1'){
                          $('#loading_btn').hide();
                      }
                  }else{
                      
                  }
                }
            });
        }
</script>
<!--gallery section start-->
	<div class="gallery-images">
    	<h2><?php echo $this->data['hooks_meta']->title; ?></h2>
        <input type="hidden" name="start_limit" id="start_limit" value="8"/>
		<ul class="about-gallery" id="prod_list">
			<?php foreach($gallery_list as $list){ 
                            if(!empty($list->image) && is_file(file_upload_path($list->image))){
                            ?>
			<li>
				<div class="block">
					<img src="<?php echo $list->image; ?>" alt="<?php echo $list->title; ?>" title="<?php echo $list->title; ?>">
					<div class="about-gal-textwrap">
						<div class="display-table">
							<div class="display-tablecell">
								<div class="about-gal-text">
									<p><?php echo $list->title; ?></p>
									<a href="<?php echo $list->image; ?>" data-fancybox-group="gallery" class="btn fancybox">View</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</li>
                        <?php } } ?>
		</ul>
		<div class="clear"></div>
                <?php if(!empty($next_gallery_list)){ ?>
        <span id="loading_btn" class="loading">
        	<a href="javascript:void(0);" onclick=load_more(); class="view-btn">Load more</a>
        </span>
                <?php } ?>
	</div>
	<!--gallery section end-->
<?php $this->load->view($footer);
