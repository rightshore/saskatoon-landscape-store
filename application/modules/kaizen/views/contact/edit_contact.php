<?php $this->load->view($header); ?>
<?php $this->load->view($left); ?>
<script type="text/javascript" src="<?php echo site_url("public/ckeditor/ckeditor.js");?>"></script>
<link rel="stylesheet" href="<?php echo site_url("public/validator/css/validationEngine.jquery.css");?>" type="text/css"/>
<script src="<?php echo site_url("public/validator/js/languages/jquery.validationEngine-en.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo site_url("public/validator/js/jquery.validationEngine.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo site_url("public/js/jquery.mask.min.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url("public/js/jquery.fancybox.js");?>" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url("public/css/jquery.fancybox.css");?>" type="text/css"/>
<script>
$('.fancybox').fancybox({
  width:'1200',
  height: '800'
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('.phone_us').mask('000-000-0000');
	$('.phone_us1').mask('(000) 000-0000');
	$("#cont").validationEngine();
});
function form_submit(){
$('#cont').submit();
}

function addAnothercontent(id){ 
	var count = $('#count_content').val();
        var pid = $('#featured_block_id').val();
	var count_val = parseInt(count)+1;
	jQuery('#count_content').val(count_val);
	//alert(count_val);
	jQuery.ajax({
			   type: "POST",
                           async: false,
				url : '<?php echo site_url("kaizen/contact/add_subjects/");?>',
              data: { count:count,id:id,pid:pid},
				dataType : "html",
				success: function(data)
				{
					if(data)
					{
							jQuery("#add_subjects").prepend(data);
					}
					else
					{
						//alert("Sorry, Some problem is there. Please try again.");
					}
				},
				error : function() 
				{
					alert("Sorry, The requested property could not be found.");		
				}
			});
                        }
</script>
<script type="text/javascript">
     $(function() {
     $('.iframe-btn').fancybox({	
      	'width'		: 900,
      	'height'	: 600,
      	'type'		: 'iframe',
              'autoScale'    	: false
          });
    });
</script>
<div class="rightDiv">
<div class="right-outer">
  <h3 class="title">Edit
    <?php if(isset($details->title)){echo $details->title;}?>
  </h3>
  <div class="clear"></div>
  <div class="mid-block padbot40">
    <div class="mid-content web-cont-mid" style="min-height:180px !important">
      <div id="webcont-form">
        <div id="member-form" class="midarea">
          <?php 
		  $attributes = array('name' => 'cont', 'id' => 'cont');
		  echo form_open_multipart('kaizen/contact/addedit/'.$details->id,$attributes);
		  echo form_hidden('contact_id', $details->id);
		  ?>
          <?php
		if($this->session->userdata('ERROR_MSG')==TRUE){
			echo '<div class="notific_error">
					<h2 align="center" style="color:#000;">'.$this->session->userdata('ERROR_MSG').'</h1></div>';
			$this->session->unset_userdata('ERROR_MSG');
		}
		if($this->session->userdata('SUCC_MSG')==TRUE){
			echo '<div class="notific_suc"><h2 align="center" style="color:#000;">'.$this->session->userdata('SUCC_MSG').'</h1></div>';
			$this->session->unset_userdata('SUCC_MSG');
		}
		?>
          <?php echo validation_errors('<div class="notific_error">', '</div>'); ?>
          <div class="single-column" >
            <label class="question-label">Enter Title <span>*</span> </label>
            <input type="text" name="contact_title" id="contact_title" value="<?php if(isset($details->title)){echo $details->title;}?>" class="inputinpt validate[required]" />
          </div>
         
            
            
            <div class="single-column">
              <label class="question-label">Address </label>
              <?php
                                    if(!empty($details->address)){
                                        $cont_txt1 = outputEscapeString($details->address);
                                    }
                                    else{
                                        $cont_txt1 = "";
                                    }?>
              <textarea id="address" name="address" class="editor"><?php echo $cont_txt1;?></textarea>
            </div>
            
            <div class="single-column">
              <label class="question-label">Contact Form Content </label>
              <?php
                                    if(!empty($details->content)){
                                        $cont_txt11 = outputEscapeString($details->content);
                                    }
                                    else{
                                        $cont_txt11 = "";
                                    }?>
              <textarea id="content" name="content" class="editor"><?php echo $cont_txt11;?></textarea>
            </div>
            
            <div class="single-column" >
	          <label class="question-label">Contact Photo - Size Requirement:  1002 X 867 px ( W x H )</label><p class="sizetxt"></p>
	          <input id="htmlfile2" name="htmlfile2" value="<?php if(!empty($details->image)) echo $details->image; ?>" type="text">
	          <a href="<? echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile2&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>
               
            <div class="single-column" >
	          <label class="question-label">Logo Photo - Size Requirement:  319 x 324 px ( W x H )</label><p class="sizetxt"></p>
	          <input id="htmlfile1" name="htmlfile1" value="<?php if(!empty($details->logo_photo)) echo $details->logo_photo; ?>" type="text">
	          <a href="<?php echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile1&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>
              
            <div class="single-column">
                        <ul>
                            <li class="fullWidth">
                                <label class="labelname">Contact Email: </label>
                                <input type="text" name="contact_email" id="contact_email" value="<?php if(isset($details->contact_email)){echo $details->contact_email;}?>" class="validate[optional,custom[email]]"/>
                            </li>
                        </ul>
                    </div>
                    
                    
                    <div class="single-column" >
            <label class="question-label">Fax <span></span> </label>
            <input type="text" name="fax" id="fax" value="<?php if(isset($details->fax)){echo $details->fax;}?>" class="inputinpt" />
          </div>
                    
            
            <div class="single-column" >
    	<label class="question-label">Phone:</label>
<input style="" type="text" name="contact_phone" id="contact_phone" value="<?php if(isset($details->contact_phone)){echo $details->contact_phone;}?>" class="inputinpt phone_us1" />
    </div>
            
            <div class="single-column" >
            <label class="question-label">Longitude <span>*</span> </label>
            <input type="text" name="longitude" id="longitude" value="<?php if(isset($details->longitude)){echo $details->longitude;}?>" class="inputinpt validate[required,custom[number]]" />
          </div>
            
            <div class="single-column" >
            <label class="question-label">Latitude <span>*</span> </label>
            <input type="text" name="latitude" id="latitude" value="<?php if(isset($details->latitude)){echo $details->latitude;}?>" class="inputinpt validate[required,custom[number]]" />
          </div>
          
          
          <div class="single-column">
              <label class="question-label">Working Time </label>
              <?php
                                    if(!empty($details->work_time)){
                                        $cont_txt1 = outputEscapeString($details->work_time);
                                    }
                                    else{
                                        $cont_txt1 = "";
                                    }?>
              <textarea id="work_time" name="work_time" class="editor"><?php echo $cont_txt1;?></textarea>
            </div>
            
            <?php /*?><div class="single-column" >
            <label class="question-label">copy righy <span>*</span> </label>
            <input type="text" name="copy_righy" id="copy_righy" value="<?php if(isset($details->copy_righy)){echo $details->copy_righy;}?>" class="inputinpt validate[required]" />
          </div><?php */?>
        <!--<div class="single-column">
                             <input type="hidden" name="count_content" id="count_content" value="1" />
                     <label class="labelname" ><a href="javascript:void(0);" onclick="addAnothercontent();" >+Add Subjects</a></label>
                             <div id="add_subjects" class=""></div>
             </div>-->    

              
<!--               <div class="single-column">
                             <input type="hidden" name="count_content" id="count_content" value="1" />
    <label class="labelname" ><a href="javascript:void(0);" onclick="addAnothercontent('','','','','','','','','','','','');" >+Add Contact</a></label>
                             <div id="add_content" class=""></div>
             </div>-->
             
             
          <div class="bottonserright" style="padding-bottom:20px;"> 
            <a href="javascript:void(0);" class="web-red-btn" onClick="form_submit();"><span>Save</span></a> <?php echo form_close();?> </div>
        </div>
        </div>
        <div class="bodybottom"> </div>
      </div>
    </div>
    
  </div></div>
  <div class="clear"></div>
  
  <script type="text/javascript">
	var editor, html = '';
	if (editor ){
   	editor.destroy();
	editor = null;
	}
    CKEDITOR.replace( 'work_time' ,{
	width : '95%',
	contentsCss : '<?php echo site_url("public/css/style_ck.css");?>',	
	filebrowserBrowseUrl : '<?php echo site_url("public/ckfinder/ckfinder.html");?>',
	filebrowserUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files");?>',
	filebrowserImageUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images");?>',
	filebrowserFlashUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash");?>'
});
var editor, html = '';
	if (editor ){
   	editor.destroy();
	editor = null;
	}
    CKEDITOR.replace( 'content' ,{
	width : '95%',
	contentsCss : '<?php echo site_url("public/css/style_ck.css");?>',	
	filebrowserBrowseUrl : '<?php echo site_url("public/ckfinder/ckfinder.html");?>',
	filebrowserUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files");?>',
	filebrowserImageUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images");?>',
	filebrowserFlashUploadUrl : '<?php echo site_url("public/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash");?>'
});
</script>
  <?php $this->load->view($footer); ?>
</div>
<script type="text/javascript">
<?php if(!empty($content_list)) {
   // pre($content_list);
     foreach ($content_list as $content){
                ?>
 addAnothercontent('<?php echo $content->id; ?>');
     <?php } }?>
</script>
