<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php //pre($this->data['cmspages_top_header']);exit;
if(!empty($this->data['hooks_meta']->meta_title))
{
	echo '<title>'.$this->data['hooks_meta']->meta_title.'</title>'."\n";
}
else if(!empty($this->data['hooks_meta']->title))
{
	echo '<title>'.$this->data['hooks_meta']->title.'</title>'."\n";
}
else
{
	echo '<title>'.$this->data['site_settings']->site_name.'</title>'."\n";
}

if(!empty($meta_keyword))
{
	$meta_keywords= $meta_keyword;
}
else if(!empty($this->data['hooks_meta']->meta_keyword))
{
	$meta_keywords= $this->data['hooks_meta']->meta_keyword;
}
else
{
	$meta_keywords= $this->data['site_settings']->meta_keyword;
}

if(!empty($meta_description))
{
	$meta_descriptions= $meta_description;
}
else if(!empty($this->data['hooks_meta']->meta_description))
{
	$meta_descriptions= $this->data['hooks_meta']->meta_description;
}
else
{
	$meta_descriptions= $this->data['site_settings']->meta_description;
}	
$mainpage_link = $this->uri->segment(1);
$meta = array(
        array('name' => 'robots', 'content' => 'index, follow, all'),
        array('name' => 'description', 'content' => $meta_descriptions),
        array('name' => 'keywords', 'content' => $meta_keywords),
		array('name' => 'X-UA-Compatible', 'content' => 'IE=edge', 'type' => 'equiv'),
		array('name' => 'viewport', 'content' => 'width=device-width, initial-scale=1.0'),
        array('name' => 'Content-type', 'content' => 'text/html; charset=utf-8', 'type' => 'equiv')
		
    );

$page_link = xss_clean($this->uri->segment(1));
if($page_link=='pages')
{
	$page_link = xss_clean($this->uri->segment(2));
}
echo meta($meta);
if(!empty($this->data['site_settings']->site_verification)){
    echo '<meta name="google-site-verification" content="'.$this->data['site_settings']->site_verification.'" />';
}
?>

<!--font section start-->
<link href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800,900|Sacramento" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700" rel="stylesheet"><!--font section end-->

<!--css section start-->
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/style.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/slider.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/media.css'); ?>" />
<link href="<?php echo base_url('public/default/images/'); ?>/home_pro_icon.png" rel="shortcut icon" type="image/ico" />

<!--script section start-->
<script type='text/javascript' src='<?php echo base_url('public/default/js/jquery-1.8.3.min.js'); ?>'></script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/responsiveslides.min.js'); ?>'></script>
<script>
	$(function() {
    	$(".rslides").responsiveSlides({
			pager: true,
			nav: true,
			auto: false
		});
  	});
</script>
<script type='text/javascript' src='<?php echo base_url('public/default/js/custom.js'); ?>'></script>
<?php echo link_tag("public/validator/css/validationEngine.jquery.css")."\n"; ?>
<script src="<?php echo base_url("public/validator/js/languages/jquery.validationEngine-en.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url("public/validator/js/jquery.validationEngine.js");?>" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function(){
    $("#frm_search").validationEngine({promptPosition:"bottomLeft"});
});
</script>
<!--script section end-->
</head>
<!--script section end-->
</head>

<body>
<?php //pre($this->data['contact']); ?>
	<!--top section start-->
	<div class="top-header">
		<div class="wrapper">
			<ul class="top-info">
                                <?php if(!empty($this->data['contact']->contact_phone)){ ?><li class="call"><p><?php echo $this->data['contact']->contact_phone; ?></p></li><?php } ?>
				<?php if(!empty($this->data['contact']->address)){ ?><li class="addrs"><p><?php echo $this->data['contact']->address; ?></p></li><?php } ?>
			</ul>
			<ul class="top-social">
<!--				<li><a href="#" class="fb" target="_blank"></a></li>
				<li><a href="#" class="insta" target="_blank"></a></li>
                                <li><a href="#" class="pint" target="_blank"></a></li>-->
                                <?php echo socialLinks(); ?>
			</ul>
		</div>
	</div>
   	<!--top section end-->
	
	
	<!--banner section start-->
        <?php 
        $banner='';
        if(!empty($commonbanner->banner_photo) && is_file(file_upload_path($commonbanner->banner_photo))){
            $banner=$commonbanner->banner_photo;
        } 
        ?>
        <div class="banner" style="background: url(<?php if(!empty($banner)) echo $banner; else echo ''; ?>) no-repeat center center;">
	
		<!--header section start-->
		<div class="header">
			<div class="wrapper">
                            <?php 
                    $header_logo_photo = base_url('public/default/images/logo.png');
                    //pre($this->data['site_settings']);
                    if(!empty($this->data['site_settings']->logo_photo) && is_file(file_upload_path($this->data['site_settings']->logo_photo))){
                        $header_logo_photo = $this->data['site_settings']->logo_photo;
                    }
                     ?>
				<a href="<?php echo site_url(); ?>"><img src="<?php echo $header_logo_photo; ?>" alt="<?php echo $this->data['site_settings']->site_name; ?>" title="<?php echo $this->data['site_settings']->site_name; ?>" class="logo"/></a>
				<div class="navigation">
					<a class="responsive"><span></span></a>
                                        <?php echo $hooks_cmspages_list; ?>
<!--					<ul class="nav">
						<li><a href="home.html" class="active">Home</a></li>
						<li><a href="about.html">About Us</a></li>
						<li><a href="products.html">Products</a></li>
						<li><a href="gallery.html">Gallery</a></li>
						<li><a href="contact.html">Contact Us</a></li>
                        
					</ul>-->
				</div>
			</div>
		</div>
		<!--header section end-->
        
        <!-- search section start -->
        <div class="search">
            <div class="wrapper">
                <form action="<?php echo site_url('search'); ?>" method="get" name="frm_search" id="frm_search">
                    <div class="search-container">
                        <input type="text" class="validate[required]" placeholder="Search for Products" id="search" name="search" value="<?php if(!empty($search)) echo $search; ?>"/>
                        <input type="submit" value="Search" class="btn-search"/>
                    </div>
                </form>
            </div>
        </div>
        <!-- search section end -->
		
	</div>
	<!--banner section end-->