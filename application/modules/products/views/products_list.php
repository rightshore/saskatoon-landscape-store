<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header); ?>
<!--content section start-->
<div class="product-form">
        <div class="wrapper">
                <h2><?php echo $this->data['hooks_meta']->title; ?></h2>
    <div class="pro-breadcrumb">
        <ul>
                <li class="active">You are Here:</li>
            <li><a href="<?php echo site_url('products'); ?>">Products</a></li>
            <li>></li>
            <li><a href="<?php echo site_url('products/category/'.$main_cat_dtls->page_link); ?>"><?php echo $main_cat_dtls->title; ?></a></li>
            <li>></li>
            <li><a href="<?php echo site_url('products/listing/'.$cat_dtls->page_link); ?>"><?php echo $cat_dtls->title; ?></a></li>
        </ul>
    </div>
        </div>
</div>

<div class="product-section">
        <div class="wrapper">
    <div class="product-left">
        <?php if(!empty($prod_list)){ ?>
        <div class="product-blocks">
            <ul>
                <?php foreach($prod_list as $prod){ ?>
                <li>
                     <?php if(!empty($prod->image)){ ?>
                    <div class="image">
                        <a href="<?php echo site_url('products/details/'.$prod->page_link); ?>">
                            <img src="<?php echo $prod->image; ?>" alt="" title=""></a>
                    </div>
                    <?php } ?>
                    <div class="detail">
                        <h5><a href="<?php echo site_url('products/details/'.$prod->page_link); ?>"><?php echo $prod->title; ?></a></h5>
                        <?php if(!empty($prod->excerpt)) echo '<p>'.$prod->excerpt.'</p>'; ?>
                        <a href="<?php echo site_url('products/details/'.$prod->page_link); ?>" class="btn">More</a>
                        <div class="clear"></div>
                    </div>
                </li>
                <?php } ?>
            </ul>
            <div class="clear"></div>
        </div>
        <?php }else{ echo '<p>No Product Found.</p>'; } ?>
    </div>



<div class="product-right">
    <?php $this->load->view($right); ?>
    <div class="clear"></div>
</div>
    </div>
    </div>
    <div class="clear"></div>
<!--content section end-->
<?php $this->load->view($footer);
