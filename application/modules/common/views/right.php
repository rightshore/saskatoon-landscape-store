<?php 
if(!empty($hooks_meta->parent_id)) {
    $where=array('parent_id'=>$hooks_meta->parent_id,'is_active'=>1);
    $order_by=array('display_order'=>'asc');
    $subpage_list=$this->model_pages->select_row('cms_pages',$where,$order_by);
    $where=array('id'=>$hooks_meta->parent_id);
    $page_details=$this->model_pages->selectOne('cms_pages',$where);
    //$header=$page_details->title;
}else{
    $where=array('parent_id'=>$hooks_meta->id,'is_active'=>1);
    $order_by=array('display_order'=>'asc');
    $subpage_list=$this->model_pages->select_row('cms_pages',$where,$order_by);
    //$header=$hooks_meta->title;
}
if(!empty($subpage_list)){
?>
<div class="inner-right">
    <div class="right-block">
            <ul class="right-menu">
                <?php foreach($subpage_list as $list) {
                    $url=urlByPageLink($list->page_link);
                    ?>
      <li><a <?php if($list->id==$hooks_meta->id) echo 'class="active"'; ?> href="<?php echo $url; ?>"><?php echo $list->title; ?></a></li>
                <?php } ?>
            </ul>
    </div>
</div>
<?php  } ?>