<?php $this->load->view($header); ?>
<?php $this->load->view($left); ?>
<script type="text/javascript" src="<?php echo site_url("public/ckeditor/ckeditor.js");?>"></script>
<link rel="stylesheet" href="<?php echo site_url("public/validator/css/validationEngine.jquery.css");?>" type="text/css"/>
<script src="<?php echo site_url("public/validator/js/languages/jquery.validationEngine-en.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo site_url("public/validator/js/jquery.validationEngine.js");?>" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url("public/js/jquery.fancybox.js");?>" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo base_url("public/css/jquery.fancybox.css");?>" type="text/css"/>
<script>
$('.fancybox').fancybox({
  width:'1200',
  height: '800'
});
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#cont").validationEngine();
	});
function form_submit(){
    $('#selected_id option').prop('selected', true);
$('#cont').submit();
}

</script>
<script type="text/javascript">
     $(function() {
     $('.iframe-btn').fancybox({	
      	'width'		: 900,
      	'height'	: 600,
      	'type'		: 'iframe',
              'autoScale'    	: false
          });
    });
</script>
<div class="rightDiv">
	<div class="right-outer">
		<?php if(isset($details->id) && $details->id >0){?> <h3 class="title">Edit <?php if(isset($details->title)){echo $details->title;}?></h3> <?php }
    else {?> <h3 class="title">Add Common Banner</h3> <?php } ?>
		
        <div class="clear"></div>
        <div class="mid-block padbot40">
	        <div class="mid-content web-cont-mid">
	            <div id="webcont-form">
        		<div id="member-form" class="midarea">
			<?php 
		  $attributes = array('name' => 'cont', 'id' => 'cont');
		  echo form_open_multipart('kaizen/commonbanner/addedit/'.$details->id,$attributes);
		  echo form_hidden('commonbanner_id', $details->id);
		  ?>
			<?php
		if($this->session->userdata('ERROR_MSG')==TRUE){
			echo '<div class="notific_error">
					<h2 align="center" style="color:#fff;">'.$this->session->userdata('ERROR_MSG').'</h1></div>';
			$this->session->unset_userdata('ERROR_MSG');
		}
		if($this->session->userdata('SUCC_MSG')==TRUE){
			echo '<div class="notific_suc"><h2 align="center" style="color:#000;">'.$this->session->userdata('SUCC_MSG').'</h1></div>';
			$this->session->unset_userdata('SUCC_MSG');
		}
		?>
			<?php echo validation_errors('<div class="notific_error">', '</div>'); ?>
                            <div class="single-column" >
                                <label class="question-label">Enter Title  <span>*</span></label>
                                    <input type="text" name="commonbanner_title" id="commonbanner_title" value="<?php if(isset($details->title)){echo $details->title;}?>" class="inputinpt validate[required]" />
                            </div>
                            <div class="single-column" >
                                    <label class="labelname"><?php if(empty($add_for)){ ?>Select Page  : <?php }else{  ?> Select Page  <?php } ?><span>*</span></label>
                                    <select name="page_id[]" id="cat_id" size="10" multiple="multiple" style="width:200px; height:200px;"  >
                                            <option value="0" disabled="disabled"><?php if(empty($add_for)){ ?>Page : <?php }else{  ?> Page <?php } ?></option>
                                            <?php 	echo $pageList; 	?>
                                    </select>
                                   <input type="button" name="left" value="=>" id="MoveRight"/>
                                   <input type="button" name="right" value="<=" id="MoveLeft" />
                                    <select name="selected_id[]" id="selected_id" style="width:200px; height:200px;" multiple="multiple" size="10" class="inplogin validate[required]">
                    
                    <?php 	echo $selectedprg; 	?>
													
                                            </select>
                                    
                            </div>
                    <div class="single-column" >
	          <label class="question-label">Common Banner Photo - Size Requirement:  2000 x 531 px ( W x H )</label><p class="sizetxt"></p>	          <input id="htmlfile1" name="htmlfile1" value="<?php if(!empty($details->banner_photo)) echo $details->banner_photo; ?>" type="text">
	          <a href="<? echo front_base_url().'filemanager/dialog.php?type=1&field_id=htmlfile1&relative_url=1'?>?>" class="btn iframe-btn" type="button">Open File Manager</a>           
	        </div>
                            
<!--              <div class="single-column cont-banner-upload" >
    			<label class="labelname">Banner Image:</label>
                <div id="imagedeletesuccess"></div>
                <a class="fancybox fancybox.iframe upload removebtn" href="<?php echo base_url("kaizen/commonbanner/newcrop"); ?>?image_id=htmlfile1&folder_name=commonbanner&img_sceen=img_sceen&prev_img=<?php if(!empty($details->banner_photo)){ echo $details->banner_photo; }?>&controller=commonbanner&height=448&width=2000" >Upload</a>
                <input type="hidden" id="htmlfile1" name="htmlfile1" value="<?php if(!empty($details->banner_photo)){ echo $details->banner_photo; } ?>" />
                <p class="sizetxt">Size Requirement: 2000 x 448px</p>
	           <div id="banner_photodiv">
                <?php if(!empty($details->banner_photo) && is_file(file_upload_absolute_path()."commonbanner/".$details->banner_photo))	
				{
				 
					$logo_photo1=file_upload_base_url()."commonbanner/".$details->banner_photo;

				}
				else
				{
					$logo_photo1=file_upload_base_url()."noiphoto.png";
				}?>
                <img id="img_sceen" src="<?php echo $logo_photo1;?>" style="background:white;" width="150" height="100"/>
                <?php if(!empty($details->banner_photo)){ ?>
                <a href="javascript:void(0);" title="Remove" onClick="imagedeletecommon('<?php echo $details->id;?>','id','banner_photo','commonbanner','commonbanner','commonbanner','htmlfile1','imagedeletesuccess','banner_photodiv');" class="removebtn" >Remove</a> 
                <?php } ?>
                </div>
  				</div>-->

<!--                <div class="single-column" >
              <label class="question-label">Button Text<span> *</span></label>
              <input type="text" name="button_text" id="button_text" value="<?php if(isset($details->button)){echo $details->button;}?>" class="inputinpt validate[required]" />
            </div>

            <div class="single-column">
                 <label class="labelname">Button Type<span> *</span></label>
     <input type="radio" id="button_type" name="button_type" value="1" <?php if(!empty($details->button_type) && $details->button_type == '1'){echo "checked";} ?>  onclick="show_page_div('button_page_div','button_ext_div');" class="inputinpt validate[required]"/> Select Page
    <input type="radio" id="button_type" name="button_type" value="2" <?php if(!empty($details->button_type) && $details->button_type == '2'){echo "checked";} ?>  onclick="show_ext_div('button_page_div','button_ext_div');"  class="inputinpt validate[required]"/> External Link
                   </div>-->
                   
<!--                   <script>
                   function show_page_div(a,b){ 
					   document.getElementById(a).style.display = '';
					   document.getElementById(b).style.display = 'none';
				   }
				   function show_ext_div(a,b){
					   document.getElementById(a).style.display = 'none';
					   document.getElementById(b).style.display = '';
				   }
                   </script>
                   <div class="single-column" id="button_page_div" style="display:<?php if(!empty($details->button_type) && $details->button_type == '1'){echo  ''; }else{echo  'none';} ?>;">
              <label class="question-label">Select Page *:</label>
              <select name="page_link" id="page_link" class="inputinpt validate[required]">
                  <option value="" >-Select Pages-</option>
           <?php if(!empty($page_list)) { 
                  foreach($page_list as $page){
                  if($page->id!=1) { ?>
   <option value="<?php echo $page->page_link; ?>" <?php if(!empty($details->selected_page_link) && ($details->selected_page_link == $page->page_link)){echo "selected"; }else{ echo ""; } ?> ><?php echo $page->title; ?></option>
                  <?php } } } ?>
              </select>
            </div>
                   <div class="single-column" id="button_ext_div" style="display:<?php if(!empty($details->button_type) && $details->button_type == '2'){echo  ''; }else{echo  'none';} ?>;">
              <label class="question-label">External Link URL *:</label>
              <input type="text" name="external_url" id="external_url" value="<?php if(!empty($details->external_url) && $details->button_type == '2'){ echo $details->external_url;} ?>" class="inputinpt validate[required,custom[url]]" />
            </div>-->

                            
                            <div class="single-column" >
                                <label class="question-label">Status:<span>*</span></label>
                                <input type="radio" name="is_active" id="is_active" value="1" 
                                <?php echo ((isset($details->is_active) && $details->is_active ==1)?'checked="checked"':'')?>/>
                              &nbsp;Active &nbsp;&nbsp;
                              <input type="radio" name="is_active" id="is_active_1" value="0" <?php echo ((isset($details->is_active) && $details->is_active ==0)?'checked="checked"':'')?> />
                              &nbsp;Inactive &nbsp;&nbsp; 
                            </div>
			
			<div class="bottonserright" style="padding-bottom:20px;"> 
                            <a href="javascript:void(0);" title="Delete" onClick="rowdelete('<?php echo $details->id; ?>','commonbanner');" class="web-red-btn" <?php if(isset($details->id) && $details->id >0){}else{echo 'style="display:none;"';}?>> <span>Delete</span> </a> <a href="javascript:void(0);" class="web-red-btn" onClick="form_submit();"><span>Save</span></a> <?php echo form_close();?> </div>
		</div>
	</div>
	<div class="bodybottom"> </div>
</div>
</div>
<div class="rt-block">
  <?php $this->load->view($right); ?>
</div>
</div>
    <div class="clear"></div>
                        <?php $this->load->view($footer); ?>
<script type="text/javascript">
     $(function() {
					
					$("#MoveRight,#MoveLeft").click(function(event) {
						var id = $(event.target).attr("id");
						var selectFrom = id == "MoveRight" ? "#cat_id" : "#selected_id";
						var moveTo = id == "MoveRight" ? "#selected_id" : "#cat_id";

						var selectedItems = $(selectFrom + " :selected").toArray();
						$(moveTo).append(selectedItems);
					   
					});
					});
</script>