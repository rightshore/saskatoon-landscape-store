<?php
class Model_search extends MY_Model{
	
	var $site_id='';
	public function __construct(){
		parent::__construct();	
		$this->site_id=$this->config->item("SITE_ID");	
	}
	
        
  function get_search($table,$field,$match) 
  {
	    $field_list=explode(',',$field);		
		if(!empty($field_list))
		{
                    $str_like="(";
			$cnt=0;
			foreach($field_list as $list){
				$cnt++;
				if($cnt==1){
				#$this->db->like($list,$match);
                                    $str_like.=" $list LIKE '%$match%'";
				}
				else
				{
				#$this->db->or_like($list,$match);
                                    $str_like.=" OR $list LIKE '%$match%'";
				}
			}
                        $str_like.=")";
                        $this->db->where($str_like,NULL,FALSE);
		}
		//$this->db->where('site_id',$this->site_id);
		$this->db->where('is_active',1);
		$result_list = $this->db->get($this->db->dbprefix($table));
		if($result_list)
		{
			#echo $this->db->last_query();
			return $result_list->result();	
		}
		else{
			return false;
		} 
  }
}
?>