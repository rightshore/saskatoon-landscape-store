<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site_map extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		if( ! $this->session->userdata('web_admin_logged_in')) {
			redirect('kaizen/welcome','refresh');
		}
		$this->load->vars( array(
		  'global' => 'Available to all views',
		  'header' => 'common/header',
		  'left' => 'common/left',
		  'footer' => 'common/footer',
		  'right' => 'common/right'
		));
                $this->load->model("modelsite_map");

	}

	public function index()
	{
            $data = array();
            //$data['page_list_arr']=$this->modelsite_map->getAllParentDetails();
            $data['page_list_menu']=$this->get_category_menunew();
            $this->load->view('site_map/site_map',$data);
	}

        public function get_category_menunew(){

	    $cms_page_arr = $this->modelsite_map->getallcmspages(); //echo '<pre>';print_r($cms_page_arr);
            $menus_array = array();
            foreach ($cms_page_arr as $rs_menu_id){
              $menus_array[$rs_menu_id->id] = array('id' => $rs_menu_id->id,'title' => $rs_menu_id->title,'parent_id' => $rs_menu_id->parent_id,'page_link' => $rs_menu_id->page_link,'display_order' => $rs_menu_id->display_order);
            }
            $TOP_NAV_MENU = '';
            $category_menu = $this->generate_menunew(0,$menus_array, $TOP_NAV_MENU, 0);
            return $category_menu;


	}

        public function generate_menunew($parent,$menus_array, &$TOP_NAV_MENU, $level_depth=0,$count = 1){
		$has_childs = false;
		$level_depth++;

		foreach($menus_array as $key => $value){
      if ($value['parent_id'] == $parent){
        if ($has_childs === false){
          $has_childs = true;
					if($level_depth==1){
            $TOP_NAV_MENU .= "<ol class='dd-list' >\n";
          }
					else{
            $TOP_NAV_MENU .= "<ol class='dd-list' >\n";
          }
        }
				if($value['page_link']=="blog"){
				}
				else{
					$TOP_NAV_MENU .= '<li class="dd-item" data-id="'.$value['id'].'" id="id_'.$value['id'].'"><div class="dd-handle">'. $value['title'] .'</div>';
				}
                                $count++;
				if($level_depth<=3){
          $this->generate_menunew($key,$menus_array,$TOP_NAV_MENU,$level_depth,$count);
        }
         //call function again to generate nested list for subcategories belonging to this category
         $TOP_NAV_MENU .= "</li>\n";
       }
     }
     if ($has_childs === true){
       $TOP_NAV_MENU .= "</ol>\n";
     }
     if($level_depth=="2")
       $TOP_NAV_MENU .= "<hr>";
     return $TOP_NAV_MENU;
	}


        public function changedisplayordernew(){

            $data_arr = array();
            $fulldata = $this->input->post("fulldata");
            $fulldata_arr = json_decode($fulldata);
            //print_r($fulldata_arr);
            $i = 1;

            foreach($fulldata_arr as $fuldata){
                $data_arr[] = array('id' =>$fuldata->id,
                                    'display_order' => $i);

                //print_r($fuldata->children);
                $j = 1;
                if(!empty($fuldata->children)){
                    foreach($fuldata->children as  $fulchldrn){
                        $data_arr[] = array('id' =>$fulchldrn->id,
                                    'display_order' => $j);


                                    $k = 1;
                                    if(!empty($fulchldrn->children)){
                                        foreach($fulchldrn->children as $fulchldrnchld){
                                            $data_arr[] = array('id' =>$fulchldrnchld->id,
                                                        'display_order' => $k);


                                                        $m = 1;
                                                            if(!empty($fulchldrnchld->children)){
                                                                foreach($fulchldrnchld->children as $fulchldrnchldchield){
                                                                    $data_arr[] = array('id' =>$fulchldrnchldchield->id,
                                                                                'display_order' => $m);


                                                                    $m++;
                                                                }
                                                            }



                                            $k++;
                                        }
                                    }


                        $j++;
                    }
                }
                $i++;
            }
            //pre($data_arr);
            //exit;
            $this->db->update_batch($this->db->dbprefix('cms_pages'), $data_arr, 'id');
//            echo $this->db->last_query();
            echo "Sequencing has been changed.";
}

}
