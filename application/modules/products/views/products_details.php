<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header); ?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('public/default/css/jcarousel.connected-carousels.css'); ?>">
<script type="text/javascript" src="<?php echo base_url('public/default/js/jquery.connected-jcarousel.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('public/default/js/jcarousel.connected-carousels.js'); ?>"></script>
<!--content section start-->
<div class="product-form">
        <div class="wrapper">
                <h2><?php echo $this->data['hooks_meta']->title; ?></h2>
    <div class="pro-breadcrumb">
        <ul>
                <li class="active">You are Here:</li>
            <li><a href="<?php echo site_url('products'); ?>">Products</a></li>
            <li>></li>
            <li><a href="<?php echo site_url('products/category/'.$main_cat_dtls->page_link); ?>"><?php echo $main_cat_dtls->title; ?></a></li>
            <li>></li>
            <li><a href="<?php echo site_url('products/listing/'.$cat_dtls->page_link); ?>"><?php echo $cat_dtls->title; ?></a></li>
            <li>></li>
            <li><a href="<?php echo site_url('products/details/'.$prod_dtls->page_link); ?>"><?php echo $prod_dtls->title; ?></a></li>
        </ul>
    </div>
        </div>
</div>

<div class="product-section">
        <div class="wrapper">
    <!--left section start-->
			<div class="product-left">
				<h2><?php echo $prod_dtls->title; ?></h2>
				<?php if(!empty($prod_dtls->excerpt)) echo '<p>'.nl2br($prod_dtls->excerpt).'</p>'; ?>
				<?php if(!empty($gallery_list)){ ?>
				<div class="pro-slider">
					<div class="connected-carousels">
						<div class="stage">
							<div class="carousel carousel-stage">
								<ul>
									<?php foreach($gallery_list as $gal){ 
											if(!empty($gal->image) && is_file(file_upload_path($gal->image))){
									?>
									<li style="background: url(<?php echo $gal->image; ?>) no-repeat center 0;"></li>
									<?php } } ?>
							   </ul>
							</div>
							<a href="#" class="prev prev-stage"><span>&lsaquo;</span></a>
							<a href="#" class="next next-stage"><span>&rsaquo;</span></a>
						</div>
						<div class="navigation">
							<a href="#" class="prev prev-navigation">&lsaquo;</a>
							<a href="#" class="next next-navigation">&rsaquo;</a>
							<div class="carousel carousel-navigation">
								<ul>
									<?php foreach($gallery_list as $gal){ 
										if(!empty($gal->image) && is_file(file_upload_path($gal->image))){
										?>
									<li style="background: url(<?php echo $gal->image; ?>) no-repeat center 0;"></li>
									<?php } } ?>
								</ul>
							</div>
						 </div>
					</div>
					<div class="clear"></div>
				</div>
				<?php } ?>
				<?php if(!empty($prod_dtls->description)) echo outputEscapeString($prod_dtls->description); ?>
				<a href="javascript:history.go(-1);" class="view-btn">Back</a>
			</div>
			<!--left section end-->



<div class="product-right">
    <?php $this->load->view($right); ?>
    <div class="clear"></div>
</div>
    </div>
    </div>
    <div class="clear"></div>
<!--content section end-->
<?php $this->load->view($footer);
