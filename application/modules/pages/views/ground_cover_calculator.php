<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header); //pre($circlr_except_list); exit; 
?>
<script type="text/javascript">
function CalculateSumRect(RAtext, RBtext, RCtext, RDtext, REtext, form)
{
var RA = parseFloat(RAtext);
var RB = parseFloat(RBtext);
var RC = parseFloat(RCtext);
var RD = parseFloat(RDtext);
var RE = parseFloat(REtext);
form.RAnswer.value = Math.round(100*((RA * 12) + RB) * ((RC*12) + RD)/144)/100;
form.RAnswer2.value = Math.round(100*(((RA * 12) + RB) * ((RC*12) + RD) * RE)/1728)/100;
form.RAnswer3.value = Math.round(100*((((RA * 12) + RB) * ((RC*12) + RD) * RE)/1728)/27)/100;
}

function ClearFormRect(form)
{
form.input_RA.value = "0";
form.input_RB.value = "0";
form.input_RC.value = "0";
form.input_RD.value = "0";
form.input_RE.value = "0";
form.RAnswer.value = "";
form.RAnswer2.value = "";
form.RAnswer3.value = "";
}


function CalculateSumCircle(CAtext, CBtext, CCtext, form)
{
var CA = parseFloat(CAtext);
var CB = parseFloat(CBtext);
var CC = parseFloat(CCtext);
form.CAnswer.value = Math.round(100*(((CA * 12) + CB) * ((CA*12) + CB)*3.14)/144)/100;
form.CAnswer2.value = Math.round(100*((((CA * 12) + CB) * ((CA*12) + CB) * CC)*3.14)/1728)/100;
form.CAnswer3.value = Math.round(100*(((((CA * 12) + CB) * ((CA*12) + CB) * CC)*3.14)/1728)/27)/100;
}

function ClearFormCircle(form)
{
form.input_CA.value = "0";
form.input_CB.value = "0";
form.input_CC.value = "0";

form.CAnswer.value = "";
form.CAnswer2.value = "";
form.CAnswer3.value = "";
}

function CalculateSumTri(TAtext, TBtext, TCtext, TDtext, TEtext, form)
{
var TA = parseFloat(TAtext);
var TB = parseFloat(TBtext);
var TC = parseFloat(TCtext);
var TD = parseFloat(TDtext);
var TE = parseFloat(TEtext);
form.TAnswer.value = Math.round(100*(((TA * 12) + TB) * ((TC*12) + TD)/144)/2)/100;
form.TAnswer2.value = Math.round(100*((((TA * 12) + TB) * ((TC*12) + TD) * TE)/1728)/2)/100;
form.TAnswer3.value = Math.round(100*(((((TA * 12) + TB) * ((TC*12) + TD) * TE)/1728)/27)/2)/100;
}

function ClearFormTri(form)
{
form.input_TA.value = "0";
form.input_TB.value = "0";
form.input_TC.value = "0";
form.input_TD.value = "0";
form.input_TE.value = "0";
form.TAnswer.value = "";
form.TAnswer2.value = "";
form.TAnswer3.value = "";
}
// end of JavaScript functions -->
</script>
<div class="about-section calc-wrap">
    <div class="wrapper">	
    <h2><?php echo $this->data['hooks_meta']->title; ?></h2>
    
    	<div class="calc-block">
    		<h5>Average Depth of Material Needed :</h5>
			<div class="calc-blockin">
				<ul class="side">
                                    <?php if(!empty($this->data['hooks_meta']->content)) echo outputEscapeString($this->data['hooks_meta']->content); ?>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
		
   <form id="form1" name="form1" method="post" action="">
   
   		<div class="calc-block">
			<h5>Rectangular Areas</h5>
			<div class="calc-blockin">
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Length:</label>
						<div class="calc-field-wrap">
							<div class="calc-twocol">
								<label>feet</label>
								<div class="calc-field">
									<input name="input_RA" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
							<div class="calc-twocol">
								<label>inches</label>
								<div class="calc-field">
									<input name="input_RB" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
						</div>
						
					</div>
					<div class="calc-formlt right">
						<label>Square Feet =</label>
						<div class="calc-field">
							<input type="text" name="RAnswer" class="rounded2">
						</div>
					</div>
				</div>
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Width:</label>
						<div class="calc-field-wrap">
							<div class="calc-twocol">
								<label>feet</label>
								<div class="calc-field">
									<input name="input_RC" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
							<div class="calc-twocol">
								<label>inches</label>
								<div class="calc-field">
									<input name="input_RD" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
						</div>
						
					</div>
					<div class="calc-formlt right">
						<label>Cubic Feet =</label>
						<div class="calc-field">
							 <input type="text" name="RAnswer2" class="rounded2">
						</div>
					</div>
				</div>
				
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Depth:</label>
						<div class="calc-field-wrap">
							<div class="calc-twocol">
								<label>inches</label>
								<div class="calc-field">
									<input name="input_RE" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
						</div>
						
					</div>
					<div class="calc-formlt right">
						<label>Cubic Yards =</label>
						<div class="calc-field">
							  <input type="text" name="RAnswer3" class="rounded2">
						</div>
					</div>
				</div>
				
				<div class="calc-submit">
					<input name="AddButton" type="button" value="Calculate" class="submit submit2" onclick="CalculateSumRect(this.form.input_RA.value, this.form.input_RB.value, this.form.input_RC.value, this.form.input_RD.value, this.form.input_RE.value, this.form)">
					<input name="RClearButton" type="button" value="Clear" class="clrfield clrfield2" onclick="ClearFormRect(this.form)">
				</div>
			</div>
			<div class="clear"></div>
		</div>
    </form>
                        
                        
                        
     <form id="form1" name="form1" method="post" action="">
   
   		<div class="calc-block">
			<h5>Circular Areas</h5>
			<div class="calc-blockin">
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Radius:</label>
						<div class="calc-field-wrap">
							<div class="calc-twocol">
								<label>feet</label>
								<div class="calc-field">
									<input name="input_CA" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
							<div class="calc-twocol">
								<label>inches</label>
								<div class="calc-field">
									<input name="input_CB" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
						</div>
						
					</div>
					<div class="calc-formlt right">
						<label>Square Feet =</label>
						<div class="calc-field">
							  <input type="text" name="CAnswer" class="rounded2">
						</div>
					</div>
				</div>
				
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Depth:</label>
						<div class="calc-field-wrap">
							<div class="calc-twocol">
								<label>inches</label>
								<div class="calc-field">
									<input name="input_CC" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
						</div>
						
					</div>
					<div class="calc-formlt right">
						<label>Cubic Feet =</label>
						<div class="calc-field">
							   <input type="text" name="CAnswer2" class="rounded2">
						</div>
					</div>
				</div>
				
				<div class="calc-com">
					<div class="calc-formlt">
						&nbsp;
						
					</div>
					<div class="calc-formlt right">
						<label>Cubic Yards =</label>
						<div class="calc-field">
							    <input type="text" name="CAnswer3" class="rounded2">
						</div>
					</div>
				</div>
				
				<div class="calc-submit">
					<input name="AddButton" type="button" value="Calculate" class="submit submit2" onclick="CalculateSumCircle(this.form.input_CA.value, this.form.input_CB.value,  this.form.input_CC.value, this.form)">
					<input name="CClearButton" type="button" value="Clear" class="clrfield clrfield2" onclick="ClearFormCircle(this.form)">
				</div>
			</div>
			<div class="clear"></div>
		</div>
    </form>
                        
                        
   <form id="form1" name="form1" method="post" action="">
   
   		<div class="calc-block">
			<h5>Triangular Areas</h5>
			<div class="calc-blockin">
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Base:</label>
						<div class="calc-field-wrap">
							<div class="calc-twocol">
								<label>feet</label>
								<div class="calc-field">
									 <input name="input_TA" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
							<div class="calc-twocol">
								<label>inches</label>
								<div class="calc-field">
									<input name="input_TB" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
						</div>
						
					</div>
					<div class="calc-formlt right">
						<label>Square Feet =</label>
						<div class="calc-field">
							  <input type="text" name="TAnswer" class="rounded2">
						</div>
					</div>
				</div>
				
				
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Height:</label>
						<div class="calc-field-wrap">
							<div class="calc-twocol">
								<label>feet</label>
								<div class="calc-field">
									  <input name="input_TC" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
							<div class="calc-twocol">
								<label>inches</label>
								<div class="calc-field">
									<input name="input_TD" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
						</div>
						
					</div>
					<div class="calc-formlt right">
						<label>Cubic Feet =</label>
						<div class="calc-field">
							 <input type="text" name="TAnswer2" class="rounded2">
						</div>
					</div>
				</div>
				
				<div class="calc-com">
					<div class="calc-formlt">
						<label>Depth:</label>
						<div class="calc-field-wrap">
							<div class="calc-twocol">
								<label>inches</label>
								<div class="calc-field">
									<input name="input_TE" type="text" class="rounded1" value="0" onblur="if(this.value == '') this.value = '0';" onfocus="if(this.value == '0')
this.value = '';">
								</div>
							</div>
						</div>
						
					</div>
					<div class="calc-formlt right">
						<label>Cubic Yards =</label>
						<div class="calc-field">
							  <input type="text" name="TAnswer3" class="rounded2">
						</div>
					</div>
				</div>
				
				<div class="calc-submit">
					<input name="AddTButton" type="button" value="Calculate" class="submit submit2" onclick="CalculateSumTri(this.form.input_TA.value, this.form.input_TB.value, this.form.input_TC.value, this.form.input_TD.value, this.form.input_TE.value, this.form)">
					<input name="TClearButton" type="button" value="Clear" class="clrfield clrfield2" onclick="ClearFormTri(this.form)">
				</div>
			</div>
			<div class="clear"></div>
		</div>
    </form>

                        
        <div class="clear"></div>
    </div>
</div>
<!-- about section end --> 
<?php $this->load->view($footer);/*end*/ ?>