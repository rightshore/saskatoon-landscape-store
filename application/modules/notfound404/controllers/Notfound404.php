<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notfound404 extends CI_Controller {
	var $data;
	public function __construct() {
	   parent::__construct();
	   // Set master content and sub-views
	   $this->load->vars( array(
		  'header' => 'common/header',
		  'footer' => 'common/footer'
		));
		$this->load->model('home/model_home');	
                $this->load->model('home/model_meta');
		
	}

	public function index()	{
                        $where=array('page_link'=>'notfound404','is_active'=>1);
			$this->data['hooks_meta'] = $this->model_home->selectOne('cms_pages',$where);
			$this->data['commonbanner'] = $this->model_meta->commonbanner($this->data['hooks_meta']->id);
                        $this->load->view('notfound404/notfound404',$this->data);
	}
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */