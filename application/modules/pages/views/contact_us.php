<?php echo $this->load->view($header); ?>
<script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>


<?php // pre($contact);  ?>

<!--content section start-->
	<div class="contact-info">
		<div class="wrapper">
        	<h2><?php echo $this->data['hooks_meta']->title; ?></h2>
			<ul>
                            <?php if(!empty($contact->address)){ ?>
                            <li>
					<img src="<?php echo base_url('public/default/'); ?>/images/contact_info_icon1.png" alt="" title="">
					<p><?php echo nl2br($contact->address); ?></p>
				</li>
                              <?php } ?>
                                
                              <?php if(!empty($contact->contact_phone) || !empty($contact->fax)){ ?>  
				<li>
					<img src="<?php echo base_url('public/default/'); ?>/images/contact_info_icon2.png" alt="" title="">
					<p>Call: <?php echo $contact->contact_phone.'<br>'; ?>Fax: <?php echo $contact->fax; ?></p>
				</li>
                               <?php } ?>
                                
                               <?php if(!empty($contact->contact_email)){ ?>
				<li>
					<img src="<?php echo base_url('public/default/'); ?>/images/contact_info_icon3.png" alt="" title="">
                                        <p><a href="mailto:<?php echo $contact->contact_email; ?>"><?php echo $contact->contact_email; ?></a></p>
				</li>
                                <?php } ?>
                               <?php if(!empty($contact->work_time)){ ?> 
				<li>
					<img src="<?php echo base_url('public/default/'); ?>/images/contact_info_icon4.png" alt="" title="">
					<?php echo outputEscapeString($contact->work_time); ?>
				</li>
                               <?php } ?>
			</ul>
		</div>
	</div>
	<div class="contact-form-section">
            <?php 
            $back_img='';
            if(!empty($contact->image) && is_file(file_upload_path($contact->image))){
                $back_img=$contact->image;
            }
            ?>
		<span class="contact-pic" style="background: url(<?php echo $back_img; ?>) no-repeat center 0;"></span>
		<div class="wrapper">
			<div class="contact-form" id="contact-left">
				<?php echo outputEscapeString($contact->content); ?>
                                <div id="comment_message"></div>
		<form class="commentForm" method="post" action="<?php echo site_url("common/contact_form");?>" id="commentForm" name="commentForm">
					<label>Name:</label>
					<div class="contact-field">
						<input type="text" placeholder="Name*" name="name" id="name" class="validate[required]">
					</div>
					<label>Email:</label>
					<div class="contact-field">
						 <input type="text" placeholder="Email*" name="email" id="email" class="validate[required,custom[email]]">
					</div>
					<label>Comments:</label>
					<div class="contact-field">
						 <textarea id="comments" name="comments" placeholder="Comments*" class="validate[required]"></textarea>
					</div>
                                        <div class="g-recaptcha captcha" data-sitekey="6LeZiVYUAAAAAPqVuCY1aB__BIi2vkAJeQ09oscu"></div>
<!--					<div class="captcha">
						<img src="<?php echo base_url('public/default/'); ?>/images/captcha.jpg" alt="" title="">
					</div>-->
					<input type="submit" value="Submit">
				</form>
			</div>
		</div>
	</div>
        
	<div class="contact-map">
            <div id="map">
         <iframe src="<?php echo site_url("common/google_map/1");?>" height="100%" width="100%" frameborder="0" allowfullscreen></iframe>
            </div>
	</div>
	<!--content section end-->

   
<script type="text/javascript">
//    var succ_msg = '<?php // echo preg_replace('/\s\s+/', ' ',outputEscapeString($this->data['site_settings']->success_message)); ?>';
		function beforeCall(form, options){
			return true;
		}
            
		// Called once the server replies to the ajax form validation request
		function ajaxValidationCallback(status, form, json, options){			    
			if (status === true) {
				from_send(json);
			}
		}
		function captchaValidationbcall(){ 
							var recaptcha = $('#g-recaptcha-response').val();
							if(recaptcha == ""){
								alert("Please check the captcha checkbox.");
								return false;
							}else{
							
							}
			}
		jQuery(document).ready(function(){ 
			jQuery("#commentForm").validationEngine('attach',{
				relative: true,
				overflownDIV:"#divOverflown",
				promptPosition:"bottomLeft",
				ajaxFormValidation: true,
				ajaxFormValidationMethod: 'post',
				onAjaxFormComplete: ajaxValidationCallback,
                onBeforeAjaxFormValidation: captchaValidationbcall
			});
		});
		
		function from_send(json){
			var vl ='';
			jQuery(json).each(function(i,val){
				jQuery.each(val,function(k,v){
					if(v!=""){
						vl+=v;
					}
					
			});
			});
			if(vl=="")
                             {
			jQuery("#comment_message").html("");
			jQuery("#contact-left").html('<p><font color="#fff" size="+2"><br/>Thank you for contacting us!</font></p>');
                       jQuery(window).scrollTop($("#contact-left").offset().top);
			}
			else  
			{
				 jQuery("#comment_message").html(vl);
                 jQuery(window).scrollTop($("#contact-left").offset().top);
			}
		}
	</script>
 
<?php echo $this->load->view($footer); ?>