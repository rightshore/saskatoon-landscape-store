
	
	<!--contact section start-->
    <?php //pre($cmspages_down_footer);exit; ?>
	<div class="bottom-contact" id="bottom-contact">
		<div class="wrapper">
        
        <div class="threestack bottom-info">
        	<div class="three">
            	<div class="innerpadding">
                    <h2>Get In Touch</h2>
                    <ul>
                        <?php if($this->data['contact']->address){ ?><li><img src="<?php echo base_url('public/default/images/'); ?>/bottom_info_icon1.png" alt="" title=""> <p><?php echo $this->data['contact']->address; ?></p></li><?php } ?>
                        <?php if($this->data['contact']->contact_phone){ ?><li><img src="<?php echo base_url('public/default/images/'); ?>/bottom_info_icon2.png" alt="" title=""> <p><?php echo $this->data['contact']->contact_phone; ?></p></li><?php } ?>
                        <?php if($this->data['contact']->fax){ ?><li><img src="<?php echo base_url('public/default/images/'); ?>/bottom_info_icon5.png" alt="" title=""> <p><?php echo $this->data['contact']->fax; ?></p></li><?php } ?>
                        <?php if($this->data['contact']->contact_email){ ?><li><a href="mailto:office@saskatoonlandscapestore.com"><img src="<?php echo base_url('public/default/images/'); ?>/bottom_info_icon3.png" alt="" title=""> <p><?php echo $this->data['contact']->contact_email; ?></p></a></li><?php } ?>
                    </ul>
                </div>
            </div>
            <?php if(!empty($this->data['contact']->work_time)){ ?>
            <div class="three">
            	<div class="innerpadding">
                    <h2>Hours of Operation</h2>
                    <ul>
                       <li>
                      	 <img src="<?php echo base_url('public/default/images/'); ?>/bottom_info_icon4.png" alt="" title=""> 
                       	 <?php echo $this->data['contact']->work_time; ?>
                       </li>
                       
                    </ul>
                </div>
            </div>
            <?php } ?>
            <div class="three">
            	<div class="innerpadding">	
            		<h2>Online Calculators</h2>
                    <ul>
                        <li><a href="<?php echo site_url('pages/retaining_wall_calculator'); ?>"><img src="<?php echo base_url('public/default/images/'); ?>/calculator_icon.png" alt="" title=""> <p>RETAINING WALL CALCULATOR</p></a></li>
                        <li><a href="<?php echo site_url('pages/ground_cover_calculator'); ?>"><img src="<?php echo base_url('public/default/images/'); ?>/calculator_icon.png" alt="" title=""> <p>GROUND COVER CALCULATOR</p></a></li>
                    </ul>
                </div>
            </div>
        </div>
		</div>
	</div>
	<!--contact section end-->
	
	
	<!--footer section start-->
	<div class="footer">
		<div class="wrapper">
        <?php 
		$footer_logo_photo = base_url('public/default/images/ftlogo.png');
		//pre($this->data['site_settings']);
		if(!empty($this->data['site_settings']->footer_logo) && is_file(file_upload_path($this->data['site_settings']->footer_logo))){
			$footer_logo_photo = $this->data['site_settings']->footer_logo;
		}
		 ?>
			<a href="<?php echo site_url(); ?>" class="ftlogo"><img src="<?php echo $footer_logo_photo; ?>" alt="<?php echo $this->data['site_settings']->site_name; ?>" title="<?php echo $this->data['site_settings']->site_name; ?>"></a>
                        <?php echo $hooks_footerpages_list; ?>
<!--			<ul class="ftnav">
                            
				<li><a href="index.html" class="active">Home</a></li>
				<li>-</li>
				<li><a href="about.html">About Us</a></li>
				<li>-</li>
				<li><a href="products.html">Products</a></li>
				<li>-</li>
				<li><a href="gallery.html">Gallery</a></li>
				<li>-</li>
				<li><a href="contact.html">Contact Us</a></li>
			</ul>-->
                        <p><?php if(!empty($this->data['site_settings']->copy_right)){ ?>© <?php echo date('Y')." ".$this->data['site_settings']->copy_right; ?><?php } ?> | Webdesign and development by <a href="https://www.2webdesign.com/" rel="nofollow" target="_blank">2webdesign</a></p>
		</div>
		<div class="clear"></div>
	</div>
	<!--footer section end-->


</body>
<?php if(!empty($this->data['site_settings']->analytics_code)) echo $this->data['site_settings']->analytics_code; ?>
</html>