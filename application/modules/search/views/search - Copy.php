<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$this->load->view($header);
?>
<script type="text/javascript" src="<?php echo base_url("public/js/highlight.pack.js");?>"></script>
<script type="text/javascript" src="<?php echo base_url("public/js/tabifier.js");?>"></script>
<script src="<?php echo base_url("public/js/jPages.js");?>"></script>
<script>
	$(document).ready(function(){
	   $("li:contains(is)").css("text-color", "yellow");
	});
</script>
<script>
  $(function() {    
    $("ul.all").jPages({
        containerID: "itemContainerAll",	
        perPage : 3,
        previous : 'Previous',
        next : 'Next',
        first: 'First',
        last: 'Last',
        minHeight: false,
        callback : function(pages, items ){
            var aid='scrollto';
            var aTag = $("a[name='"+ aid +"']");
            var scrollpagi =$("#scrollpagi").val();
            if(scrollpagi!=1) {
            $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }
            $("#scrollpagi").val(2);
        }
    });
  });
  $(function() {    
    $("ul.all1").jPages({
        containerID: "itemContainerAll1",	
        perPage : 3,
        previous : 'Previous',
        next : 'Next',
        first: 'First',
        last: 'Last',
        minHeight: false,
        callback : function(pages, items ){
            var aid='scrollto1';
            var aTag = $("a[name='"+ aid +"']");
            var scrollpagi =$("#scrollpagi1").val();
            if(scrollpagi!=1) {
            $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }
            $("#scrollpagi1").val(2);
        }
    });
  });
  $(function() {    
    $("ul.all2").jPages({
        containerID: "itemContainerAll2",	
        perPage : 3,
        previous : 'Previous',
        next : 'Next',
        first: 'First',
        last: 'Last',
        minHeight: false,
        callback : function(pages, items ){
            var aid='scrollto2';
            var aTag = $("a[name='"+ aid +"']");
            var scrollpagi =$("#scrollpagi2").val();
            if(scrollpagi!=1) {
            $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }
            $("#scrollpagi2").val(2);
        }
    });
  });
  $(function() {    
    $("ul.all3").jPages({
        containerID: "itemContainerAll3",	
        perPage : 3,
        previous : 'Previous',
        next : 'Next',
        first: 'First',
        last: 'Last',
        minHeight: false,
        callback : function(pages, items ){
            var aid='scrollto3';
            var aTag = $("a[name='"+ aid +"']");
            var scrollpagi =$("#scrollpagi3").val();
            if(scrollpagi!=1) {
            $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }
            $("#scrollpagi3").val(2);
        }
    });
  });
  $(function() {    
    $("ul.all4").jPages({
        containerID: "itemContainerAll4",	
        perPage : 4,
        previous : 'Previous',
        next : 'Next',
        first: 'First',
        last: 'Last',
        minHeight: false,
        callback : function(pages, items ){
            var aid='scrollto4';
            var aTag = $("a[name='"+ aid +"']");
            var scrollpagi =$("#scrollpagi4").val();
            if(scrollpagi!=1) {
            $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }
            $("#scrollpagi4").val(2);
        }
    });
  });
  $(function() {    
    $("ul.all5").jPages({
        containerID: "itemContainerAll5",	
        perPage : 4,
        previous : 'Previous',
        next : 'Next',
        first: 'First',
        last: 'Last',
        minHeight: false,
        callback : function(pages, items ){
            var aid='scrollto5';
            var aTag = $("a[name='"+ aid +"']");
            var scrollpagi =$("#scrollpagi5").val();
            if(scrollpagi!=1) {
            $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }
            $("#scrollpagi5").val(2);
        }
    });
  });
  $(function() {    
    $("ul.all6").jPages({
        containerID: "itemContainerAll6",	
        perPage : 3,
        previous : 'Previous',
        next : 'Next',
        first: 'First',
        last: 'Last',
        minHeight: false,
        callback : function(pages, items ){
            var aid='scrollto6';
            var aTag = $("a[name='"+ aid +"']");
            var scrollpagi =$("#scrollpagi6").val();
            if(scrollpagi!=1) {
            $('html,body').animate({scrollTop: aTag.offset().top},'slow');
            }
            $("#scrollpagi6").val(2);
        }
    });
  });
  </script> 
  <!--program section start-->
  <input type="hidden" value="1" name="scrollpagi" id="scrollpagi"/>
  <input type="hidden" value="1" name="scrollpagi1" id="scrollpagi1"/>
  <input type="hidden" value="1" name="scrollpagi2" id="scrollpagi2"/>
  <input type="hidden" value="1" name="scrollpagi3" id="scrollpagi3"/>
  <input type="hidden" value="1" name="scrollpagi4" id="scrollpagi4"/>
  <input type="hidden" value="1" name="scrollpagi5" id="scrollpagi5"/>
  <input type="hidden" value="1" name="scrollpagi6" id="scrollpagi6"/>
   <?php //if((!empty($product_list))||(!empty($category_list))||(!empty($menufac_list))||(!empty($pageslist))||(!empty($nodata))){ ?> 
    <?php //if(!empty($product_list)){?> 
    <div class="content">
    	<div class="wrapper">
            <div class="">
                <?php if(!empty($report_list)|| !empty($staff_list)|| !empty($speakers_bureau_list) || !empty($resource_list)|| !empty($initiative_list) || !empty($events_list) || !empty($blog_list)){ ?>
                <a name="scrollto"/></a>
                <?php if(!empty($blog_list)){ ?>
                <h2>News</h2>
                <ul class="news-list" id="itemContainerAll">
                 <?php 
				$post_image = '';
				foreach($blog_list as $blog){ 
				if(!empty($blog->post_image) && is_file(file_upload_absolute_path()."blog_photo/".$blog->post_image)){
					$post_image = base_url("public/uploads/blog_photo/".$blog->post_image);	
				}
				 ?>
                 <li class="<?php if(empty($post_image)){echo 'nopic';} ?>">
                 		<?php if(!empty($post_image)){ ?>
                    	<div class="news-pic" style="background: url('<?php echo $post_image?>') no-repeat center center ;">
                        <a href="<?php echo site_url('blog/blog_details/'.$blog->page_link); ?>"></a>
                        </div>
                        <?php } ?>
                        <div class="news-content">
                        	<h4><a href="<?php echo site_url('blog/blog_details/'.$blog->page_link); ?>">
                            <?php if(!empty($blog->title)){  echo $blog->title; } ?></a>
                            </h4>
                            <div class="news-publish-info">
							<?php if(!empty($blog->post_dt)){  ?>
                             <span class="published">Published - <?php echo date('d/m/Y',strtotime($blog->post_dt));?></span>
							<?php } ?>
                            <?php if(!empty($blog->post_dt)){  ?>
                              <span class="com-count"><?php echo $blog->comment_count;?></span>
							<?php } ?>   
                              
                            </div>
                            <p>
                           <?php 
							$trimmed_text='';
							if(!empty($blog->excerpt))
							{
                                $desc = $blog->excerpt;//outputEscapeString($blog->description);
								$last_space = ' ';
                                //echo substr($desc, 0, 20);
                                $last_space = strrpos(substr($desc, 0, 120), ' ');
                                $trimmed_text = substr($desc,0, $last_space);
							}
                            echo $trimmed_text;
							?>
                            </p>
                            <a href="<?php echo site_url('blog/blog_details/'.$blog->page_link); ?>" class="more">Read More</a>
                        </div>
                    </li>
                <?php } ?>
                </ul>                
                    <?php  if(count($blog_list)>3){ ?>
                        <ul class="pagination all">
                        </ul>
                    <?php } ?>
                <?php } ?>
                <a name="scrollto1"/></a>
                <?php if(!empty($events_list)){ ?>
                <h2>Events</h2>
                <ul class="events-list" id="itemContainerAll1">
                 <?php foreach($events_list as $event) { ?>	
                    <li>
                    	<div class="event-pic" style="background: url(<?php echo base_url('public/uploads/event_photo/'.$event->event_photo);?>) no-repeat center center;"><a href="<?php echo site_url('attend_an_event/event_detail/'.$event->page_link); ?>"></a></div>
                        <div class="event-cont">
                         <?php if(!empty($event->start_date)) { ?>
                        	<div class="event-date">
                                <p class="date"><?php echo date('d',strtotime($event->start_date)); ?></p>
                                <p class="month"> <?php echo date('M',strtotime($event->start_date)); ?></p>
                                <p class="year"> <?php echo date('Y',strtotime($event->start_date)); ?></p>
                            </div>
                            <?php  } ?>
                            <div class="event-cont-right">
                            <?php if(!empty($event->title)) { ?>
                            	<h4><a href="<?php echo site_url('attend_an_event/event_detail/'.$event->page_link); ?>"><?php echo $event->title; ?></a></h4>
                              <?php } ?>
                                 <?php  $count = mb_strlen($event->excerpt);
							      $last_space = '';
							      $last_space = strrpos(substr($event->excerpt, 0, 110), ' ');
								  $trimmed_text = substr($event->excerpt, 0, $last_space);
							  ?>
                          <?php if(!empty($event->excerpt)) { ?><p><?php echo $trimmed_text; ?></p> <?php } ?>
                                <a href="<?php echo site_url('attend_an_event/event_detail/'.$event->page_link); ?>" class="detail">View details</a>
                            </div>
                        </div>
                    </li>
                   <?php } ?>
                </ul>
                <?php if(count($events_list)>3){ ?>
                <ul class="pagination all1">
                </ul>
                <?php } } ?>
                <?php //pre($initiative_list); echo '</pre>'; ?>
                <a name="scrollto2"/></a>
                <?php if(!empty($initiative_list)){ ?>
                <h2>Reconciliation</h2>
                <ul class="initiatives-list" id="itemContainerAll2">
                    <?php foreach($initiative_list as $row){
                        $where=array('id'=>$row->subject_matter_id);
                        $sub_mater_id=$this->model_search->select_row('subject_matter',$where);
                        $where=array('id'=>$row->region_id);
                        $region_id=$this->model_search->select_row('region',$where);
                        $where=array('id'=>$row->reconciliation_pillars_id);
                        $reconciliation_pillars_id=$this->model_search->select_row('reconciliation_pillars',$where);
                        ?>
                    <li class="search <?php if(!empty($row->region_id)){ echo $row->region_id; } ?>">
                         <?php if(!empty($row->initiatives_featured_image) || !empty($row->embed_video_code)) { ?>   
                    	<div class="initiative-pic-outer">
                            <?php if($row->featured_type==1) { if(!empty($row->initiatives_featured_image) && is_file(file_upload_absolute_path()."initiatives_featured_image/".$row->initiatives_featured_image)) { ?>
                            <div class="initiative-pic" style="background: url(<?php echo base_url("public/uploads/initiatives_featured_image/".$row->initiatives_featured_image); ?>) no-repeat 0 0;"><a href="<?php echo site_url('reconciliations/details/'.$row->page_link); ?>"></a></div><?php } } else { if(!empty($row->embed_video_code)) { ?>
                            <div class="initiative-iframe"><iframe src="<?php echo $row->embed_video_code; ?>" frameborder="0" allowfullscreen></iframe></div> <?php } }?>
                            
                        </div>
                    <?php } ?>
                        <div class="initiative-cont">
                        	<?php if(!empty($row->title)) {?><h3><a href="<?php echo site_url('reconciliations/details/'.$row->page_link); ?>"><?php echo $row->title; ?></a></h3><?php } ?>
                            <?php if(!empty($sub_mater_id[0]->subject_matter)) {?><span class="position"><?php echo $sub_mater_id[0]->subject_matter; ?><br/><?php echo $region_id[0]->region; ?>,<?php echo $reconciliation_pillars_id[0]->reconciliation_pillars; ?></span><?php } ?>
                            <?php if(!empty($row->excerpt)) {?><p><?php echo $row->excerpt; ?></p><?php } ?>
                            <a href="<?php echo site_url('reconciliations/details/'.$row->page_link); ?>" class="view">View Details</a>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                
                <?php if(count($initiative_list)>3) { ?>
                <ul class="pagination all2">
                </ul>
                <?php } } ?>
                <a name="scrollto3"/></a>
                <?php if(!empty($resource_list)){ ?>
                <h2>Resources</h2>
                <ul class="resources-list" id="itemContainerAll3">
                  <?php foreach($resource_list as $resource){ 
                                  $resource_pic = base_url('public/default/images/newsletter_nopic.jpg'); 
                                  if(!empty($resource->image) && is_file(file_upload_absolute_path()."resource_photo/".$resource->image)){
                                      $resource_pic = base_url('public/uploads/resource_photo/'.$resource->image); 
                                  }
                              ?>
                  <li>
                    <div class="resource-pic-outer">
                      <div class="resource-pic" style="background-color : rgba(0, 0, 0, 0); background:url('<?php echo $resource_pic; ?>') no-repeat scroll center 0"><a href="<?php echo site_url('resource/purchase/'.$resource->page_link); ?>"></a></div>
                    </div>
                    <h5><a href="<?php echo site_url('resource/purchase/'.$resource->page_link); ?>">
                      <?php if(!empty($resource->title)){ echo $resource->title; } ?>
                      </a></h5>
                    <p>
                      <?php if(!empty($resource->subtitle)){ echo $resource->subtitle; } ?>
                    </p>
                    <div class="resource-bottom">
                      <?php if(!empty($resource->is_download) && is_file(file_upload_absolute_path()."resource_photo/".$resource->pdffile)) { ?>
                      <a href="<?php echo base_url('public/uploads/resource_photo/'.$resource->pdffile); ?>" target="_blank">Free Download <span></span></a>
                      <?php }else{ ?>
                      <span class="price">$
                      <?php if(!empty($resource->price)){ echo $resource->price; }else{ echo 0; } ?>
                      </span> <a href="<?php echo site_url('cart'); ?>" class="add-cart" onClick="form_submit('<?php echo $resource->id; ?>');">add to cart <span></span></a> 
                      <?php } ?>
                    </div>
                  </li>
                  <?php } ?>
                </ul>
                <?php if(count($resource_list)>4){ ?>
                <ul class="pagination all3">
                </ul> 
                <?php }} ?>
            <a name="scrollto4"/></a>
            <?php if(!empty($speakers_bureau_list)){ ?>
                <h2>Speakers</h2>
                <ul class="staff-list" id="itemContainerAll4">
                    <?php foreach($speakers_bureau_list as $speaker){ 
                        $speaker_photo = base_url('public/default/images/speaker3.jpg');
                        if(!empty($speaker->speakers_bureau_photo) && is_file(file_upload_absolute_path()."speakers_bureau_photo/".$speaker->speakers_bureau_photo)){
                            $speaker_photo = base_url('public/uploads/speakers_bureau_photo/'.$speaker->speakers_bureau_photo);
                        }
                        ?>
                	<li class="<?php $val =  rand(1, 4); if($val =='1'){ echo 'red';}if($val =='2'){ echo 'yellow';}if($val =='3'){ echo 'green';}if($val =='4'){ echo 'blue';} ?>">
                    	<div class="staff-pic">
                        	<figure class="effect-ming">
                            	<img src="<?php echo $speaker_photo; ?>" alt="" title="" />
                                <a href="<?php echo site_url('book_a_speaker/details/'.$speaker->page_link); ?>"><figcaption></figcaption></a>
                            </figure>
                        </div>
                        <div class="staff-names">
                            <h6><a href="<?php echo site_url('book_a_speaker/details/'.$speaker->page_link); ?>"><?php if(!empty($speaker->title)){ echo $speaker->title; } ?></a></h6>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
             <?php if(count($speakers_bureau_list)>4){ ?>
                <ul class="pagination all4">
                </ul> 
             <?php } }?>
                 <a name="scrollto5"/></a>
                <?php if(!empty($staff_list)){ ?>
                <h2>Staff</h2>
                <ul class="staff-list" id="itemContainerAll5">                    
                    <?php $i=0; foreach($staff_list as $row) {
                        $rn=$i%4;
                        if($rn==0)
                            $css="red";
                        elseif($rn==1)
                            $css="yellow";
                        elseif($rn==2)
                            $css="blue";
                        else
                            $css="green";
                        ?>
                	<li class="<?php echo $css; ?>">
                        <?php $staff_photo = base_url('public/default/images/staff_nopic.jpg'); ?>
                            <?php if(!empty($row->staff_photo) && is_file(file_upload_absolute_path()."staff_photo/".$row->staff_photo)) { 
                                    $staff_photo = base_url("public/uploads/staff_photo/".$row->staff_photo);
                                }
                            ?>
                    	<div class="staff-pic">
                        	<figure class="effect-ming">
                                
                            	<img src="<?php echo $staff_photo; ?>" alt="" title="" />
                                <a href="#inline<?php echo $i+1; ?>" class="fancybox"><figcaption></figcaption></a>
                            </figure>
                        </div>
                            <?php //} ?>
                        <div class="staff-names">
                        	<a href="#inline<?php echo $i+1; ?>" class="fancybox">
                                <?php if(!empty($row->title)) { ?><h3><?php echo $row->title; ?></h3><?php } ?>
                                <?php if(!empty($row->designation)) { ?><h6><?php echo $row->designation; ?></h6><?php } ?>
                            </a>
                        </div>
                    </li>
                    <?php $i++; } ?>
                </ul>
                 <?php if(count($staff_list)>4){ ?>
                <ul class="pagination all5">
                </ul>
                <?php } ?>
                    
                <?php $j=0; foreach($staff_list as $row) {
    $rn=$j%4;
                        if($rn==0)
                            $css="red";
                        elseif($rn==1)
                            $css="yellow";
                        elseif($rn==2)
                            $css="blue";
                        else
                            $css="green";
                        ?>
                        <div id="inline<?php echo $j+1; ?>" class="fancy-content fancy-<?php echo $css; ?>">
                            <div class="staff-detail-pic">
                                    <?php $staff_photo = base_url('public/default/images/staff_nopic.jpg'); 
                                    if(!empty($row->staff_photo) && is_file(file_upload_absolute_path()."staff_photo/".$row->staff_photo)) { 
                                        $staff_photo = base_url("public/uploads/staff_photo/".$row->staff_photo);
                                    }?>
                                <img src="<?php echo $staff_photo; ?>" alt="" title="" />
                            </div>
                            <div class="staff-detail">
                                <?php if(!empty($row->title)) { ?><h2><?php echo $row->title; ?></h2><?php } ?>
                                <?php if(!empty($row->designation)) { ?><h6><?php echo $row->designation; ?></h6><?php } ?>
                                <?php if(!empty($row->content)) { echo outputEscapeString($row->content); }?>
                            </div>
                        </div>
                    <?php $j++; } ?>    
                    
                <?php } ?>
                <a name="scrollto6"/></a>
                <?php if(!empty($report_list)){ ?>
                <h2>Annual Reports</h2>
                <ul class="annual-reports" id="itemContainerAll6">
                    <?php foreach ($report_list as $row) { ?>
                	<li>
                    	<div class="annual-report-pic">
                        	<img src="<?php if(!empty($row->image)){ echo base_url("public/uploads/annual_photo/".$row->image); }?>" alt="" title="" />
                        </div>
                        <div class="annual-report-right">
                        	<p><strong><?php if(!empty($row->title)){ echo $row->title; }?></strong></p>
                                <?php 
                                $trimmed_text='';
                                if(!empty($row->description))
                                {
                                    $count = mb_strlen($row->description);
                                    $last_space = '';
                                    $last_space = strrpos(substr($row->description, 0, 100), ' ');
                                    $trimmed_text = substr($row->description, 0, $last_space); 
                                }
                                ?>
                            <p><?php if(!empty($trimmed_text)){ echo $trimmed_text; }?></p>
                           <?php if(!empty($row->pdffile)){ ?> <a href="<?php if(!empty($row->pdffile)){ echo base_url("public/uploads/annual_photo/".$row->pdffile); }?>" class="download" target="_blank">Download</a> <?php } ?>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                <?php if(count($report_list)>3){ ?>
                <ul class="pagination all6">
                </ul>
                <?php } }?>
                <?php }else{ ?><ul><h3><?php echo 'No Result Found.';?></h3></ul><?php } ?>
            </div>
    	</div>
    </div>        
<?php
$this->load->view($footer);/*end*/