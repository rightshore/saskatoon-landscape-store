// JavaScript Document

$(function(){
	
	//equalheight jquery start
	$(window).bind("load", function() {
		equalHeight($(".threestack .three"));
		equalHeight($(".product-section .product-blocks ul li"));
		equalHeight($(".home-products ul li"));
		equalHeight($(".contact-info ul li"));
	});
	
	
	//navigation jquery start
	var widthVal = $(window).width();
	
	if(widthVal > 1024){
		var hasActive = $('.nav li').children('a').hasClass('active');
			$('.nav li').hover(function() {		
				$('ul:first',this).stop(true,true).slideDown(400);
					if(hasActive == false){
						$(this).children('a').addClass('active');
						$(this).children('a').addClass('parent-active');
					}
				},function(){
					$('ul:first',this).stop(true,true).slideUp('fast');
					if(hasActive == false){
						$(this).children('a').removeClass('active');
						$(this).children('a').removeClass('parent-active');
					}
				});
			}else{
		 
				$('.nav > li.parent-dropdown').click(function() {
				
				var hasActive = $(this).children('a').hasClass('active');
				var hasActive1 = $(this).children('a').hasClass('activeurl');
				var childUl  = $(this).children('ul').css('display');
				var hasUrl = $(this).children('a').attr('href');
						
				$('ul:first',this).stop(true,true).slideToggle(400);
			
			
				if(hasActive == false){
					$(this).children('a').toggleClass('parent-active');
					$(this).children('a').toggleClass('active');
					return false;
				}else{
					//window.location.href = hasUrl;				
				}
				
				if(hasActive1 == false && childUl== 'none' ){
					$(this).children('a').addClass('activeurl');
					return false;
				}else{
					window.location.href = hasUrl;
				}
        });
	}
	
	
	//responsive menu jquery start
	$('.responsive').click(function(){
		$(this).toggleClass('active');
		var cheClass = $('.nav').hasClass('activeWidth');
			if(cheClass == false){
			$('.nav').addClass('flexnav-show')
			$('.nav').addClass('activeWidth');
			}else{
			$('.nav').removeClass('flexnav-show')
			$('.nav').removeClass('activeWidth');
		}
	});	
	
		
	//form jquery start	
	$('form input[type=search],form input[type=text],form input[type=email],form input[type=password], form textarea').each(function(){
		var textVal = $(this).val();
		var idVal = $(this).attr('id');
		
		$('#'+idVal).focus(function(){
			if($(this).val() == textVal)
				$(this).val('');
		});
		
		$('#'+idVal).blur(function(){
			if($(this).val() == '')
				$(this).val(textVal);
		});
		
	});
	
	
	//video iframe jquery start
	$('.play').click(function(ev){
		var videoId = $(this).parent('div').children('iframe').attr('id');
		//alert(videoId);
		$(this).fadeOut();
		$("#"+videoId)[0].src += "&autoplay=1";
	});	
	
	
	//background jquery start
	$(window).scroll(function() {
		var makeItResponsive  = $('#bottom-contact').height();
		var widowTop = $(window).scrollTop();
		$('#countVal').html(widowTop);
			$('.bottom-contact').css('background-position', 'center '+parseInt(-widowTop / 22)+'px');
	});
	
	
	$(window).scroll(function() {
		var makeItResponsive  = $('#testimonial').height();
		var widowTop = $(window).scrollTop();
		$('#countVal').html(widowTop);
			$('.testimonial').css('background-position', 'center '+parseInt(-widowTop / 8)+'px');
	});

	
});


$(window).resize(function() {	
			 
	$(".threestack .three, .product-section .product-blocks ul li, .home-products ul li, .contact-info ul li").css('height','auto');
			 
	equalHeight($(".threestack .three"));
	equalHeight($(".product-section .product-blocks ul li"));
	equalHeight($(".home-products ul li"));
	equalHeight($(".contact-info ul li"));
		
});


function equalHeight(group) {
	 var tallest = 0;
	 group.each(function() {
	 var thisHeight = jQuery(this).height();
	 if(thisHeight > tallest) {
	 tallest = thisHeight;
	 }
	 });
	 group.height(tallest);
}


var onImgLoad = function(selector, callback){
    $(selector).each(function(){
        if (this.complete || /*for IE 10-*/ $(this).height() > 0) {
            callback.apply(this);
        }
        else {
            $(this).on('load', function(){
                callback.apply(this);
            });
        }
    });
};



$(function($, window, document, undefined ) { 
   //https://osvaldas.info/responsive-equal-height-blocks
	var $list       = $( '.equalheight' ),
	   $items      = $list.find( '.list__item' ),
	   setHeights  = function()
	   {
		   $items.css( 'height', 'auto' );
	
		   var perRow = Math.floor( $list.width() / $items.width() );
		   if( perRow == null || perRow < 2 ) return true;
	
		   for( var i = 0, j = $items.length; i < j; i += perRow )
		   {
			   var maxHeight   = 0,
				   $row        = $items.slice( i, i + perRow );
	
			   $row.each( function()
			   {
				   var itemHeight = parseInt( $( this ).outerHeight() );
				   if ( itemHeight > maxHeight ) maxHeight = itemHeight;
			   });
			   $row.css( 'height', maxHeight );
		   }
	   };
	
	setHeights();
	$( window ).on( 'resize', setHeights );
	 });





